'use strict';

(function () {
  // Dashboards Controller Spec
  describe('Dashboards Controller Tests', function () {
    // Initialize global variables
    var DashboardsController,
      scope,
      $httpBackend,
      $stateParams,
      $location,
      Authentication,
      Dashboards,
      mockDashboard;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData: function (util, customEqualityTesters) {
          return {
            compare: function (actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _Authentication_, _Dashboards_) {
      // Set a new global scope
      scope = $rootScope.$new();

      // Point global variables to injected services
      $stateParams = _$stateParams_;
      $httpBackend = _$httpBackend_;
      $location = _$location_;
      Authentication = _Authentication_;
      Dashboards = _Dashboards_;

      // create mock dashboard
      mockDashboard = new Dashboards({
        _id: '525a8422f6d0f87f0e407a33',
        title: 'An Dashboard about MEAN',
        content: 'MEAN rocks!'
      });

      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the Dashboards controller.
      DashboardsController = $controller('DashboardsController', {
        $scope: scope
      });
    }));

    it('$scope.find() should create an array with at least one dashboard object fetched from XHR', inject(function (Dashboards) {
      // Create a sample dashboards array that includes the new dashboard
      var sampleDashboards = [mockDashboard];

      // Set GET response
      $httpBackend.expectGET('api/dashboards').respond(sampleDashboards);

      // Run controller functionality
      scope.find();
      $httpBackend.flush();

      // Test scope value
      expect(scope.dashboards).toEqualData(sampleDashboards);
    }));

    it('$scope.findOne() should create an array with one dashboard object fetched from XHR using a dashboardId URL parameter', inject(function (Dashboards) {
      // Set the URL parameter
      $stateParams.dashboardId = mockDashboard._id;

      // Set GET response
      $httpBackend.expectGET(/api\/dashboards\/([0-9a-fA-F]{24})$/).respond(mockDashboard);

      // Run controller functionality
      scope.findOne();
      $httpBackend.flush();

      // Test scope value
      expect(scope.dashboard).toEqualData(mockDashboard);
    }));

    describe('$scope.craete()', function () {
      var sampleDashboardPostData;

      beforeEach(function () {
        // Create a sample dashboard object
        sampleDashboardPostData = new Dashboards({
          title: 'An Dashboard about MEAN',
          content: 'MEAN rocks!'
        });

        // Fixture mock form input values
        scope.title = 'An Dashboard about MEAN';
        scope.content = 'MEAN rocks!';

        spyOn($location, 'path');
      });

      it('should send a POST request with the form input values and then locate to new object URL', inject(function (Dashboards) {
        // Set POST response
        $httpBackend.expectPOST('api/dashboards', sampleDashboardPostData).respond(mockDashboard);

        // Run controller functionality
        scope.create();
        $httpBackend.flush();

        // Test form inputs are reset
        expect(scope.title).toEqual('');
        expect(scope.content).toEqual('');

        // Test URL redirection after the dashboard was created
        expect($location.path.calls.mostRecent().args[0]).toBe('dashboards/' + mockDashboard._id);
      }));

      it('should set scope.error if save error', function () {
        var errorMessage = 'this is an error message';
        $httpBackend.expectPOST('api/dashboards', sampleDashboardPostData).respond(400, {
          message: errorMessage
        });

        scope.create();
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      });
    });

    describe('$scope.update()', function () {
      beforeEach(function () {
        // Mock dashboard in scope
        scope.dashboard = mockDashboard;
      });

      it('should update a valid dashboard', inject(function (Dashboards) {
        // Set PUT response
        $httpBackend.expectPUT(/api\/dashboards\/([0-9a-fA-F]{24})$/).respond();

        // Run controller functionality
        scope.update();
        $httpBackend.flush();

        // Test URL location to new object
        expect($location.path()).toBe('/dashboards/' + mockDashboard._id);
      }));

      it('should set scope.error to error response message', inject(function (Dashboards) {
        var errorMessage = 'error';
        $httpBackend.expectPUT(/api\/dashboards\/([0-9a-fA-F]{24})$/).respond(400, {
          message: errorMessage
        });

        scope.update();
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      }));
    });

    describe('$scope.remove(dashboard)', function () {
      beforeEach(function () {
        // Create new dashboards array and include the dashboard
        scope.dashboards = [mockDashboard, {}];

        // Set expected DELETE response
        $httpBackend.expectDELETE(/api\/dashboards\/([0-9a-fA-F]{24})$/).respond(204);

        // Run controller functionality
        scope.remove(mockDashboard);
      });

      it('should send a DELETE request with a valid dashboardId and remove the dashboard from the scope', inject(function (Dashboards) {
        expect(scope.dashboards.length).toBe(1);
      }));
    });

    describe('scope.remove()', function () {
      beforeEach(function () {
        spyOn($location, 'path');
        scope.dashboard = mockDashboard;

        $httpBackend.expectDELETE(/api\/dashboards\/([0-9a-fA-F]{24})$/).respond(204);

        scope.remove();
        $httpBackend.flush();
      });

      it('should redirect to dashboards', function () {
        expect($location.path).toHaveBeenCalledWith('dashboards');
      });
    });
  });
}());
