$('#tablesorter').tablesorter({
	widgets:['zebra']
});

function checkAll(ele) {
    var checkboxes = document.getElementsByTagName('input');
    if (ele.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = true;
            }
        }
    } else {
        for (var i = 0; i < checkboxes.length; i++) {
            console.log(i)
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = false;
            }
        }
    }
}

$(document).ready(function() {
    $('.periodcontent').hide();

    $('#select').change(function() {
        $('.periodcontent').hide();
        $('.' + $(this).val()).show();
    }).triggerHandler('change');
});

$(function() {
    $('#popupDatepicker').datepick();
    $('#popupDatepicker2').datepick();
    $( '#dl-menu' ).dlmenu();
});

var config = {
    '.chosen-select-no-single' : {disable_search_threshold:10,disable_search:true}
}
for (var selector in config) {
    $(selector).chosen(config[selector]);
}

(function($){
    $(window).load(function(){
        $("#tablescroll").mCustomScrollbar({
            axis:"yx",
            theme:"rounded-dots"
        });
    });
})(jQuery);