'use strict';

angular.module('news').config(['$stateProvider', '$sceDelegateProvider', 'cfpLoadingBarProvider',
    function($stateProvider, $sceDelegateProvider, cfpLoadingBarProvider) {
	   $stateProvider
            .state('news', {
                abstract: true,
                url: '/news',
                template: '<ui-view/>',
                data: {
                    roles: ['user', 'admin']
                }
            })
            .state('news.print', {
                url: '',
                templateUrl: 'modules/news/views/print/news.client.view.html'
            })
            .state('news.tv', {
                url: '/tv',
                templateUrl: 'modules/news/views/tv/news-tv.client.view.html'
            })
            .state('news.radio', {
                url: '/radio',
                templateUrl: 'modules/news/views/radio/news-radio.client.view.html'
            });

        $sceDelegateProvider.resourceUrlWhitelist([
           // Allow same origin resource loads.
           'self',
           // Allow loading from our assets domain.  Notice the difference between * and **.
           'http://app.digivla.id/input/**'
        ]);

        cfpLoadingBarProvider.includeSpinner = false;
    }
]);
