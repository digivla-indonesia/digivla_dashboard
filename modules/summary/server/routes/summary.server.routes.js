'use strict';

var summaryPolicy = require('../policies/summary.server.policy'),
  summary = require('../controllers/summary.server.controller');

module.exports = function(app) {

  app.route('/api/summary/gettablelist').all(summaryPolicy.isAllowed)
    .post(summary.getTableList);

  app.route('/api/summary/editsummary')
  	.post(summary.updateSummary);

  app.route('/api/summary/editdataclient')
  	.post(summary.updateDataClient);

  app.route('/api/summary/deletearticle')
    .post(summary.deleteArticles);

  app.route('/api/summary/getcategorylist')
    .get(summary.getCategoryList);

  app.route('/api/summary/savearticle')
    .post(summary.saveArticle);

  /** these route is not used for get article detail
  app.route('/api/dashboard/getdetailarticle').all(summaryPolicy.isAllowed)
     .post(summary.getArticleDetail);
  */
};