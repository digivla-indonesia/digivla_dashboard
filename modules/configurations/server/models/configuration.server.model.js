'use strict';

/**
 * Module dependencies.
 */
// var mongoose = require('mongoose'),
//   Schema = mongoose.Schema;

/**
 * Configuration Schema
 */
// var ConfigurationSchema = new Schema({
//   created: {
//     type: Date,
//     default: Date.now
//   },
//   title: {
//     type: String,
//     default: '',
//     trim: true,
//     required: 'Title cannot be blank'
//   },
//   content: {
//     type: String,
//     default: '',
//     trim: true
//   },
//   user: {
//     type: Schema.ObjectId,
//     ref: 'User'
//   }
// });

// mongoose.model('Configuration', ConfigurationSchema);

var request = require('request');

module.exports = function (segment, datas, callback) {
  // var url = (segment.type === 'do') ? 'http://139.59.245.121:3035' + segment.url : 'http://127.0.0.1:8080' + segment.url;
  var url = (segment.type === 'do') ? global.digitalOceanAPI + segment.url : global.apiAddress + segment.url;
  var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
  var req = {
    uri: url,
    // uri: 'http://182.23.64.70:8000/' + segment,
    // uri: 'http://202.56.162.38:8080/' + segment,
    // uri: 'http://182.23.64.29:8080/' + segment,
    // method: 'POST',
    method: segment.method,
    headers: {
        Authorization: 'Basic ' + auth,
        'Content-Type': 'application/json'
    },
    json: datas
  };

  request(req, function(err, httpResponse, body){
    if(err){
      return callback({
        success: false,
        data: err
      });
    }

    var resultVar = JSON.parse(JSON.stringify(body));

    if(resultVar.status === 'failed'){
      return callback({
        success: false,
        data: resultVar
      });
    } else{
      return callback({
        success: true,
        data: resultVar
      });
    }

  });
};
