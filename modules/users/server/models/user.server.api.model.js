'use strict';

/**
 * Module dependencies.
 */
var request = require('request');

module.exports = function (segment, datas, callback) {

  var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
  var req = {
    uri: global.apiAddress + '/' + segment,
    // uri: 'http://202.56.162.38:8080/' + segment,
    // uri: 'http://182.23.64.29:8080/' + segment,
    method: 'POST',
    headers: {
        Authorization: 'Basic ' + auth,
        'Content-Type': 'application/json'
    },
    json: datas
  };

  request(req, function(err, httpResponse, body){
    if(err){
      return callback({
        success: false,
        data: err
      });
    }

    var resultVar = JSON.parse(JSON.stringify(body));

    if(resultVar.status === 'failed'){
      return callback({
        success: false,
        data: resultVar
      });
    } else{
      return callback({
        success: true,
        data: resultVar
      });
    }

  });
};
