'use strict';

//Dashboards service used for communicating with the articles REST endpoints
var appModule = angular.module('dashboards');
appModule.factory('Dashboards', ['$resource',
  function ($resource) {
    /*return $resource('api/dashboards/:dashboardId', {
      dashboardId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });*/
  }
]);

appModule.factory('ArticleListData', function() {
    var objectValue = {
        // type: '',
        // date_to: '',
        // date_from: '',
        // media: '',
        // category: '',
        // tone: ''
    };
    
    return {
        setObject: function(value){
        	//objectValue = value;
        	// objectValue.type = value.type;
        	// objectValue.date_from = value.date_from;
        	// objectValue.date_to = value.date_to;
        	// objectValue.media = value.media;
        	// objectValue.category = value.category;
         //    objectValue.tone = value.tone;

            objectValue.type = value.type;
            objectValue.date_to = value.date_to;
            objectValue.date_from = value.date_from;
            objectValue.media_set = value.media_set;
            objectValue.media_id = value.media_id;
            objectValue.group_category = value.group_category;
            objectValue.sub_category = value.sub_category;
            objectValue.tone = value.tone;
            objectValue.original_date_from = value.original_date_from;
            objectValue.original_date_to = value.original_date_to;
            objectValue.old_url = value.old_url;
        },
        getObject: function() {
            return objectValue;
        }
    }
});

appModule.directive('fancybox', function($compile){
    return {
        restrict: 'A',
        // scope: {
        //     save_edit: '='
        // },
        scope: true,
        controller: function($scope){
            $scope.openFancyBox = function(type, items){
                var compiledTemplate = $compile('<div>ABC lima dasar</div>');
                compiledTemplate($scope);

                var content = '<div class="dialogPadd">'+
                    '<table width="350px;">'+
                        '<tr>'+
                            '<td width="20%">Media Set</td>'+
                            '<td width="5%" align="center">:</td>'+
                            '<td width="50%"><input type="text" style="width:100%;" value="'+ type +'" id="medsete_edit" /></td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td colspan=3>&nbsp</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td align="center" colspan=3>'+
                                '<a class="btn-d  closeFancybox">Cancel</a>'+
                                // '<a class="btn-d" ng-click="save_edit()">Save</a>'+
                                '<a class="btn-d" ng-click="save_edit()">{{swt}}</a>'+
                            '</td>'+
                        '</tr>'+
                    '</table>'+
                '</div>';

                $.fancybox.open({content: content, type: 'html'});
            };
            $scope.swt = 'Save';
            $scope.save_edit = function(){
                $scope.swt = 'Kuda';
                alert('HAI HAI');
            };
        }
    }
});