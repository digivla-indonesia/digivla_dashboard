'use strict';

/**
 * Module dependencies.
 */
var request = require('request');

module.exports = function (uriSegment, datas, callback) {

  var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
  var req = {
    uri: global.apiAddress + "/" + uriSegment.first + "/" + uriSegment.second,
    method: 'POST',
    headers: {
        Authorization: 'Basic ' + auth,
        'Content-Type': 'application/json'
    },
    json: datas
  };

  request(req, function(err, httpResponse, body){
    if(err){
      return callback({
        success: false,
        data: err
      });
    }

    var resultVar = JSON.parse(JSON.stringify(body));

    return callback({
      success: true,
      data: resultVar
    });
  });
};
