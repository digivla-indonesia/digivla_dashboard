'use strict';

angular.module('core').controller('PeriodController', ['$scope', '$rootScope', '$state', '$http', 'Authentication',
  function($scope, $rootScope, $state, $http, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    /* For option 'Pilih' */
    // setup clear
    $scope.clear = function() {
      $scope.dateFrom = null;
      $scope.dateTo = null;
    };

    // open min-cal
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
      $scope.opened1 = false;
    };

    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
      $scope.opened = false;
    };

    $scope.maxDate = new Date();
    $scope.minDate = '2010-01-01';
    $scope.format = 'yyyy-MM-dd';

    $rootScope.dateFrom = '';
    $rootScope.dateTo = '';
    /* For option 'Pilih' */
  }
]);