'use strict';

angular.module('search').controller('SearchController', ['$scope', '$http', '$rootScope', '$window', '$sce', 'Authentication',
  function($scope, $http, $rootScope, $window, $sce, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    // Pagination
    $scope.filteredArticles = [];
    $scope.results = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 10;
    $scope.maxSize = 5;
    $scope.isDisplay = false;

    $scope.totalArticles = 0;
    $scope.articleLimit = '-';
    $scope.offset = '-';

    $scope.loadingData = false;

    function searchArticle(offset){
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      $scope.loadingData = true;
      $scope.tableData = [];

      $scope.formdata.media_type = $scope.selectedType;
      $scope.formdata.offset = offset;
      $scope.formdata.time_frame = $rootScope.selectedPeriodOptions.value;
      $scope.formdata.date_from = from;
      $scope.formdata.date_to = to;

      $http.post('/api/search_article', $scope.formdata)
        .success(function(response) {
          $scope.loadingData = false;
          $scope.totalItems = response.pagination.total_articles;
          $scope.totalArticles = response.pagination.total_articles;
          $scope.isDisplay = true;
          $scope.tableData = response.data;
          $scope.articleLimit = response.pagination.limit;
          $scope.offset = response.pagination.offset;
        })
        .error(function(response) {
          $scope.results = response.message;
        });
    };

    $scope.searchArticlePost = function() {
      searchArticle(0);
    };

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.detailArticle = function(val) {
      var url = '/api/search/getarticledetail?_id=' + val;
      $scope.mediaHTML = "";
      $scope.mediaUrl = null;
      $http.get(url)
        .success(function(response) {
          var results = response;

          // highlight for keyword
          // var res = results.content.split(" ");
          // var keyword = $scope.formdata.keyword.split(" ");
          // for(var i=0;i<res.length;i++){
          //   for(var j=0;j<keyword.length;j++){
          //     if(res[i].toLowerCase() == keyword[j].toLowerCase()){
          //       res[i] = "<span style='color:#f89406; font-weight:bolder;'>" + res[i] + "</span>";
          //     } else{
          //       var a = res[i].toLowerCase().indexOf(keyword[j].toLowerCase());
          //       if(a != -1){
          //           var separatedString = "<span style='color:#f89406; font-weight:bolder;'>" + res[i].substr(a, keyword[j].length) + "</span>";
          //           var b = res[i].replace(res[i].substr(a, keyword[j].length), separatedString);
          //           res[i] = b;
          //       }
          //     }
          //   }
          // }
          // results.content = res.join(" ");
          var temp;
          var x, y;
          var selectedKeyword;
          selectedKeyword = $scope.formdata.keyword.match(/\w+|"[^"]+"/g);
          for(var k=0;k<selectedKeyword.length;k++){
            x = selectedKeyword[k];
            x = x.replace(/\"/g, "");
            x = x.replace(/\'/g, "");

            temp = results.content.replace(new RegExp(x, "ig"), "<span style='color:#f89406; font-weight:bolder;'>" + x + "</span>");
            results.content = temp;
          }

          results.content = results.content.replace(/(?:\r\n|\r|\n)/g, '<br />');

          if(results.file_pdf.indexOf('.mp4') !== -1){
            console.log(JSON.stringify(results));
            var media_type = (results.file_pdf.indexOf('.mp3') !== -1) ? 'media_radio' : 'media_tv';
            var file = results.file_pdf;
            var tanggal = file.substring(8, 10),
            bulan = file.substring(5, 7),
            tahun = file.substring(0, 4);

            $scope.mediaHTML = '<video width="25%" controls>' +
              '<source src="' + 'http://input.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="video/mp4">' +
              'Your browser does not support the video tag.' +
            '</video>';

          } else if(results.file_pdf.indexOf('.mp3') !== -1){
            var file = response.data.data.file_pdf;
            var tanggal = file.substring(8, 10),
            bulan = file.substring(5, 7),
            tahun = file.substring(0, 4);

            $scope.mediaHTML = '<audio width="25%" controls>' +
              '<source src="' + 'http://input.antara-insight.id/media_radio/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="audio/mpeg">' +
              'Your browser does not support the audio tag.' +
            '</audio>';
          } else{
            results.file_pdf = results.file_pdf;
          }

          $scope.articleDetail = results;
          $.fancybox.open({href: '#dd1'});
        }).error(function(response) {
          $scope.articleDetail = null;
        });
    };

    $scope.pageChanged = function() {
      // $scope.offset = ($scope.currentPage - 1) * 20;
      var articleOffset = ($scope.currentPage - 1) * 10;
      $scope.loadingData = true;
      $scope.tableData = [];
      // getTableData($scope.offset);
      searchArticle(articleOffset);
    };

    // generate pdf
    $scope.getPDF = function(type, ai){
      var items = {
        type: type,
        article_id: ai,
        article_detail: $scope.articleDetail
      };

      // $http.post('/api/news/generatepdf').success(function(data, status, headers, config){
      $http.post('/api/news/generatepdf', items).success(function(resp){
        var fn = 'pdf_' + Date.now().toString() + '.pdf';
        pdfMake.createPdf(resp).download(fn);
      }).error(function(resp){
        alert('failed: ' + JSON.stringify(resp));
      });
    };

    $scope.getScan = function(type, file){
      var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);
      var fileUrl = 'http://pdf.antara-insight.id/' + type + '/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;

      window.open(fileUrl, '_blank');
    };

    $scope.getWord = function(){
      var xx = btoa(JSON.stringify($scope.articleDetail));

      var downloadWordURL = '/api/news/generatedoc?data=' + xx;
      window.open(downloadWordURL, '_blank');
    };

    $scope.showType = ['All Media', 'Print Media', 'Online Media', 'TV', 'Radio'];
    $scope.selectedType = $scope.showType[0];


    $scope.saveArticle = function(articleDetail){
      $http.get('/api/search/getcategorylist').success(function(data){
        $scope.categoryLists = data;
        $.fancybox.open({href: "#save-article"});
      }).error(function(data){
        alert(data);
      });
    };

    $scope.doSaveArticle = function(){
      var selectedArticle = $scope.articleDetail;
      selectedArticle.categories = $scope.chosenCategory;

      $http.post('/api/search/savearticle', selectedArticle).success(function(data){
        alert('success');
        $scope.chosenCategory = [];
        $.fancybox.close();
      }).error(function(data){
        alert('failed');
      });
    };

    $scope.chosenCategory = [];
    $scope.addCategory = function(val){
      var existsFlag = $scope.chosenCategory.indexOf(val);
      if(existsFlag !== -1){
        $scope.chosenCategory.splice(existsFlag, 1);
      } else{
        $scope.chosenCategory.push(val);
      }
    };

    $scope.originaleSingle = function(stat, formate, article, type){
      var items = [{
        article_id: article.article_id,
        category_id: article.category_id,
        activity: type
      }];

      $http.post('/api/news/saveactivity', items).success(function(response){
        var url = 'http://pdf.antara-insight.id?stat=' + stat;
        url += '&article=' + article.article_id;
        url += '&formate=' + formate;
        url += '&logo_head=' + Authentication.user.user_detail.logo_head;
        url += '&client_id=' + Authentication.user.user_detail.client_id;

        $window.open(url, '_blank');
      }).error(function(response){
        alert(response);
      });

    };
  }
]);