'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
  path = require('path'),
  restService = require(path.resolve('./modules/users/server/models/user.server.api.model')),
  config = require(path.resolve('./config/config')),
  request = require('request');

/**
 * Module init function.
 */
module.exports = function (app, db) {
  // Serialize sessions
  passport.serializeUser(function (user, done) {
    done(null, user.token);
  });

  // Deserialize sessions
  passport.deserializeUser(function (id, done) {
    restService('user/profile', {token: id}, function(response){

      if(!response.success){
        done(response.data);
      } else{
        if(typeof response.data.data_login === 'undefined'){
          // console.log(JSON.stringify(response));
          done('Cannot get user profile');
        } else{
          var userData = {
            'token': id,
            'user_detail': response.data.data_login[0],
            'provider': 'local',
            'roles': ['user'],
            'menune': response.data.menune
          };

          done(null, userData);
        }
      }
    });
  });

  // Initialize strategies
  config.utils.getGlobbedPaths(path.join(__dirname, './strategies/**/*.js')).forEach(function (strategy) {
    require(path.resolve(strategy))(config);
  });

  // Add passport's middleware
  app.use(passport.initialize());
  app.use(passport.session());
};
