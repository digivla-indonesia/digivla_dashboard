$(document).ready(function() {
	$('.fancybox').fancybox({});
	$('.periodcontent').hide();

    $('#select').change(function() {
        $('.periodcontent').hide();
        $('.' + $(this).val()).show();
    }).triggerHandler('change');
});	

$(function() {
	$('#popupDatepicker').datepick();
	$('#popupDatepicker2').datepick();
	$( '#dl-menu' ).dlmenu();
});

$('#tablesorter2').tablesorter({
	widgets:['zebra']
});

function checkAll(ele) {
	var checkboxes = document.getElementsByTagName('input');
	if (ele.checked) {
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].type == 'checkbox') {
				checkboxes[i].checked = true;
			}
		}
	} else {
		for (var i = 0; i < checkboxes.length; i++) {
			console.log(i)
			if (checkboxes[i].type == 'checkbox') {
				checkboxes[i].checked = false;
			}
		}
	}
}

var config = {
	'.chosen-select-no-single' : {disable_search_threshold:10,disable_search:true}
}
for (var selector in config) {
	$(selector).chosen(config[selector]);
}

(function($){
	$(window).load(function(){
		$("#search-selection").mCustomScrollbar({
			theme:"rounded-dots"
		});
		$("#dom-selection").mCustomScrollbar({
			theme:"rounded-dots"
		});
	});
})(jQuery);