'use strict';

/**
 * Module dependencies.
 */
var dashboardsPolicy = require('../policies/dashboards.server.policy'),
  dashboards = require('../controllers/dashboards.server.controller');

module.exports = function (app) {
  // Dashboards collection routes
  // app.route('/api/dashboards').all(dashboardsPolicy.isAllowed)
  //   .get(dashboards.list)
  //   .post(dashboards.create);

  app.route('/api/dashboards/getprocess')
    .get(dashboards.getProcess);

  app.route('/api/dashboards/getprocess3')
    .get(dashboards.getProcess3);

  app.route('/api/dashboards/getprocess4')
    .get(dashboards.getProcess4);

  app.route('/api/dashboards/getprocess6')
    .get(dashboards.getProcess6);

  app.route('/api/dashboards/getarticlelist')
    .post(dashboards.getArticleList);

  app.route('/api/dashboards/getarticlelistfromcharts')
    .post(dashboards.getArticleListFromCharts);

  app.route('/api/dashboards/getarticledetail')
    .get(dashboards.getArticleDetail);

  app.route('/api/dashboards/downloadppt')
    .get(dashboards.downloadPPT)
    .post(dashboards.downloadPPTPost);

  app.route('/api/dashboards/downloadexcel')
    .get(dashboards.downloadExcel);
    // .post(dashboards.downloadExcel);
  // Single dashboard routes
  // app.route('/api/dashboards/:dashboardId').all(dashboardsPolicy.isAllowed)
  //   .get(dashboards.read)
  //   .put(dashboards.update)
  //   .delete(dashboards.delete);

  // Finish by binding the dashboard middleware
  //app.param('dashboardId', dashboards.dashboardByID);
};
