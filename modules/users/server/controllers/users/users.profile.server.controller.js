'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  restService = require(path.resolve('./modules/users/server/models/user.server.api.model'));
  // mongoose = require('mongoose'),
  // User = mongoose.model('User');

/**
 * Update user details
 */
// exports.update = function (req, res) {
//   // Init Variables
//   var user = req.user;

//   // For security measurement we remove the roles from the req.body object
//   delete req.body.roles;

//   if (user) {
//     // Merge existing user
//     user = _.extend(user, req.body);
//     user.updated = Date.now();
//     user.displayName = user.firstName + ' ' + user.lastName;

//     user.save(function (err) {
//       if (err) {
//         return res.status(400).send({
//           message: errorHandler.getErrorMessage(err)
//         });
//       } else {
//         req.login(user, function (err) {
//           if (err) {
//             res.status(400).send(err);
//           } else {
//             res.json(user);
//           }
//         });
//       }
//     });
//   } else {
//     res.status(400).send({
//       message: 'User is not signed in'
//     });
//   }
// };

/**
 * Update profile picture
 */
// exports.changeProfilePicture = function (req, res) {
//   var user = req.user;
//   var message = null;

//   if (user) {
//     fs.writeFile('./modules/users/client/img/profile/uploads/' + req.files.file.name, req.files.file.buffer, function (uploadError) {
//       if (uploadError) {
//         return res.status(400).send({
//           message: 'Error occurred while uploading profile picture'
//         });
//       } else {
//         user.profileImageURL = 'modules/users/img/profile/uploads/' + req.files.file.name;

//         user.save(function (saveError) {
//           if (saveError) {
//             return res.status(400).send({
//               message: errorHandler.getErrorMessage(saveError)
//             });
//           } else {
//             req.login(user, function (err) {
//               if (err) {
//                 res.status(400).send(err);
//               } else {
//                 res.json(user);
//               }
//             });
//           }
//         });
//       }
//     });
//   } else {
//     res.status(400).send({
//       message: 'User is not signed in'
//     });
//   }
// };

/**
 * Send User
 */
exports.me = function (req, res) {
  res.json(req.user || null);
};

/**
 *  Get Users Asset for filtering
 */
exports.getClientAssets = function(req, res){
  var token = req.user.token;
  var client_id = req.user.user_detail.client_id;
  var requestType = req.query.t;
  var segment = (requestType === 'media') ? 'tb_media_set' : 'tb_category_set_type';

  var items = {
    "client_id": client_id,
    "token": token
  };

  restService(segment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }
    res.status(200).send(resp.data);
  });
};

exports.getClientSubAssets = function(req, res){
  var token = req.user.token;
  var client_id = req.user.user_detail.client_id;
  var requestType = req.query.t;
  var segment = (requestType === 'media') ? 'tb_media_client' : 'tb_category_list';

  var items = {
    "client_id": client_id,
    "token": token
  }

  restService(segment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }
    res.status(200).send(resp.data);
  });
};

exports.getSubClientCategory = function(req, res){
  var token = req.user.token;
  var client_id = req.user.user_detail.client_id;
  var category_set = req.query.cs;
  var segment = 'tb_category_set';

  var items = {
    "client_id": client_id,
    "token": token,
    "category_set": category_set
  }

  restService(segment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }
    res.status(200).send(resp.data.data);
  });
};

exports.getSubClientMedia = function(req, res){
  var token = req.user.token;
  var client_id = req.user.user_detail.client_id;
  var media_set = req.query.ms.toString();
  var segment = 'tb_media_set_choosen';

  var items = {
    "client_id": client_id,
    "token": token,
    "user_media_type_id": media_set
  }

  restService(segment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }

    res.status(200).send(resp.data.data);
  });
};

exports.updateClientAssets = function(req, res){
  var token = req.user.token;
  var client_id = req.user.user_detail.client_id;
  var updateId = (typeof req.body._id !== 'undefined') ? req.body._id : '';
  var name = (typeof req.body.name !== 'undefined') ? req.body.name : '';
  var requestType = req.query.t;
  var segment = (requestType === 'media') ? 'update/tb_media_set' : 'update/tb_category_set_type';
  var items = {};

  items.token = token;
  items.client_id = client_id;

  if(requestType === 'media'){
    items.media_set_id = updateId;
    items.media_name = name;
  } else{
    items.category_set = updateId;
    items.descriptionz = name;
  }

  restService(segment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }
    res.status(200).send(resp.data);
  });

};

exports.updateClientSubAssets = function(req, res){
  var token = req.user.token;
  var client_id = req.user.user_detail.client_id;
  var updateId = (typeof req.body._id !== 'undefined') ? req.body._id : '';
  var name = (typeof req.body.name !== 'undefined') ? req.body.name : '';
  var requestType = req.query.t;
  var segment = (requestType === 'media') ? 'update/tb_media_client' : 'update/tb_category_list';
  var items = {};

  items.token = token;
  items.client_id = client_id;
  items.category_id_str = updateId;
  items.category_id_str_new = name;

  console.log(JSON.stringify(items));

  restService(segment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }
    res.status(200).send(resp.data);
  });
};

exports.deleteClientAssets = function(req, res){
  return res.send(200);
};

exports.deleteClientSubAssets = function(req, res){
  return res.send(200);
};