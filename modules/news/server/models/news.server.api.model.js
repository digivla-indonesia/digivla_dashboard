'use strict';

/**
 * Module dependencies
 */
var request = require('request');

module.exports = function (datas, uriSegment, callback) {
  var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
  var req = {
    uri: global.digitalOceanAPI + '/' + uriSegment.index,
    headers: {
      Authorization: 'Basic ' + auth,
      'Content-Type': 'application/json'
    },
    method: uriSegment.method,
    json: datas
  };

  request(req, function(err, httpResponse, body) {
    if (err) {
      return callback({
        success: false,
        data: err
      });
    }

    var resultVar = JSON.parse(JSON.stringify(body));

    return callback({
      success: true,
      data: resultVar
    });
  });
};
