'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  async = require('async'),
  restService = require(path.resolve('./modules/configurations/server/models/configuration.server.model')),
  restAPIService = require(path.resolve('./modules/configurations/server/models/configuration.server.api.model')),
  //Configuration = mongoose.model('Configuration'),
  request = require('request'),
  os = require('os'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  moment = require('moment');

/*
 *  Custom Controller based on digivla requirement
 */
exports.getCategorySetDetail = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      group_category_id = (typeof req.query.group_category_id !== 'undefined') ? parseInt(req.query.group_category_id) : 1,
      // uriSegment = {
      //   type: 'do',
      //   url: '/api/configuration/categorysetdetail',
      //   method: 'POST'
      // };
      items = {
        token: token,
        client_id: client_id,
        category_set: group_category_id
      };

  // restService(uriSegment, items, function(resp){
  //   if(!resp.success){
  //     res.status(400).send(resp.data);
  //     return;
  //   }

  //   return res.status(200).send(resp.data);
  // });

  async.waterfall([
    function(cb){
      var uriSegment= {
        first: 'tb_category_set',
        second: ''
      };

      restAPIService(uriSegment, items, function(resp){
        var result = resp.data.data;
        var catSetArr = [];

        for(var i=0;i<result.length;i++){
          catSetArr.push(result[i].category_id);
        }

        cb(null, catSetArr);
      });
    },
    function(catSetArr, cb){
      var uriSegment= {
        first: 'tb_category_list',
        second: ''
      };

      restAPIService(uriSegment, items, function(resp){
        var resultArr = [];
        var result = resp.data.data;

        for(var i=0;i<result.length;i++){
          if(catSetArr.indexOf(result[i].category_id) == -1){
            resultArr.push({
              category_id: result[i].category_id,
              chosen: false
            });
          } else{
            resultArr.push({
              category_id: result[i].category_id,
              chosen: true
            });
          }
        }

        cb(null, resultArr);
      });
    }

  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.getSubCategorySetDetail = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      category_id = (typeof req.query.category_id !== 'undefined') ? req.query.category_id : 1;

  var items = {
    token: token,
    client_id: client_id,
    category_id: category_id
  };

  // restService(uriSegment, items, function(resp){
  //   if(!resp.success){
  //     res.status(400).send(resp.data);
  //     return;
  //   }

  //   return res.status(200).send(resp.data);
  // });
  async.waterfall([
    function(cb){
      var uriSegment= {
        first: 'tb_run_backtrack',
        second: ''
      };

      restAPIService(uriSegment, items, function(resp){
        var result = resp.data.data;
        var firstArr = [];

        for(var i=0;i<result.length;i++){
          firstArr.push(result[i].category_id);
        }

        cb(null, firstArr);
      });
    },
    function(firstArr, cb){
      var uriSegment = {
        first: 'tb_keyword',
        second: ''
      };

      restAPIService(uriSegment, items, function(resp){
        if(resp.data.status === 'failed'){
          cb(null, []);
        }

        var resultArr = [];
        var result = resp.data.data;

        for(var i=0;i<result.length;i++){
          if(firstArr.indexOf(result[i].keyword) == -1){
            resultArr.push({
              keyword: result[i].keyword,
              chosen: false
            });
          } else{
            resultArr.push({
              keyword: result[i].keyword,
              chosen: true
            });
          }
        }

        cb(null, resultArr);
      });
    }

  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.getMediaDetail = function(req, res, next){
  //var mediaListJson = require(path.resolve('./modules/configurations/server/models/modea_collection.json'));
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      media_set = (typeof req.query.media_set !== 'undefined') ? req.query.media_set : 1

  // var items = {
  //   token: token,
  //   client_id: client_id,
  //   media_set: media_set
  // };

  // restService(uriSegment, items, function(resp){
  //   if(!resp.success){
  //     res.status(400).send(resp.data);
  //     return;
  //   }

  //   return res.status(200).send(resp.data);
  // });

  async.waterfall([
    function(cb){
      // var uriSegment = {
      //   first: 'media',
      //   second: 'datalist'
      // },
      // items = {
      //   "statuse": ["A"],
      //   "token": token
      // };
      var items = {
        // statuse: 'A'
        "statuse": "A",
        "token": token
      },
      uriSegment = {
        first: 'media',
        second: 'datalist'
      };

      restAPIService(uriSegment, items, function(resp){
        var result = resp.data.data;

        cb(null, result);
      });
    },
    function(result, cb){
      var items = {
        "token": token,
        "client_id": client_id,
        "user_media_type_id": media_set
      }, uriSegment= {
        first: 'tb_media_set_choosen',
        second: ''
      };

      restAPIService(uriSegment, items, function(resp){
        var chosenMedia = resp.data.data;
        var chosenMediaArr = [];

        if(resp.success){
          for(var i=0;i<chosenMedia.length;i++){
            chosenMediaArr.push(chosenMedia[i].user_media_id);
          }
        }

        cb(null, result, chosenMedia, chosenMediaArr);
      });
    },
    function(result, chosenMedia, chosenMediaArr, cb){
      var items = {
        "token": token,
        "client_id": client_id
      }, uriSegment= {
        first: 'tb_media_client',
        second: ''
      }, resultArr = [];

      restAPIService(uriSegment, items, function(resp){
        var clientMedia = resp.data.data;
        var clientMediaArr = [];

        if(typeof clientMedia === 'object'){
          for(var i=0;i<clientMedia.length;i++){
            clientMediaArr.push(clientMedia[i].media_id);
          }
        } else{
          return cb('No media has selected for this user. Please contact Administrator for further verification.', null);
        }

        var mediaTypeArr = [];

        for(var i=0;i<result.length;i++){
          if(mediaTypeArr.indexOf(result[i].media_type) == -1)
            mediaTypeArr.push(result[i].media_type);
        }

        for(var i=0;i<mediaTypeArr.length;i++){
          var tempJson = {};
          tempJson.media_type = mediaTypeArr[i];
          tempJson.media_list = [];

          for(var j=0;j<result.length;j++){
            if(mediaTypeArr[i] === result[j].media_type){
              if(clientMediaArr.indexOf(result[j].media_id) !== -1){

                if(chosenMediaArr.length > 0){
                  tempJson.media_list.push({
                    media_id: result[j].media_id,
                    media_name: result[j].media_name,
                    chosen: (chosenMediaArr.indexOf(result[j].media_id) !== -1) ? true : false
                  });
                } else{
                  tempJson.media_list.push({
                    media_id: result[j].media_id,
                    media_name: result[j].media_name,
                    chosen: false
                  });
                }

              }
              //  else{
              //   tempJson.media_list.push({
              //     media_id: result[j].media_id,
              //     media_name: result[j].media_name,
              //     chosen: false
              //   });
              // }
            }
          }

          resultArr.push(tempJson);
        }

        // return res.send(resultArr);
        cb(null, resultArr);
      });
    }

  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send(result);
  });
};




exports.createCategoryList = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      category_id_str = (typeof req.body.category_id_str !== 'undefined') ? req.body.category_id_str : '',
      uriSegment = {
        type: 'api',
        url: '/insert/tb_category_list',
        method: 'POST'
      };

  var items = {
    client_id: client_id,
    token: token,
    category_id_str: category_id_str
  };

  restService(uriSegment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }

    return res.status(200).send(resp.data);
  });
};

exports.deleteCategoryList = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      category_id_str = (typeof req.query._id !== 'undefined') ? req.query._id : '',
      uriSegment = {},
      items = {};

  async.waterfall([
    function(cb){
      uriSegment = {
        type: 'api',
        url: '/delete/tb_category_list',
        method: 'POST'
      },
      items = {
        client_id: client_id,
        token: token,
        category_id_str: category_id_str
      };

      restService(uriSegment, items, function(resp){
        console.log('/delete/tb_category_list');
        console.log(JSON.stringify(resp));
        console.log('################################################################################');
        if(!resp.success){
          return cb(resp.data, null)
        }

        return cb(null, resp.data);
      });
    },
    function(statement, cb){
      uriSegment = {
        type: 'api',
        url: '/delete/tb_keyword',
        method: 'POST'
      },
      items = {
        client_id: client_id,
        token: token,
        category_id_str: category_id_str
      };

      restService(uriSegment, items, function(resp){
        console.log('/delete/tb_keyword');
        console.log(JSON.stringify(resp));
        console.log('################################################################################');
        if(!resp.success){
          return cb(resp.data, null)
        }

        return cb(null, resp.data);
      });
    },
    function(statement, cb){
      var req;
      req = {
        uri: global.botApiGet + 'updatekeywordallserver',
        method: 'GET'
      };

      request(req, function(err, httpResponse, body){
        console.log(global.botApiGet + 'updatekeywordallserver');
        console.log('body: ' + JSON.stringify(body));
        console.log('httpResponse: ' + JSON.stringify(httpResponse));

        cb(null, true);
      });

    },
    function(statement, cb){
      var req;
      req = {
        uri: global.botApiGet + 'updateclientallserver',
        method: 'GET'
      };

      request(req, function(err, httpResponse, body){
        console.log(global.botApiGet + 'updateclientallserver');
        console.log('body: ' + JSON.stringify(body));
        console.log('httpResponse: ' + JSON.stringify(httpResponse));

        cb(null, true);
      });

    },

    function(statement, cb){
      var url = '/backup/data_client/' + token + '/' + category_id_str + '/' + client_id;
      uriSegment = {
        type: 'api',
        url: url,
        method: 'DELETE'
      },
      items = {};

      restService(uriSegment, items, function(resp){
        console.log('/backup/data_client');
        console.log(JSON.stringify(resp));
        console.log('################################################################################');
        if(!resp.success){
          return cb(resp.data, null)
        }

        return cb(null, resp.data);
      });
    },
    function(statement, cb){
      var url = '/elastic/data_client/' + token + '/' + category_id_str + '/' + client_id;
      uriSegment = {
        type: 'api',
        url: url,
        method: 'DELETE'
      },
      items = {};

      restService(uriSegment, items, function(resp){
        console.log('/elastic/data_client');
        console.log(JSON.stringify(resp));
        console.log('################################################################################');
        if(!resp.success){
          return cb(resp.data, null)
        }

        return cb(null, resp.data);
      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.addKeyword = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      category_id_str = (typeof req.body.category_id !== 'undefined') ? req.body.category_id : '',
      keyword = (typeof req.body.keyword !== 'undefined') ? req.body.keyword : '',
      usr_uid = req.user.user_detail.usr_uid,
      current_time = new Date(),
      uriSegment = {
        type: 'api',
        url: '/insert/tb_keyword',
        method: 'POST'
      },
      dateFormatter = function(tgl){
        return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
      },
      date_from = dateFormatter(new Date(req.body.date_from)) || dateFormatter(new Date()),
      date_to = dateFormatter(new Date(req.body.date_to)) || dateFormatter(new Date());

  current_time = moment(current_time).format("YYYY-MM-DD HH:mm:ss");

  var items = {
    token: token,
    client_id: client_id,
    category_id: category_id_str,
    keyword: encodeURIComponent(keyword),
    input_data_date: current_time,
    pc_name: os.hostname(),
    usere: usr_uid.toString(),
  };

  async.waterfall([
    function(cb){
      restService(uriSegment, items, function(resp){
        if(!resp.success){
          return cb('Error insert to tb_keyword');
        }

        cb(null, true);
      });
    },
    function(resp, cb){
      uriSegment = {
        type: 'api',
        url: '/insert/tb_run_backtrack',
        method: 'POST'
      },
      items = {
        token: token,
        client_id: client_id,
        category_id: category_id_str,
        keyword: keyword,
        time_frame: "0",
        date_from: date_from,
        date_to: date_to,
        pc_name: os.hostname(),
        usere: usr_uid,
        flag: current_time,
        statuse: 'Q'
      };

      restService(uriSegment, items, function(resp){
        cb(null, true);
      });
    },
    function(resp, cb){
      var req;
      req = {
        uri: global.botApiGet + 'updatekeywordallserver',
        method: 'GET'
      };

      request(req, function(err, httpResponse, body){
        console.log(global.botApiGet + 'updatekeywordallserver');
        console.log('body: ' + JSON.stringify(body));
        console.log('httpResponse: ' + JSON.stringify(httpResponse));

        cb(null, true);
      });

    },
    function(resp, cb){
      var req;
      req = {
        uri: global.botApiGet + 'updateclientallserver',
        method: 'GET'
      };

      request(req, function(err, httpResponse, body){
        console.log(global.botApiGet + 'updateclientallserver');
        console.log('body: ' + JSON.stringify(body));
        console.log('httpResponse: ' + JSON.stringify(httpResponse));

        cb(null, true);
      });

    },
    function(resp, cb){
      req = {
        uri: global.botApiPost + 'serachbacktrack',
        method: 'POST',
        formData: {
          keyword: keyword,
          start_date: date_from,
          to_date: date_to,
          category_id: category_id_str,
          client_id: client_id
        }
      };

      setTimeout(function(){
        request(req, function(err, httpResponse, body){
          console.log(global.botApiPost + 'serachbacktrack');
          console.log('body: ' + JSON.stringify(body));
          console.log('httpResponse: ' + JSON.stringify(httpResponse));
          httpResponse = JSON.parse(JSON.stringify(httpResponse));
          // if(err){
          //   console.log('err: ' + JSON.stringify(err));
          //   res.status(400).send(resp.data);
          //   return;
          // }

          cb(null, httpResponse);
        });
      }, 20000);
    }
  ],
  function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.deleteKeyword = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      // category_id_str = (typeof req.body.category_id_str !== 'undefined') ? req.body.category_id_str : '',
      // keyword = (typeof req.body.keyword !== 'undefined') ? req.body.keyword : '';
      category_id_str = (typeof req.query.category_id_str !== 'undefined') ? req.query.category_id_str : '',
      keyword = (typeof req.query.keyword !== 'undefined') ? decodeURIComponent(req.query.keyword) : '';

  async.waterfall([
    function(cb){

      var uriSegment = {
        type: 'api',
        url: '/delete/tb_keyword',
        method: 'POST'
      };

      var items = {
        client_id: client_id,
        category_id_str: category_id_str,
        token: token,
        keyword: keyword
      };

      restService(uriSegment, items, function(resp){
        if(!resp.success){
          // res.status(400).send(resp.data);
          // return;
          cb('error deleting tb_keyword', null);
        }

        // return res.status(200).send(resp.data);
        cb(null, resp);
      });

    },
    function(result, cb){

      var uriSegment = {
        type: 'api',
        url: '/delete/tb_run_backtrack',
        method: 'POST'
      };

      var items = {
        client_id: client_id,
        category_id_str: category_id_str,
        token: token,
        keyword: keyword
      };

      restService(uriSegment, items, function(resp){
        if(!resp.success){
          // res.status(400).send(resp.data);
          // return;
          cb('error deleting tb_run_backtrack');
        }

        // return res.status(200).send(resp.data);
        cb(null, resp);
      });

    }
  ],
  function(err, result){
    if(err){
      res.status(400).send(result.data);
      return;
    }

    return res.status(200).send(result.data);
  });
};

exports.updateMediaIdList = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      media_set = (typeof req.body.media_set_id !== 'undefined') ? req.body.media_set_id.toString() : '',
      media_list = (typeof req.body.list !== 'undefined') ? req.body.list : [],
      usr_uid = req.user.user_detail.usr_uid.toString(),
      current_time = new Date(),
      uriSegment = {
        type: 'api',
        url: '/delete/tb_media_set_choosen',
        method: 'POST'
      };

  var selectedMedia = [],
      items = {
        client_id: client_id,
        media_set_id: media_set,
        token: token
      },
      current_time = moment(current_time).format("YYYY-MM-DD HH:mm:ss");

  for(var i=0;i<media_list.length;i++){
    var ml = media_list[i].media_list;
    for(var j=0;j<ml.length;j++){
      if(ml[j].chosen){
        selectedMedia.push(ml[j].media_id);
      }
    }
  }

  restService(uriSegment, items, function(resp){
    // if(!resp.success){
    //   res.status(400).send(resp.data);
    //   return;
    // }

    uriSegment = {
      type: 'api',
      url: '/insert/tb_media_set_choosen',
      method: 'POST'
    }, items = {
      client_id: client_id,
      user_media_type_id: media_set,
      token: token,
      pc_name: os.hostname(),
      usere: usr_uid,
      user_media_id: selectedMedia,
      input_data_date: current_time
    };

    restService(uriSegment, items, function(resp){
      if(!resp.success){
        res.status(400).send(resp.data);
        return;
      }

      return res.status(200).send(resp.data);
    });
  });
};

exports.updateCategoryIdList = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      category_set = (typeof req.body.category_set !== 'undefined') ? req.body.category_set.toString() : '',
      category_list = (typeof req.body.list !== 'undefined') ? req.body.list : [],
      usr_uid = req.user.user_detail.usr_uid.toString(),
      current_time = new Date(),
      uriSegment = {
        type: 'api',
        url: '/delete/tb_category_set',
        method: 'POST'
      };

  var selectedCategory = [],
      items = {
        client_id: client_id,
        category_set: category_set,
        token: token
      },
      current_time = moment(current_time).format("YYYY-MM-DD HH:mm:ss");

  for(var i=0;i<category_list.length;i++){
    if(category_list[i].chosen){
      selectedCategory.push(category_list[i].category_id);
    }
  }

  restService(uriSegment, items, function(resp){
    // if(!resp.success){
    //   res.status(400).send(resp.data);
    //   return;
    // }

    uriSegment = {
      type: 'api',
      url: '/insert/tb_category_set',
      method: 'POST'
    }, items = {
      client_id: client_id,
      category_set: category_set,
      token: token,
      pc_name: os.hostname(),
      usere: usr_uid,
      category_id: selectedCategory,
      data_input_date: current_time,
      default_set: ''
    };

    restService(uriSegment, items, function(resp){
      if(!resp.success){
        res.status(400).send(resp.data);
        return;
      }

      return res.status(200).send(resp.data);
    });
  });
};

exports.addCategorySet = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      descriptionz = (typeof req.body.descriptionz !== 'undefined') ? req.body.descriptionz : '',
      usr_uid = req.user.user_detail.usr_uid.toString(),
      current_time = new Date(),
      uriSegment = {
        type: 'api',
        url: '/insert/tb_category_set_type',
        method: 'POST'
      };

  var items = {
    descriptionz: descriptionz,
    client_id: client_id,
    uid: usr_uid,
    client_ip: os.hostname(),
    token: token
  };

  restService(uriSegment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }

    return res.status(200).send(resp.data);
  });
};

exports.deleteCategorySet = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      category_set = (typeof req.query._id !== 'undefined') ? req.query._id : '',
      uriSegment = {
        type: 'api',
        url: '/delete/tb_category_set_type',
        method: 'POST'
      }, items = {
        client_id: client_id,
        token: token,
        category_set: category_set.toString()
      };

  async.waterfall([
    function(cb){
      restService(uriSegment, items, function(resp){
        if(!resp.success){
          cb(resp.data, null);
          return;
        }

        cb(null, resp.data);
      });
    },
    function(res, cb){
      uriSegment = {
        type: 'api',
        url: '/delete/tb_category_set',
        method: 'POST'
      };

      restService(uriSegment, items, function(resp){
        if(!resp.success){
          cb(resp.data, null);
          return;
        }

        cb(null, resp.data);
      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.addMediaSet = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      media_name = (typeof req.body.media_name !== 'undefined') ? req.body.media_name : '',
      usr_uid = req.user.user_detail.usr_uid.toString(),
      current_time = new Date(),
      uriSegment = {
        type: 'api',
        url: '/insert/tb_media_set',
        method: 'POST'
      };

  var items = {
    media_name: media_name,
    client_id: client_id,
    uid: usr_uid,
    client_ip: os.hostname(),
    token: token
  };

  restService(uriSegment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }

    return res.status(200).send(resp.data);
  });
};

exports.deleteMediaSet = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      media_set = (typeof req.query._id !== 'undefined') ? req.query._id : '',
      uriSegment = {
        type: 'api',
        url: '/delete/tb_media_set',
        method: 'POST'
      };

  var items = {
    client_id: client_id,
    token: token,
    media_set_id: media_set,
    media_name: ''
  };

  restService(uriSegment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }

    return res.status(200).send(resp.data);
  });
};