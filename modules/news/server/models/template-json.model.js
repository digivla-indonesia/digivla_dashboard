exports.getTableList = function(item){
	var params = {
		"size": 0,
		"sort": [{
			"datee": {
				"order": "desc"
			}
		}],
		"query": {
			"constant_score": {
				"filter": {
					"bool": {
						"must": [{
							"terms": {
								"media_id": item.media
							}
						}, {
							"range": {
								"datee": {
									"gte": item.date_from,
									"lte": item.date_to,
									"format": "yyyy-MM-dd||yyyy-MM-dd"
								}
							}
						}],
						"should": item.category
					}
				}
			}
		},
		"aggs": {
			"datagroup": {
				"terms": {
					"field": "article_id",
					"size": 0
				},
				"aggs": {
					"category": {
						"top_hits": {
							"sort": [
								{
									"datee": {
										"order": "desc"
									}
								}
							],
							"_source": {
								"include": ["id","article_id","category_id"]
							}
						}
					}
				}
			}
		}
	};

	return params;
};


exports.getEmailContent = function(items){
	var params = {
	"size": 0,
		"sort": [{
			"datee": {
				"order": "desc"
			}
		}],
		"query": {
			"constant_score": {
				"filter": {
					"bool": {
						"must": [{
							"terms": {
								"article_id": items.articles
							}
						}]
					}
				}
			}
		},
		"aggs": {
			"datagroup": {
				"terms": {
					"field": "article_id",
					"size": 0
				},
				"aggs": {
					"category": {
						"top_hits": {
							"sort": [
								{
									"data_input_date": {
										"order": "desc"
									}
								}
							]
						}
					}
				}
			}
		}
	};

	return params;
};