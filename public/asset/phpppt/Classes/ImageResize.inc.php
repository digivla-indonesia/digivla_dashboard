<?php
/**
 * @author Dodit Suprianto
 * Email: d0dit@yahoo.com
 * Website: http://doditsuprianto.com
 * 
 * Nama file: ImageResize.inc
 * 
 * This class to resize an image from bigger to smaller size.
 * Supporting PNG, JPG, and GIF image format.
 * 
 * if you chose $proportional=true, it means that width or height image is proportional, example:
 * $r = new Resize("test.jpg", 0, 500, true); height image will be 500 pixel and width will be proportional
 * $r = new Resize("test.jpg", 500, 0, true); width image will be 500 pixel and height will be proportional
 * 
 * if you chose $proportional=false, it means the width and height image will be customizable, example:
 * $r = new Resize("test.jpg", 500, 500, true); it forces the image size will be 500 pixel width and 500 pixel height
 * 
 */

	class Resize
	{
		private $file_name;		
		private $file_source;		
		private $width_resize;
		private $height_resize;	
		private $proportional;
		
		public function __construct($file_source, $file_name, $width_resize, $height_resize, $proportional)
		{
			$this->file_source = $file_source;
			$this->file_name = $file_name;
			$this->width_resize = $width_resize;
			$this->height_resize = $height_resize;			
			$this->proportional = $proportional;
		}
		
		public function setProportional($proportional)
		{
			$this->proportional = $proportional;
		}
		
		public function setFileSource($file_source)
		{
			$this->file_source = $file_source;
		}
		
		public function setHeightResize($height_resize)
		{
			$this->height_resize = $height_resize;
		}
		
		public function setWidthResize($width_resize)
		{
			$this->width_resize = $width_resize;
		}
		
		private function MemoryUsage()
		{
			$imageInfo    = getimagesize($this->file_source); 
			$memoryNeeded = round(($imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + Pow(2, 16)) * 1.65);
			  
			$memoryLimit = (int) ini_get('memory_limit')*1048576;
			if ((memory_get_usage() + $memoryNeeded) > $memoryLimit)
			ini_set('memory_limit', ceil((memory_get_usage() + $memoryNeeded + $memoryLimit)/1048576).'M');
		}
				
		public function ImageResize()
		{
			$this->MemoryUsage();
			if ( $this->height_resize <= 0 && $this->width_resize <= 0 ) return false;
       
			$info = getimagesize($this->file_source);
		    $image = '';
		      
			$final_width = 0;
			$final_height = 0;
			list($width_old, $height_old) = $info;
		      
			if ($this->proportional) 
			{		          
				$proportion = $width_old / $height_old;
		          
				if ( $this->width_resize > $this->height_resize && $this->height_resize != 0) 
				{
					$final_height = $this->height_resize;
					$final_width = $final_height * $proportion;
				}
				elseif ( $this->width_resize < $this->height_resize && $this->width_resize != 0) 
				{
					$final_width = $this->width_resize;
					$final_height = $final_width / $proportion;
				}
				elseif ( $this->width_resize == 0 ) 
				{
					$final_height = $this->height_resize;
					$final_width = $final_height * $proportion;           
				}
				elseif ( $this->height_resize == 0) 
		        {
					$final_width = $this->width_resize;
					$final_height = $final_width / $proportion;           
				}
				else 
				{
					$final_width = $this->width_resize;
					$final_height = $this->height_resize;
				}
			}
			else 
			{
				$final_width = ($this->width_resize <= 0) ? $this->width_resize_old : $this->width_resize;
				$final_height = ($this->height_resize <= 0) ? $this->height_resize_old : $this->height_resize;
			}
		  
			switch ( $info[2] ) 
			{
				case IMAGETYPE_GIF:
					$image = imagecreatefromgif($this->file_source);
				break;
				case IMAGETYPE_JPEG:
					$image = imagecreatefromjpeg($this->file_source);
				break;
				case IMAGETYPE_PNG: 					
					$image = imagecreatefrompng($this->file_source);
				break;
				default:
					return false;
			}
		      
			$image_resized = imagecreatetruecolor( $final_width, $final_height );
			imagecolortransparent($image_resized, imagecolorallocate($image_resized, 0, 0, 0) );
			imagealphablending($image_resized, false);
			imagesavealpha($image_resized, true);
		       		      
			imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
		      		      
			switch ( $info[2] ) 
			{
				case IMAGETYPE_GIF:					
					imagegif($image_resized,  '../images/'.$this->file_name, 100);
				break;
				case IMAGETYPE_JPEG:
					imagejpeg($image_resized, '../images/'.$this->file_name, 100);
				break;
				case IMAGETYPE_PNG:					
					imagepng($image_resized, '../images/'.$this->file_name, 9);
				break;
				default:
					return false;
			}		      
			return true;	
		}
	}
?>
