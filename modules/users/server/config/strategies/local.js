'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  request = require('request'),
  crypto = require('crypto');

module.exports = function () {
  // Use local strategy
  passport.use(new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password'
    }, function(username, password, done){

      var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
      var req = {
        uri: global.apiAddress + '/login',
        method: 'POST',
        headers: {
            Authorization: 'Basic ' + auth,
            'Content-Type': 'application/json'
        },
        json: {
          'usr_login': username,
          'password': crypto.createHash('md5').update(password).digest("hex")
        }
      };

      request(req, function(err, httpResponse, body){
        if(err){
          return done(err);
        }

        if(body.status === 'failed'){
          return done(null, false, {
            message: 'Invalid username or password'
          });
        }

        var resultVar = JSON.parse(JSON.stringify(body));
        return done(null, resultVar);
      });
    }
  ));

};
