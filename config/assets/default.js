'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/bootstrap/dist/css/bootstrap.css',
        'public/lib/bootstrap/dist/css/bootstrap-theme.css',
        'public/lib/angular-loading-bar/src/loading-bar.css',
        /* Add asset */
        'public/asset/dist/css/main.css',
        'public/asset/dist/css/responsive.css',
        'public/asset/plugins/select/chosen.css',
        'public/asset/plugins/scroll/jquery.mCustomScrollbar.css',
        '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.css',
        
        'public/asset/plugins/mobile/component.css',
        'public/asset/plugins/table/css/theme.blue.css',
        'public/asset/css/table.css',
        'public/asset/css/colorbox/colorbox.css',
        'public/asset/css/ui/jquery-ui-1.9.2.custom.min.css',
        'https://cdn.rawgit.com/gilf/ngPrint/master/ngPrint.min.css',

        /*test ngTable*/
        'public/lib/ng-table/bundles/ng-table.min.css'
      ],
      js: [
        'public/asset/plugins/vendor/jquery-1.11.3.min.js',

        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',
        'public/lib/angular-animate/angular-animate.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/angular-ui-utils/ui-utils.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
        'public/lib/angular-file-upload/angular-file-upload.js',
        'public/lib/highcharts/adapters/standalone-framework.js',
        'public/lib/highcharts/highcharts.js',
        'public/lib/highcharts-ng/dist/highcharts-ng.js',
        'public/lib/angular-loading-bar/src/loading-bar.js',
        /* Add asset */
        'public/asset/plugins/mobile/modernizr.custom.js',
        'public/asset/plugins/date/jquery.plugin.js',
        'public/asset/plugins/dom/jquery.fancybox.js',

        'public/asset/plugins/select/chosen.jquery.js',
        'public/asset/plugins/oloader/js/jquery.oLoader.min.js',
        'public/asset/plugins/mobile/jquery.dlmenu.js',
        'public/asset/plugins/scroll/jquery.mCustomScrollbar.concat.min.js',
        'public/asset/javascript/new/jquery.hoverIntent.min.js',
        'public/asset/javascript/new/jquery.mb.flipText.js',
        'public/asset/javascript/core/jquery-ui-1.9.2.custom.min.js',
        'public/asset/javascript/core/jquery.printElement.min.js',
        'public/asset/plugins/pathseg/pathseg.js',
        'public/lib/pdfmake/build/pdfmake.min.js',
        'public/lib/pdfmake/build/vfs_fonts.js',

        // angular-file-saver
        'public/lib/angular-file-saver/dist/angular-file-saver.bundle.min.js',

        // angular-print
        'https://rawgit.com/gilf/ngPrint/master/ngPrint.min.js',

        // xslx
        'public/lib/js-xlsx/dist/xlsx.full.min.js',
        'http://unpkg.com/angular-js-xlsx/angular-js-xlsx.js',

        /*test ngTable*/
        'public/lib/ng-table/bundles/ng-table.min.js'
      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    css: [
      'modules/*/client/css/*.css'
    ],
    less: [
      'modules/*/client/less/*.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    views: ['modules/*/client/views/**/*.html']
  },
  server: {
    gruntConfig: 'gruntfile.js',
    gulpConfig: 'gulpfile.js',
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: 'modules/*/server/config/*.js',
    policies: 'modules/*/server/policies/*.js',
    views: 'modules/*/server/views/*.html'
  }
};
