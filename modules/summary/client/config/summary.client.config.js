'use strict';

// Configuring the Articles module
angular.module('summary', ['angular-loading-bar', 'ngTable']).run(['Menus',
    function(Menus) {
        // Add the articles dropdown item
        Menus.addMenuItem('topbar', {
            title: 'Editing',
            state: 'summary'
        });
    }
]);
