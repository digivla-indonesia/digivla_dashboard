var config = {
  '.chosen-select-no-single': { disable_search_threshold: 10, disable_search: true }
}

for (var selector in config) {
  $(selector).chosen(config[selector]);
}
