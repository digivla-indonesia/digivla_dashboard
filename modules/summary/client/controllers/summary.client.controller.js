'use strict';

angular.module('summary').controller('SummaryController', ['$scope', '$rootScope', '$http', '$timeout', '$sce', '$filter', '$q', 'NgTableParams', 'Authentication',
  function($scope, $rootScope, $http, $timeout, $sce, $filter, $q, NgTableParams, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    // Pagination
    $scope.currentPage = 1;
    $scope.maxSize = 5;

    $scope.clear = function() {
      $scope.dateFrom = null;
      $scope.dateTo = null;
    };

    $timeout(function() {
      $scope.$on('reloadChart', function() {
        $scope.tableData = [];
        $scope.loadingData = true;
        $scope.getTableDataError = false;
        $scope.selectedAll = false;
        $scope.selection = [];
        getTableData();
      });
    }, 0);

    // Call getTableData()
    $scope.initData = function(){
      $timeout(function(){
        $scope.selectedAll = false;
        $scope.selection = [];
        $scope.tableData = [];
        $scope.loadingData = true;
        getTableData();
      }, 1500);
    };

    var td = [];
    var deferred = $q.defer();
    var filterBucket = false;
    $scope.tableData;
    $scope.totalArticles = 0;

    function getTableData(val) {
      var from = ($rootScope.dateFrom !== '') ? new Date($scope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($scope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var datas = {
        time_frame: $rootScope.selectedPeriodOptions.value,
        date_from: from,
        date_to: to,
        category: $rootScope.selectedGroupCategory.category_set,
        category_id: $rootScope.selectedSubCategory.category_id,
        media: $rootScope.selectedGroupMedia.media_set_id,
        media_id: $rootScope.selectedMediaSelection.user_media_id,
        offset: val,
        limit: $scope.selectedLimit
      }

      // $http.post('/api/summary/gettablelist', datas)
      //   .success(function(response) {
      //     $scope.tableData = response.data;
      //     $scope.totalItems = response.pagination.total_articles;
      //     $scope.selectedAll = checkCheckedAll();
      //     $scope.loadingData = false;
      //   }).error(function(errResponse) {
      //     $scope.getTableDataError = true;
      //     $scope.tableData = null;
      //     $scope.loadingData = false;
      //   });
      $scope.tableData = new NgTableParams({
        page: 1,
        count: 25,
        sorting: {
          datee: 'desc'     
        }
      }, {
        total: td.length,
        getData: function(params){
          var tdfrom = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
            tdto = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

          if (tdfrom !== '' && tdto !== '') {
            tdfrom = tdfrom.getFullYear() + '-' + ('0' + (tdfrom.getMonth() + 1)).slice(-2) + '-' + ('0' + tdfrom.getDate()).slice(-2);
            tdto = tdto.getFullYear() + '-' + ('0' + (tdto.getMonth() + 1)).slice(-2) + '-' + ('0' + tdto.getDate()).slice(-2);
          }
          
          if(td.length > 0 && $rootScope.selectedPeriodOptions.value == filterBucket.time_frame && tdfrom == filterBucket.date_from && tdto == filterBucket.date_to && $rootScope.selectedGroupCategory.category_set == filterBucket.category && $rootScope.selectedSubCategory.category_id == filterBucket.category_id && $rootScope.selectedGroupMedia.media_set_id == filterBucket.media && $rootScope.selectedMediaSelection.user_media_id == filterBucket.media_id ){
            td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
            var searchedData = searchData();
            params.total(searchedData.length);
            return searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
          } else{
            filterBucket = datas;
            return $http.post('/api/summary/gettablelist', datas).then(function(response){
              var results = response.data;
              td = results.data;
              td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
              var searchedData = searchData();
              $scope.totalArticles = searchedData.length;
              params.total(searchedData.length);
              return searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            });
          }
        },
        $scope: { $data: {} }
      });

      return;
    }

    $scope.$watch("searchTable", function () {
      if($scope.tableData)
        $scope.tableData.reload();
    });

    function searchData(){
      if($scope.searchTable)
         return $filter('filter')(td,$scope.searchTable);
      return td;
    }


    // // checkbox function
    // $scope.toggleSelection = function toggleSelection(article) {
    //   var found = false;
    //   var index = null;
      
    //   if($scope.selection.length > 0){
    //     for(var i=0;i<$scope.selection.length;i++) {
    //       if($scope.selection[i].article_id.toString() === article.article_id.toString() && ($scope.selection[i].category_id.length == article.categories.length && $scope.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && ($scope.selection[i]._id.length == article._id.length && $scope.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
    //           index = i;
    //           break;
    //       }
    //     }
    //   }

    //   // is currently selected
    //   if(index !== null){
    //     $scope.selection.splice(index, 1);
    //   // is newly selected
    //   } else{
    //     $scope.selection.push({
    //       _id: article._id,
    //       article_id: article.article_id,
    //       category_id: article.categories
    //     });
    //   }

    //   if($scope.selection.length > 0){
    //     $scope.showButtonEmail = true;
    //   } else{
    //     $scope.showButtonEmail = false;
    //   }
    // };

    // $scope.checkSelectionExists = function(article){
    //   var found = false;
    //   for(var i=0;i<$scope.selection.length;i++) {
    //     // if($scope.selection[i].article_id.toString() === article.article_id.toString() && $scope.selection[i].category_id === article.category_id){
    //     //   found = true;
    //     //   break;
    //     // }
    //     if($scope.selection[i].article_id.toString() === article.article_id.toString() && ($scope.selection[i].category_id.length == article.categories.length && $scope.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && ($scope.selection[i]._id.length == article._id.length && $scope.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
    //       found = true;
    //       break;
    //     }
    //   }

    //   return found;
    // };

    // function checkCheckedAll(){
    //   var result = false;
    //   var counterFlag = 0;

    //   if($scope.selection.length > 0){
    //     angular.forEach($scope.tableData, function (value, key) {
    //       for(var i=0;i<$scope.selection.length;i++){
    //         if(value.article_id.toString() === $scope.selection[i].article_id.toString()){
    //           // $scope.selection.splice(i, 1);
    //           ++counterFlag;
    //           break;
    //         }
    //       }
    //     });
    //   }

    //   if(counterFlag == $scope.selectedLimit){
    //     result = true;
    //   }

    //   return result;
    // };

    // $scope.checkAll = function(){
    //   if ($scope.selectedAll) {
    //     $scope.selectedAll = true;
    //     $scope.showButtonEmail = true;
    //     // $scope.selection = [];
    //     angular.forEach(td, function (value, key) {
    //       if(!$scope.checkSelectionExists(value)){
    //         $scope.selection.push({
    //           _id: value._id,
    //           article_id: value.article_id,
    //           category_id: value.categories
    //         });
    //       }
    //     });
    //   } else {
    //     $scope.selectedAll = false;
    //     $scope.showButtonEmail = false;
    //     // for(var j=0;j<$scope.tableData.length;j++){
    //     //   for(var i=0;i<$scope.selection.length;i++){
    //     //     if($scope.tableData[j].article_id.toString() === $scope.selection[i].article_id.toString()){
    //     //       $scope.selection.splice(i, 1);
    //     //       break;
    //     //     }
    //     //   }
    //     // }
    //     $scope.selection = [];
    //   }
    // };
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(article) {
      var found = false;
      var index = null;
      
      if($scope.selection.length > 0){
        for(var i=0;i<$scope.selection.length;i++) {
          if($scope.selection[i].article_id.toString() === article.article_id.toString() && ($scope.selection[i].category_id.length == article.categories.length && $scope.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && ($scope.selection[i]._id.length == article._id.length && $scope.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
              index = i;
              break;
          }
        }
      }

      // is currently selected
      if(index !== null){
        $scope.selection.splice(index, 1);
      // is newly selected
      } else{
        $scope.selection.push({
          _id: article._id,
          article_id: article.article_id,
          category_id: article.categories
        });
      }

      if($scope.selection.length > 0){
        $scope.showButtonEmail = true;
      } else{
        $scope.showButtonEmail = false;
      }
    };

    $scope.checkSelectionExists = function(article){
      var found = false;
      for(var i=0;i<$scope.selection.length;i++) {
        // if($scope.selection[i].article_id.toString() === article.article_id.toString() && $scope.selection[i].category_id === article.category_id){
        //   found = true;
        //   break;
        // }
        if($scope.selection[i].article_id.toString() === article.article_id.toString() && ($scope.selection[i].category_id.length == article.categories.length && $scope.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && ($scope.selection[i]._id.length == article._id.length && $scope.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
          found = true;
          break;
        }
      }

      return found;
    };

    function checkCheckedAll(){
      var result = false;
      var counterFlag = 0;

      if($scope.selection.length > 0){
        angular.forEach($scope.tableData, function (value, key) {
          for(var i=0;i<$scope.selection.length;i++){
            if(value.article_id.toString() === $scope.selection[i].article_id.toString()){
              // $scope.selection.splice(i, 1);
              ++counterFlag;
              break;
            }
          }
        });
      }

      if(counterFlag == $scope.selectedLimit){
        result = true;
      }

      return result;
    };

    $scope.checkAll = function(){
      if ($scope.selectedAll) {
        $scope.selectedAll = true;
        $scope.showButtonEmail = true;
        // $scope.selection = [];
        angular.forEach(td, function (value, key) {
          if(!$scope.checkSelectionExists(value)){
            $scope.selection.push({
              _id: value._id,
              article_id: value.article_id,
              category_id: value.categories
            });
          }
        });
      } else {
        $scope.selectedAll = false;
        $scope.showButtonEmail = false;
        // for(var j=0;j<$scope.tableData.length;j++){
        //   for(var i=0;i<$scope.selection.length;i++){
        //     if($scope.tableData[j].article_id.toString() === $scope.selection[i].article_id.toString()){
        //       $scope.selection.splice(i, 1);
        //       break;
        //     }
        //   }
        // }
        $scope.selection = [];
      }
    };

    $scope.saveArticle = function(articleDetail){
      $http.get('/api/summary/getcategorylist').success(function(data){
        $scope.categoryLists = data;
        $.fancybox.open({href: "#save-article"});
      }).error(function(data){
        alert(data);
      });
    };

    $scope.doSaveArticle = function(){
      var selectedArticle = $scope.articleDetail;
      selectedArticle.categories = $scope.chosenCategory;

      $http.post('/api/summary/savearticle', selectedArticle).success(function(data){
        alert('success');
        $scope.chosenCategory = [];
        $.fancybox.close();
      }).error(function(data){
        alert('failed');
      });
    };

    $scope.chosenCategory = [];
    $scope.addCategory = function(val){
      var existsFlag = $scope.chosenCategory.indexOf(val);
      if(existsFlag !== -1){
        $scope.chosenCategory.splice(existsFlag, 1);
      } else{
        $scope.chosenCategory.push(val);
      }
    };


    $scope.deleteArticles = function(){
      $.fancybox.open({href: '#dialog-form-delete-many'});
    };

    // $scope.deleteArticle = function(article, type, index){
    //   $scope.selectedIndex = index;
    //   $scope.selection = [];
    //   if(type === 0){
    //     $scope.selection = [{
    //       id: article.id,
    //       article_id: article.article_id,
    //       category_id: article.category_id
    //     }];
    //   } else{
    //     $scope.selection = [{
    //       id: article.id,
    //       article_id: article.article_id,
    //       category_id: article.categories[0]
    //     }];
    //   }
    //   $.fancybox.open({href: '#dialog-form-delete-single'});
    // };    

    // $scope.doDeleteArticles = function(){
    //   if($scope.selection.length > 0){
    //     $http.post('/api/summary/deletearticle', {articles: $scope.selection}).success(function(data){
    //       // delete $scope.tableData[$scope.selectedIndex];
    //       $scope.selectedIndex = null;
    //       $scope.selection = [];
    //       $scope.selectedAll = false;
    //       $scope.showButtonEmail = false;
    //       $scope.initData();
    //       $.fancybox.close();
    //     }).error(function(data){
    //       alert(data);
    //       $scope.selection = [];
    //       $scope.selectedAll = false;
    //       $scope.showButtonEmail = false;
    //       $.fancybox.close();
    //     });
    //   }
    // };
    $scope.deleteArticle = function(article){
      $scope.selection = [];
      $scope.selection = [{
        _id: article._id,
        article_id: parseInt(article.article_id),
        category_id: article.categories
      }];
      $.fancybox.open({href: '#dialog-form-delete-single'});
    };    

    $scope.doDeleteArticles = function(){
      // hapus artikel disini
      // return alert(JSON.stringify($scope.selection));
      if($scope.selection.length > 0){
        $http.post('/api/summary/deletearticle', {articles: $scope.selection}).success(function(data){
          $scope.selectedIndex = null;
          $scope.selection = [];
          $scope.selectedAll = false;
          $scope.showButtonEmail = false;
          td = [];
          // $scope.currentPage = 1;
          $scope.initData();
          $.fancybox.close();
        }).error(function(data){
          alert(data);
          $scope.selection = [];
          $scope.selectedAll = false;
          $scope.showButtonEmail = false;
          $.fancybox.close();
        });
      }
    };


    $scope.pageChanged = function() {
      $scope.offset = ($scope.currentPage - 1) * $scope.selectedLimit;
      $scope.loadingData = true;
      $scope.tableData = [];
      getTableData($scope.offset);
    };

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.getDetailArticle = function(val, mn, categories) {
      var url = '/api/dashboards/getarticledetail?_id=' + val;
      $scope.mediaHTML = "";
      $http.get(url).success(function(response) {
        
        var results = response.data.data;

        var keywords = results.keywords;
        var selectedKeyword;

        // var res = results.content.split(" ");
        // for(var i=0;i<res.length;i++){
        //   for(var j=0;j<keywords.length;j++){
        //     selectedKeyword = keywords[j].split(" ");
        //     for(var k=0;k<selectedKeyword.length;k++){
        //       if(res[i].toLowerCase() == selectedKeyword[k].toLowerCase()){
        //         res[i] = "<span style='color:#f89406; font-weight:bolder;'>" + res[i] + "</span>";
        //       } else{
        //         var a = res[i].toLowerCase().indexOf(selectedKeyword[k].toLowerCase());
        //         if(a != -1){
        //             var separatedString = "<span style='color:#f89406; font-weight:bolder;'>" + res[i].substr(a, selectedKeyword[k].length) + "</span>";
        //             var b = res[i].replace(res[i].substr(a, selectedKeyword[k].length), separatedString);
        //             res[i] = b;
        //         }
        //       }

        //     }
        //   }
        // }
        // results.content = res.join(" ");

        var temp;
        var x, y;
        for(var j=0;j<keywords.length;j++){
          selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
          for(var k=0;k<selectedKeyword.length;k++){
            x = selectedKeyword[k];
            x = x.replace(/\"/g, "");
            x = x.replace(/\'/g, "");

            temp = results.content.replace(new RegExp('('+x+')', "ig"), "<span style='color:#f89406; font-weight:bolder;'>" + x + "</span>");
            results.content = temp;
          }
        }

        results.content = results.content.replace(/(?:\r\n|\r|\n)/g, '<br />');
        
        if(results.file_pdf.indexOf('.mp4') !== -1){
          var media_type = (results.file_pdf.indexOf('.mp3') !== -1) ? 'media_radio' : 'media_tv';
          var file = response.data.data.file_pdf;
          var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);

          $scope.mediaHTML = '<video width="25%" controls>' +
            '<source src="' + 'http://pdf.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="video/mp4">' +
            'Your browser does not support the video tag.' +
          '</video>';

        } else if(results.file_pdf.indexOf('.mp3') !== -1){
          var file = response.data.data.file_pdf;
          var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);

          $scope.mediaHTML = '<audio width="25%" controls>' +
            '<source src="' + 'http://pdf.antara-insight.id/media_radio/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="audio/mpeg">' +
            'Your browser does not support the audio tag.' +
          '</audio>';
        } else{
          results.file_pdf = results.file_pdf;
        }

        $scope.articleDetail = results;
        $scope.articleDetail.media_name = mn;
        $scope.articleDetail.selectedCategories = categories;

        $.fancybox.open({href: '#dd1'});
      }).error(function(response) {
        $scope.articleDetail = null;
      });
    };

    $scope.editSummary = function(val, dest) {
      $scope.updateValue = val.tone;
      $scope.selectedArticleData = val;
      $.fancybox.open({href: '#' + dest});
    };

    $scope.doUpdateSummary = function(){
      $scope.selectedArticleData.activity = 'summary_edited';
      $http.post('/api/summary/editsummary', $scope.selectedArticleData)
        .success(function (response) {
          $.fancybox.close();
        })
        .error(function (errResponse) {
          alert('Failed to edit');
        });
    };

    $scope.doUpdateTonality = function(){
      $scope.selectedArticleData.activity = 'tone_edited';
      // return console.log(JSON.stringify($scope.selectedArticleData));
      $http.post('/api/summary/editdataclient', $scope.selectedArticleData)
        .success(function (response) {
          $.fancybox.close();
        })
        .error(function (errResponse) {
          alert(JSON.stringify(errResponse));
        });
    };

    // limit
    $scope.showLimit = [10, 20, 50, 100];
    $scope.selectedLimit = $scope.showLimit[1];

    $scope.limitChanging = function(){
      $scope.currentPage = 1;
      $scope.totalItems = null;
      $scope.loadingData = true;
      $scope.offset = 0;
      $scope.tableData = [];
      getTableData();
    };
  }
]);
