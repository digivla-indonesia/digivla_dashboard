var config = {
	'.chosen-select-no-single' : {disable_search_threshold:10,disable_search:true}
}
for (var selector in config) {
	$(selector).chosen(config[selector]);
}

$(function() {
    /* POTENTIAL GAUGE CHARTS */
    var pgchart = c3.generate({
        bindto: '#potential-gauge',
        data: {
            columns: [
                ['data', 30]
            ],
            type: 'gauge',
            onclick: function (d, i) { console.log("onclick", d, i); },
            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        },
        gauge: {
            //    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
            //    max: 100, // 100 is default
            label: {
                format: function(value, ratio) {
                    return "";
                },
                show: false // to turn off the min/max labels.
            },
            width: 15 // for adjusting arc thickness
        },
        color: {
            pattern: ['#7164E3', '#F23D64'], // the three color levels for the percentage values.
            threshold: {
            // unit: 'value', // percentage is default
            // max: 200, // 100 is default
                values: [50]
            }
        },
        size: {
            height: 55
        }
    });
    /* END OF POTENTIAL GAUGE CHARTS */

    /* EMERGING GAUGE CHARTS */
    var egchart = c3.generate({
        bindto: '#emerging-gauge',
        data: {
            columns: [
                ['data', 60]
            ],
            type: 'gauge',
            onclick: function (d, i) { console.log("onclick", d, i); },
            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        },
        gauge: {
            label: {
                format: function(value, ratio) {
                    return "";
                },
                show: false // to turn off the min/max labels.
            },
            width: 15 // for adjusting arc thickness
        },
        color: {
            pattern: ['#7164E3', '#F23D64'], // the three color levels for the percentage values.
            threshold: {
                values: [50]
            }
        },
        size: {
            height: 55
        }
    });
    /* END OF EMERGING GAUGE CHARTS */

    /* CURRENT GAUGE CHARTS */
    var cgchart = c3.generate({
        bindto: '#current-gauge',
        data: {
            columns: [
                ['data', 78]
            ],
            type: 'gauge',
            onclick: function (d, i) { console.log("onclick", d, i); },
            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        },
        gauge: {
            label: {
                format: function(value, ratio) {
                    return "";
                },
                show: false // to turn off the min/max labels.
            },
            width: 15 // for adjusting arc thickness
        },
        color: {
            pattern: ['#7164E3', '#F23D64'], // the three color levels for the percentage values.
            threshold: {
                values: [50]
            }
        },
        size: {
            height: 55
        }
    });
    /* END OF CURRENT GAUGE CHARTS */

    /* CRISIS GAUGE CHARTS */
    var crgchart = c3.generate({
        bindto: '#crisis-gauge',
        data: {
            columns: [
                ['data', 100]
            ],
            type: 'gauge',
            onclick: function (d, i) { console.log("onclick", d, i); },
            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        },
        gauge: {
            label: {
                format: function(value, ratio) {
                    return "";
                },
                show: false // to turn off the min/max labels.
            },
            width: 15 // for adjusting arc thickness
        },
        color: {
            pattern: ['#7164E3', '#F23D64'], // the three color levels for the percentage values.
            threshold: {
                values: [50]
            }
        },
        size: {
            height: 55
        }
    });
    /* END OF CRISIS GAUGE CHARTS */

    /* FLOT CHART - INDEX2 */
    var f2data1 = [[1446371471000, 130], [1446457871000, 40], [1446544271000, 80], [1446630671000, 160], [1446717071000, 159], [1446803471000, 370], [1446889871000, 330], [1446976271000, 350], [1447062671000, 370], [1447149071000, 400], [1447235471000, 330], [1447321871000, 350]];
    var f2data2 = [[1446371471000, 70], [1446457871000, 45], [1446544271000, 135], [1446630671000, 101], [1446717071000, 87], [1446803471000, 145], [1446889871000, 225], [1446976271000, 321], [1447062671000, 400], [1447149071000, 300], [1447235471000, 210], [1447321871000, 187]];
    var f2data3 = [[1446371471000, 150], [1446457871000, 190], [1446544271000, 248], [1446630671000, 333], [1446717071000, 300], [1446803471000, 247], [1446889871000, 221], [1446976271000, 194], [1447062671000, 176], [1447149071000, 101], [1447235471000, 98], [1447321871000, 54]];

    var f2dataset = [
        {label: "Importasi LNG",data: f2data1},
        {label: "PGN-Pertagas",data: f2data2},
        {label: "Kerjasama Bedhtel",data: f2data3},
    ];

    var f2options = {
        series: {
            lines: { show: true, fill: true },
            points: {
                radius: 3,
                show: true
            }
        },
        colors: ["#FFFF00", "#00B3FF", '#FF7300'],
        legend: {
            labelBoxBorderColor: "#cccccc",
            container: $("#legendcontainerf2"),
            noColumns: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            tickColor: "#D1D1D1",
            borderWidth: 1,
            borderColor: "#000"
        },
        xaxis: {
            mode: "time",
            timeformat: "%d-%b-%y"
        }
    };

    $.plot($("#coverage-tonality-flot-chart"), f2dataset, f2options);
    $("#coverage-tonality-flot-chart").bind("plotclick", function (event, pos, item) {
        if (item) { 
            //alert(JSON.stringify(item.series.label));
            var x = item.series.label;
            if(x === 'Importasi LNG'){
                setTimeout(function () {
                    pgchart.load({
                        columns: [['data', item.datapoint[1]]]
                    });
                }, 500);
            } else if(x === 'PGN-Pertagas'){
                setTimeout(function () {
                    egchart.load({
                        columns: [['data', item.datapoint[1]]]
                    });
                }, 500);
            } else if( x === 'Kerjasama Bedhtel'){
                setTimeout(function () {
                    cgchart.load({
                        columns: [['data', item.datapoint[1]]]
                    });
                }, 500);
            } else if( x === 'Turunnya Laba Pertamina'){
                setTimeout(function () {
                    crchart.load({
                        columns: [['data', item.datapoint[1]]]
                    });
                }, 500);
            }
        }
    });
    /* END OF FLOT CHART - INDEX2 */

    $('#popupDatepicker').datepick();
    $('#popupDatepicker2').datepick();
});

$(document).ready(function() {
    $('.periodcontent').fadeOut();

    $('#select').change(function() {
        $('.periodcontent').fadeOut();
        $('.' + $(this).val()).fadeIn();
    }).triggerHandler('change');

    $( '#dl-menu' ).dlmenu();
});

(function($){
	$(window).load(function(){
		$("#highlight").mCustomScrollbar({
			theme:"rounded-dots", 
		});
		$("#content-8").mCustomScrollbar({
			axis:"yx",
			theme:"rounded-dots"
		});
	});
})(jQuery);