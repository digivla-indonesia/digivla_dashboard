'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  async = require('async'),
  restService = require(path.resolve('./modules/news/server/models/news.server.api.model')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  nodeService = require(path.resolve('./modules/summary/server/models/api-node.model.js')),
  elasticService = require(path.resolve('./modules/news/server/models/elastic.model.js')),
  jsonTemplates = require(path.resolve('./modules/news/server/models/template-json.model.js')),
  request = require('request'),
  officegen = require('officegen'),
  SummaryTool = require('node-summary'),
  _ = require('lodash');

/**
 * List Article
 */
exports.getTableList = function(req, res){
  var token = req.user.token,
    client_id = req.user.user_detail.client_id,
    date_from = (typeof req.body.date_from !== 'undefined') ? req.body.date_from : '',
    date_to = (typeof req.body.date_to !== 'undefined') ? req.body.date_to : '',
    media_set = (typeof req.body.media !== 'undefined') ? req.body.media : '',
    group_category = (typeof req.body.category !== 'undefined') ? req.body.category : '',
    media_id = (typeof req.body.media_id !== 'undefined') ? req.body.media_id : '',
    category_id = (typeof req.body.category_id !== 'undefined') ? req.body.category_id : '',
    time_frame = (typeof req.body.time_frame !== 'undefined') ? req.body.time_frame : '',
    offset = (typeof req.body.offset !== 'undefined') ? parseInt(req.body.offset) : 0,
    article_limit = (typeof req.body.limit !== 'undefined') ? parseInt(req.body.limit) : 20;

  async.waterfall([
    function(cb) {
      getMediaId(client_id, media_set, token, function(medArr){
        getCategorySet(client_id, group_category, false, token, function(catArr){
          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          if(category_id !== 'All Category'){
            catArr = {
              success: true,
              data: {
                processed:[
                  {
                    match: {
                      category_id: {
                        query: category_id,
                        operator: 'and'
                      }
                    }
                  }
                ],
                raw: [category_id]
              }
            };
          }

          cb(null, medArr, catArr);
        });
      });
    },
    function(medArr, catArr, cb){
      var dateProcessing = dateFormatting(time_frame, date_from, date_to);

      var items = {
        category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
        media: (medArr.data.length > 0) ? medArr.data : [],
        date_from: dateProcessing.date_from,
          date_to: dateProcessing.date_to,
          interval: dateProcessing.interval
      };

      var params = jsonTemplates.getTableList(items);
      cb(null, medArr, catArr, params);
    },
    function(medArr, catArr, params, cb){
      var uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

      elasticService(uriSegment, params, function(resp){
        var arrayForTable = [];
        var articleIds = [];
        var buckets, bucketsArray, tempCategory, temp_id;
        var result = resp.data.aggregations.datagroup.buckets;
        var sums = (result.length > 5000) ? 5000 : result.length;
        var flagExists = false;

        for(var i=0;i<sums;i++){
          tempCategory = [];
          temp_id = [];
          buckets = result[i];

          for(var j=0;j<buckets.category.hits.hits.length;j++){
            if(catArr.data.raw.indexOf(buckets.category.hits.hits[j]._source.category_id) > -1){
              tempCategory.push(buckets.category.hits.hits[j]._source.category_id);
              temp_id.push(buckets.category.hits.hits[j]._id);
              flagExists = true;
            }
          }

          if(flagExists){
            articleIds.push(buckets.key);
            arrayForTable.push({
              article_id: buckets.key,
              categories: tempCategory,
              tone: buckets.category.hits.hits[0]._source.tone,
              _id: temp_id
            });
            flagExists = false;
          }
        }

        cb(null, medArr, articleIds, arrayForTable);
      });
    },
    function(medArr, articleIds, arrayForTable, cb){
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": articleIds,
        "limit": arrayForTable.length
      }, uriSegment = {
        first: 'article',
        second: 'search'
      }, 
      ftmi,
      tempResult = [];

      nodeService(uriSegment, reqJson, function(resp2) {
        var articleResult = resp2.data.data;



        for (var i = 0; i < articleResult.length; i++) {
          for (var j = 0; j < arrayForTable.length; j++) {
            if (articleResult[i].article_id.toString() === arrayForTable[j].article_id.toString()) {
            // if (parseInt(articleResult[i].article_id) === parseInt(arrayForTable[j].article_id)) {
              arrayForTable[j].detail = articleResult[i];
              break;
            }
          }
        }


        // please remove this looping below, after fixing the duplicate issues for better performance
        for(var i=0;i<arrayForTable.length;i++){
          ftmi = false;

          if(tempResult.length == 0){
            tempResult.push(arrayForTable[i]);
          } else{

            for(var j=0;j<tempResult.length;j++){
              if( arrayForTable[i].hasOwnProperty('detail') && tempResult[j].hasOwnProperty('detail') ){
                if(arrayForTable[i].detail.hasOwnProperty('file_pdf') && tempResult[j].detail.hasOwnProperty('file_pdf') ){
                  if((arrayForTable[i].detail.file_pdf === tempResult[j].detail.file_pdf)){
                    ftmi = true;
                  }
                }
              }
              

            }

            if(!ftmi){
              tempResult.push(arrayForTable[i]);
            }

          }

        }



        // cb(null, medArr, articleIds, arrayForTable);
        cb(null, medArr, articleIds, tempResult);
      });
    },
    function(medArr, articleIds, resultArr, cb){
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": articleIds,
        "limit": resultArr.length
      }, uriSegment = {
          first: 'tbl_activity',
          second: ''
      };

      nodeService(uriSegment, reqJson, function(resp){
        var activityResult = resp.data.data;

          for (var i = 0; i < resultArr.length; i++) {
            if (!resultArr[i].hasOwnProperty('detail')) {
              resultArr[i].detail = {};
              // Hardcode for title
            }

            // Looking for media name
            for (var j = 0; j < medArr.raw.length; j++) {
              if( resultArr[i].hasOwnProperty('detail') ){
                if(resultArr[i].detail.hasOwnProperty('media_id')){
                  if ( resultArr[i].detail.media_id.toString() === medArr.raw[j].user_media_id.toString() ) {
                    resultArr[i].detail.media_name = medArr.raw[j].media_name;
                  }
                }
              }
            }

            resultArr[i].detail.activity = [];
            if(typeof activityResult === 'object'){
              for (var j = 0; j < activityResult.length; j++) {
                if (resultArr[i].article_id.toString() === activityResult[j].article_id.toString() && resultArr[i].detail.activity.indexOf(activityResult[j].activity) == -1) {
                  resultArr[i].detail.activity.push(activityResult[j].activity);
                }
              }
            }
          }

        cb(null, {
          success: true,
          data: resultArr,
          pagination: {
            total_articles: resultArr.length
          }
        });

      });
    },
  ], function(err, result) {
    if (err) {
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.deleteArticles = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      articles = req.body.articles;

  var uriSegment = {},
      items = {};


  async.forEach(articles, function(item, cb){
      var tasksToGo = item._id.length;
      uriSegment = {
        first: 'data_client',
        second: 'delete'
      };

      for(var i=0;i<item._id.length;i++){
        items = {
          client_id: client_id,
          id: item._id[i],
          article_id: item.article_id,
          category_id: item.category_id[i],
          token: token
        };

        nodeService(uriSegment, items, function(resp){
          if (!resp.success) {
            return cb('error when deleting article', null);
          }

          if(--tasksToGo === 0){
            return cb(null, 'success');
          }
        });
      }
  }, function(err, result){
    if(err){
      return res.status(400);
    }
    
    return res.status(200).send(result);
  });
};

/**
 * List articles for sent by email
 */
exports.sendEmail = function (req, res) {
  var destinationEmail = req.body.email || '',
      client_id = req.user.user_detail.client_id,
      logo_head = req.user.user_detail.logo_head,
      media_set = (typeof req.body.media_set !== 'undefined') ? req.body.media_set : '',
      article_ids = req.body.article_id || [],
      token = req.user.token,
      limit = 100,
      articles = [];

  if(article_ids.length > 0){
    for(var i=0;i<article_ids.length;i++){
      articles.push(parseInt(article_ids[i].article_id));
    }
  }

  var dateFormatter = function(tgl) {
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  };

  async.waterfall([
    function(cb){
      getMediaId(client_id, media_set, token, function(medArr) {
        cb(null, medArr);
      });
    },
    function(medArr, cb){
      var items = {
        articles: articles
      };

      var params = jsonTemplates.getEmailContent(items);
      cb(null, medArr, params);
    },
    function(medArr, params, cb){
      var uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

      elasticService(uriSegment, params, function(resp){
        var arrayForTable = [];
        var articleIds = [];
        var buckets, bucketsArray, tempCategory, temp_id;
        var result = resp.data.aggregations.datagroup.buckets;
        var sums = (result.length > 5000) ? 5000 : result.length;
        var tempArray = [];

        for(var i=0;i<sums;i++){
          tempCategory = [];
          temp_id = [];
          buckets = result[i];
          articleIds.push(buckets.key);

          tempArray.push(buckets.category.hits.hits[0]._source);
        }

        cb(null, medArr, tempArray);
      });
    },
    // looking for summary
    function(medArr, tempArray, cb){
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": articles
      };
      var uriSegment = {
        first: 'tb_summary_data',
        second: ''
      };
      var summaryJson = {};

      nodeService(uriSegment, reqJson, function(resp2) {
        var summaryResult = resp2.data.data;

        for (var i = 0; i < summaryResult.length; i++) {
          for (var j = 0; j < articles.length; j++) {
            if (summaryResult[i].article_id.toString() === articles[j].toString()) {
              summaryJson[summaryResult[i].article_id.toString()] = summaryResult[i].summary;
            }
          }
        }

        var sumArr = {};
        var tasksToGo = articles.length;

        for (var i = 0; i < articles.length; i++) {
          if (!summaryJson.hasOwnProperty(articles[i].toString())) {
            nodeService({first: 'article', second: 'detail'}, {article_id: articles[i], token: token}, function(ad){
              SummaryTool.summarize(ad.data.data.title, ad.data.data.content.replace(/\r?\n|\r/g, " "), function(err, summary) {

                summaryJson[ad.data.data.article_id] = summary.replace(/\r?\n|\r/g, ". ");

                nodeService({first: 'insert', second: 'tb_summary_data'}, {client_id: client_id, summary: summary.replace(/\r?\n|\r/g, ". "), article_id: ad.data.data.article_id, token: token}, function(response){
                  if(--tasksToGo === 0){
                    cb(null, medArr, tempArray, summaryJson);
                  }
                });

              });
            });

          } else{
            --tasksToGo;
            if(tasksToGo === 0){
              cb(null, medArr, tempArray, summaryJson);
            }
          }
        }

      });

    },

    function(medArr, data_client, summaryJson, cb){
      var reqJson = {
        article_id: articles,
        token: token
      }, uriSegment = {
        first: 'article',
        second: 'detail'
      };

      nodeService(uriSegment, reqJson, function(response){
        var result = response.data.data.hits.hits;
        
        for(var i=0;i<data_client.length;i++){
          for(var j=0;j<result.length;j++){

            if(data_client[i].article_id.toString() === result[j]._source.article_id.toString()){
              data_client[i].mmcol = result[j]._source.mmcol;
              data_client[i].page = result[j]._source.page;
              data_client[i].file_pdf = result[j]._source.file_pdf;
              data_client[i].columne = result[j]._source.columne;
              data_client[i].journalist = result[j]._source.journalist;
              data_client[i].is_chart = result[j]._source.is_chart;
              data_client[i].is_table = result[j]._source.is_table;
              data_client[i].is_colour = result[j]._source.is_colour;
              data_client[i].title = result[j]._source.title;
              data_client[i].summary = summaryJson[result[j]._source.article_id.toString()];
            }

          }

          // Looking for media name
          for (var j = 0; j < medArr.raw.length; j++) {
            if (data_client[i].media_id.toString() === medArr.raw[j].user_media_id.toString()) {
              data_client[i].media_name = medArr.raw[j].media_name;
            }
          }
        }

        cb(null, data_client);
      });
    },
    function(data_client, cb){
      var emailHTMLBody = 'Dear all,<br><br>';
      emailHTMLBody += 'Please find below the summary of Media Monitoring for today, which comprising the following news:<br><br><br>';

      for(var i=0;i<data_client.length;i++){
        var _source = data_client[i];
        var tonality = _source.tone;

        if(tonality == '-1')
          tonality = 'Negative';
        else if(tonality == '0')
          tonality = 'Neutral';
        else if(tonality == '1')
          tonality = 'Positive';

        emailHTMLBody += '<b>' + _source.title + '</b><br><br>';
        emailHTMLBody += 'Date : ' + _source.datee + '<br><br>';
        emailHTMLBody += '(<b>Media :</b> ' + _source.media_name + ' <b>Page :</b> ' + _source.page + ' <b>Tone :</b> ' + tonality + ')<br><br>';
        emailHTMLBody += '<b>Editor:</b> ' + _source.journalist + '<br><br>';
        emailHTMLBody += '<b>Summary:</b> ' + _source.summary + '<br><br>';

        if(_source.file_pdf.indexOf('http') != -1){
          emailHTMLBody += '<b>Source:</b> <a href="' + _source.file_pdf + '" target="_blank">Source</a><br><br>';
        } else{
          var file = _source.file_pdf,
            tanggal = file.substring(8, 10),
            bulan = file.substring(5, 7),
            tahun = file.substring(0, 4),
            artikelOCR = 'http://pdf.antara-insight.id/pdf_text/' + tahun + '/' + bulan + '/' + tanggal + '/' + file,
            artikelImage = 'http://pdf.antara-insight.id/pdf_images/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;

            
            var fileUrl = 'http://pdf.antara-insight.id?stat=2';
            fileUrl += '&article=' + _source.article_id;
            fileUrl += '&formate=1';
            fileUrl += '&logo_head=' + logo_head;
            fileUrl += '&client_id=' + client_id;

            var teksAsli = 'http://pdf.antara-insight.id?stat=1';
            teksAsli += '&article=' + _source.article_id;
            teksAsli += '&formate=1';
            teksAsli += '&logo_head=' + logo_head;
            teksAsli += '&client_id=' + client_id;

          emailHTMLBody += '<b>Source:</b> <a href="' + teksAsli + '" target="_blank">Teks Asli</a> | <a href="' + fileUrl + '" target="_blank">Scan Media</a> | <a href="' + artikelImage + '">Artikel Asli</a> | <a href="' + artikelOCR + '">Artikel Scan</a> <br><br>';
        }
        
        emailHTMLBody += '<hr><br><br>';
      }

      var reqJson = {
        to: destinationEmail,
        subject: '[AntaraInsight] Today News Summary',
        html_body: emailHTMLBody,
        token: token
      }, uriSegment = {
        first: 'send',
        second: 'mail'
      };

      nodeService(uriSegment, reqJson, function(response){
        if(!response.success){
          return cb('Error sending email', null);
        }

        cb(null, response.data);
      });
    }
  ], function(err, result){
    if (err) {
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.generatePDF = function(req, res) {
  var fs = require('fs');
  var compLogo = 'https://admin.antara-insight.id/asset/images/' + req.user.user_detail.logo_head;
  var token = req.user.token;
  var type = req.body.type;
  var article_id = req.body.article_id;
  var article_detail = req.body.article_detail;
  var client_id = req.user.user_detail.client_id;
  var type = req.body.type || 'teks_pdf';

  request.defaults({
    encoding: null
  }).get(compLogo, function(error, response, body) {
    var data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');

    var dd = {
      content: [{
          style: 'tableExample',
          color: '#444',
          table: {
            body: [
              [{
                rowSpan: 4,
                image: data,
                fit: [100, 100]
              }, {
                colSpan: 2,
                text: 'Judul  : ' + article_detail.title + ''
              }, ''],
              ['', 'Media  : ' + article_detail.media_name + '', 'Wartawan          : ' + article_detail.journalist + ''],
              ['', 'Tanggal  : ' + article_detail.datee.substring(0, 10) + '', 'Nada Pemberitaan  : '],
              ['', 'Halaman  : ' + article_detail.page + '', '']
            ]
          }
        },
        ' ',
        ' '
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
      defaultStyle: {
        alignment: 'justify'
      }
    };


    // Getting the content
    if (type === 'teks_pdf') {
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": [article_id.toString()]
      };
      var uriSegment = {
        first: 'summary',
        second: 'search'
      };

      nodeService(uriSegment, reqJson, function(resp) {
        var summary = 'Belum ada summary';
        if (typeof resp.data === 'array' && resp.data.length > 0) {
          summary = resp.data[0]._source.summary;
        }

        // dd.content.push(summary);

        // sementara pake content dulu
        dd.content.push(article_detail.content);

        return res.status(200).send(dd);
      });
    }
  });
};


exports.generateWord = function(req, res) {
  var chartsData = req.query.data || [];
  var comp_logo = req.user.user_detail.logo_head;
  var comp_name = req.user.user_detail.usr_comp_name;
  var tmpfolder = './modules/dashboards/server/temp/';
  var docx = officegen('docx');
  var slide;
  var filename = req.user.user_detail.client_id + '_' + req.user.user_detail.usr_uid.toString() + '_' + Date.now().toString() + '.docx';

  chartsData = new Buffer(chartsData, 'base64').toString('utf8');
  chartsData = JSON.parse(chartsData);
  // console.log(JSON.stringify(chartsData));

  /*Downloading image*/
  var download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
      // console.log('content-type:', res.headers['content-type']);
      // console.log('content-length:', res.headers['content-length']);

      request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
  };
  /*Downloading image*/

  res.writeHead(200, {
    "x-filename": filename,
    "Content-Type": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    'Content-disposition': 'attachment; filename=' + filename
  });


  var pObj = docx.createP();

  pObj.addText('Judul      : ' + chartsData.title);
  pObj.addLineBreak();
  pObj.addText('Media      : ' + chartsData.media_name);
  pObj.addLineBreak();
  pObj.addText('Wartawan   : ' + chartsData.journalist);
  pObj.addLineBreak();
  pObj.addText('Tanggal      : ' + chartsData.datee.substring(0, 10));
  // pObj.addLineBreak ();
  // pObj.addText ('Nada Pemberitaan : Negatif');
  pObj.addLineBreak();
  pObj.addText('Halaman    : ' + chartsData.page);


  var pObj = docx.createP({
    align: 'center'
  });
  pObj.addLineBreak();
  pObj.addLineBreak();
  pObj.addLineBreak();
  pObj.addLineBreak();
  pObj.addText(chartsData.content);

  // download('http://app.digivla.id/admin/asset/images/' + comp_logo, path.resolve(tmpfolder + comp_logo), function(){
  // console.log('image downloaded');
  // adding comp logo
  // slide = pptx.makeNewSlide();
  // slide.addText(comp_name);
  // slide.addImage('http://app.digivla.id/admin/asset/images/' + comp_logo);

  async.parallel([function(chartInfo, callback) {
      docx.on('finalize', function(written) {
        console.log('Finish to create a PowerPoint file.\nTotal bytes created: ' + written + '\n');
      });

      docx.generate(res);
    }],
    function(err) {
      docx.on('error', function(err) {
        console.log(err);
        return res.status(400);
      });

      // return docx.generate(res);
    });
};

exports.saveActivity = function(req, res, next){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      usr_uid = req.user.user_detail.usr_uid,
      activities = req.body,
      uriSegment = {},
      items = {};

  var dateFormatter = function(tgl) {
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  }

  async.forEach(activities, function(item, cb){
    uriSegment = {
      first: 'insert',
      second: 'tbl_activity'
    }, items = {
      "tanggal": dateFormatter(new Date()),
      "activity": item.activity,
      "user_id": usr_uid,
      "client_id": client_id,
      "article_id": item.article_id,
      "category_id": item.category_id,
      "token": token
    };

    nodeService(uriSegment, items, function(resp){
      if (!resp.success) {
        cb('error when adding activity', null);
      }

      cb(null, 'success');
    });

  }, function(err, result){
    if(err){
      return res.status(400);
    }
    
    return res.status(200).send(result);
  });
};







function dateFormatting(timeFrame, df, dt) {
  var currentDate = new Date(),
    dateFlag = new Date(),
    interval = 'day',
    date_from = df,
    date_to = dt;

  var dateFormatter = function(tgl) {
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  }

  if (timeFrame !== 0) {
    switch (timeFrame) {
      case 1:
        dateFlag.setDate(dateFlag.getDate() - 1);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 7:
        dateFlag.setDate(dateFlag.getDate() - 6);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 30:
        dateFlag.setDate(dateFlag.getDate() - 29);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 365:
        dateFlag.setDate(dateFlag.getDate() - 364);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        interval = 'month';
        break;
    }
  } else {
    if (date_to == '' || date_from == '') {
      dateFlag.setDate(dateFlag.getDate() - 7);
      date_from = dateFormatter(dateFlag);
      date_to = dateFormatter(currentDate);
    }
  }

  return {
    date_from: date_from,
    date_to: date_to,
    interval: interval
  };
}

function getMedia(cb) {
  var uriSegment = {
      index: 'media',
      type: 'datalist',
      method: 'POST'
    },
    params = jsonTemplates.getMedia();

  elasticService(uriSegment, params, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var resArr = JSON.parse(JSON.stringify(resp));
    var arrayForChart = {};

    if (resArr.data.hits.hits.length > 0) {
      var result = resArr.data.hits.hits;

      for (var i = 0; i < result.length; i++) {
        arrayForChart[result[i]._source.media_id] = result[i]._source.media_name;
      }

      return cb({
        success: true,
        data: arrayForChart
      });

    } else {
      return cb({
        success: false,
        data: []
      });
    }

  });
};



function getMediaId(client_id, medset, token, cb) {
  var reqJson = {
      client_id: client_id,
      user_media_type_id: parseInt(medset),
      token: token
    },
    uriSegment = {
      first: 'tb_media_set_choosen',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var result = [];
    var rawData = resp.data.data;

    for (var i = 0; i < rawData.length; i++) {
      result.push(rawData[i].user_media_id);
    }

    return cb({
      success: true,
      data: result,
      raw: rawData
    });
  });
}

function getCategorySet(client_id, catset, raw, token, cb) {
  var reqJson = {
      client_id: client_id,
      category_set: parseInt(catset),
      token: token
    },
    uriSegment = {
      first: 'tb_category_set',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var rawData = resp.data.data;
    var arrayForChart = [];


    for(var i=0;i<rawData.length;i++){
      var item = {
        "match": {
          "category_id": {
            "query": rawData[i].category_id,
            "operator": "and"
          }
        }
      };

      arrayForChart.push(item);
    }

    var rawArr = [];

    for (var i = 0; i < rawData.length; i++) {
      rawArr.push(rawData[i].category_id);
    }

    return cb({
      success: true,
      data: {
        processed: arrayForChart,
        raw: rawArr
      }
    });

  });
}