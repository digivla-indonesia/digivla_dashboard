'use strict';

// Configuring the Articles module
angular.module('search', ['ngPrint', 'angular-loading-bar']).run(['Menus',
  function(Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Search',
      state: 'search'
    });
  }
]);
