'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  async = require('async'),
  restService = require(path.resolve('./modules/summary/server/models/summary.server.api.model')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  elasticService = require(path.resolve('./modules/summary/server/models/elastic.model.js')),
  nodeService = require(path.resolve('./modules/summary/server/models/api-node.model.js')),
  jsonTemplates = require(path.resolve('./modules/summary/server/models/template-json.model.js')),
  request = require('request'),
  SummaryTool = require('node-summary'),
  _ = require('lodash');

exports.getTableList = function(req, res) {
  var token = req.user.token,
    client_id = req.user.user_detail.client_id,
    date_from = (typeof req.body.date_from !== 'undefined') ? req.body.date_from : '',
    date_to = (typeof req.body.date_to !== 'undefined') ? req.body.date_to : '',
    media_set = (typeof req.body.media !== 'undefined') ? req.body.media : '',
    group_category = (typeof req.body.category !== 'undefined') ? req.body.category : '',
    media_id = (typeof req.body.media_id !== 'undefined') ? req.body.media_id : '',
    category_id = (typeof req.body.category_id !== 'undefined') ? req.body.category_id : '',
    time_frame = (typeof req.body.time_frame !== 'undefined') ? req.body.time_frame : '',
    offset = (typeof req.body.offset !== 'undefined') ? parseInt(req.body.offset) : 0,
    article_limit = (typeof req.body.limit !== 'undefined') ? parseInt(req.body.limit) : 20;

  async.waterfall([
    function(cb) {
      getMediaId(client_id, media_set, token, function(medArr){
        getCategorySet(client_id, group_category, false, token, function(catArr){
          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          if(category_id !== 'All Category'){
            catArr = {
              success: true,
              data: {
                processed:[
                  {
                    match: {
                      category_id: {
                        query: category_id,
                        operator: 'and'
                      }
                    }
                  }
                ],
                raw: [category_id]
              }
            };
          }

          cb(null, medArr, catArr);
        });
      });
    },
    function(medArr, catArr, cb){
      var dateProcessing = dateFormatting(time_frame, date_from, date_to);

      var items = {
        category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
        media: (medArr.data.length > 0) ? medArr.data : [],
        date_from: dateProcessing.date_from,
          date_to: dateProcessing.date_to,
          interval: dateProcessing.interval
      };

      var params = jsonTemplates.getTableList(items);
      cb(null, medArr, catArr, params);
    },
    function(medArr, catArr, params, cb){
      var uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

      elasticService(uriSegment, params, function(resp){
        var arrayForTable = [];
        var articleIds = [];
        var buckets, bucketsArray, tempCategory, temp_id, temp_data_input_date, tempTone;
        var result = resp.data.aggregations.datagroup.buckets;
        var sums = (result.length > 5000) ? 5000 : result.length;
        var flagExists = false;

        for(var i=0;i<sums;i++){
          tempCategory = [];
          temp_id = [];
          temp_data_input_date = [];
          buckets = result[i];

          for(var j=0;j<buckets.category.hits.hits.length;j++){
            if(catArr.data.raw.indexOf(buckets.category.hits.hits[j]._source.category_id) > -1){
              tempCategory.push(buckets.category.hits.hits[j]._source.category_id);
              temp_id.push(buckets.category.hits.hits[j]._id);
              temp_data_input_date.push(buckets.category.hits.hits[j]._source.data_input_date);

              tempTone = buckets.category.hits.hits[j]._source.tone;
              flagExists = true;
            }
          }

          if(flagExists){
            articleIds.push(buckets.key);
            arrayForTable.push({
              article_id: buckets.key,
              categories: tempCategory,
              tone: tempTone,
              _id: temp_id,
              data_input_date: temp_data_input_date
            });
            flagExists = false;
          }
        }

        cb(null, medArr, articleIds, arrayForTable);
      });
    },
    function(medArr, articleIds, arrayForTable, cb){
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": articleIds,
        "limit": arrayForTable.length,
        "full": true
      }, uriSegment = {
        first: 'article',
        second: 'search'
      }, 
      ftmi,
      tempResult = [];

      nodeService(uriSegment, reqJson, function(resp2) {
        var articleResult = resp2.data.data;

        for (var i = 0; i < articleResult.length; i++) {
          for (var j = 0; j < arrayForTable.length; j++) {
            if (articleResult[i].article_id.toString() === arrayForTable[j].article_id.toString()) {
              arrayForTable[j].detail = articleResult[i];
            }
          }
        }


        // please remove this looping below, after fixing the duplicate issues for better performance
        for(var i=0;i<arrayForTable.length;i++){
          ftmi = false;

          if(tempResult.length == 0){
            tempResult.push(arrayForTable[i]);
          } else{

            for(var j=0;j<tempResult.length;j++){
              // console.log('tempResult[j]: ' + JSON.stringify(tempResult[j]));
              // console.log('arrayForTable[i]: ' + JSON.stringify(arrayForTable[i]));
              if(arrayForTable[i].detail.file_pdf === tempResult[j].detail.file_pdf){
                ftmi = true;
              }
              

            }

            if(!ftmi){
              tempResult.push(arrayForTable[i]);
            }

          }

        }


        // cb(null, medArr, articleIds, arrayForTable);
        cb(null, medArr, articleIds, tempResult);
      });
    },
    function(medArr, articleIds, resultArr, cb) {
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": articleIds
      }, uriSegment = {
        first: 'tb_summary_data',
        second: ''
      };

      nodeService(uriSegment, reqJson, function(resp2) {
        var summaryResult = resp2.data.data;

        if(summaryResult.length > 0){
          for (var i = 0; i < summaryResult.length; i++) {
            for (var j = 0; j < resultArr.length; j++) {
              if (summaryResult[i].article_id.toString() === resultArr[j].article_id.toString()) {
                resultArr[j].summary = summaryResult[i].summary;
              }
            }
          }
        }

        cb(null, medArr, articleIds, resultArr);
      });
    },
    function(medArr, articleIds, resultArr, cb){
      var sumArr = {};
      var tasksToGo = resultArr.length;

      for (var i = 0; i < resultArr.length; i++) {
        if (!resultArr[i].hasOwnProperty('summary')) {
            SummaryTool.summarize(resultArr[i].detail.title, resultArr[i].detail.content.replace(/\r?\n|\r/g, " "), function(err, summary) {

              sumArr[resultArr[i].article_id] = summary.replace(/\r?\n|\r/g, ". ")

              nodeService({first: 'insert', second: 'tb_summary_data'}, {client_id: client_id, summary: summary.replace(/\r?\n|\r/g, ". "), article_id: resultArr[i].article_id, token: token}, function(response){
                if(--tasksToGo === 0){
                  cb(null, medArr, articleIds, resultArr, sumArr);
                }
              });

            });

        } else{
          --tasksToGo;
          if(tasksToGo === 0){
            cb(null, medArr, articleIds, resultArr, sumArr);
          }
        }
      }
    },
    function(medArr, articleIds, resultArr, sumArr, cb){
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": articleIds,
        "limit": resultArr.length
      }, uriSegment = {
          first: 'tbl_activity',
          second: ''
      };

      nodeService(uriSegment, reqJson, function(resp){
        var activityResult = resp.data.data;

          for (var i = 0; i < resultArr.length; i++) {
            if (!resultArr[i].hasOwnProperty('detail')) {
              resultArr[i].detail = {};
              // Hardcode for title
            }

            if (!resultArr[i].hasOwnProperty('summary')) {
              resultArr[i].summary = sumArr[resultArr[i].article_id];
            }

            // Looking for media name
            for (var j = 0; j < medArr.raw.length; j++) {
              if (resultArr[i].detail.media_id.toString() === medArr.raw[j].user_media_id.toString()) {
                resultArr[i].detail.media_name = medArr.raw[j].media_name;
              }
            }

            resultArr[i].detail.activity = [];
            if(typeof activityResult === 'object'){
              for (var j = 0; j < activityResult.length; j++) {
                if (resultArr[i].article_id.toString() === activityResult[j].article_id.toString() && resultArr[i].detail.activity.indexOf(activityResult[j].activity) == -1) {
                  resultArr[i].detail.activity.push(activityResult[j].activity);
                }
              }
            }
          }

        cb(null, {
          success: true,
          data: resultArr,
          pagination: {
            total_articles: resultArr.length
          }
        });

      });
    }
  ], function(err, result) {
    if (err) {
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

/** these scripts is not used for get article detail

exports.getArticleDetail = function(req, res) {
  var token = req.user.token,
    article_id = req.query._id,
    uriSegment = {
      method: 'POST',
      index: '/api/dashboard/getdetailarticle'
    };

  var items = {
    token: token,
    article_id: parseInt(article_id)
  };

  restService(items, uriSegment, function(resp) {
    if (!resp.success) {
      res.status(400).send(resp.data);
      return;
    }

    return res.status(200).send(resp.data);
  });
};
*/

exports.updateSummary = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      usr_uid = req.user.user_detail.usr_uid,
      article_id = req.body.article_id,
      category_id = req.body.categories,
      title = req.body.detail.title,
      _id = req.body._id,
      summary = req.body.summary,
      activity = req.body.activity;




  async.forEachOf(category_id, function(value, key, cb){

    var dateFormatter = function(tgl) {
      return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
    };

    var uriSegment = {
      first: 'insert',
      second: 'tbl_activity'
    }, items = {
      "tanggal": dateFormatter(new Date()),
      "activity": activity,
      "user_id": usr_uid,
      "client_id": client_id,
      "article_id": article_id,
      "category_id": value,
      "token": token
    };

    nodeService(uriSegment, items, function(resp){
      if (!resp.success) {
        cb(resp.data, null);
      }

      // cb(null, 'success');
      var uriSegment = {
        first: 'summary',
        second: 'edit'
      }, items = {
        id: _id[key],
        title: title,
        client_id: client_id,
        article_id: article_id,
        summary: summary,
        token: token
      };

      nodeService(uriSegment, items, function(resp){
        if (!resp.success) {
          cb(resp.data, null);
        }

        cb(null, 'success');
      });

    });

  }, function(err, result){
    if (err) {
      res.status(400);
      return res.send(result.data);
    }

    return res.status(200).send('success');
  });




  // async.waterfall([
  //   function(cb){
  //     var dateFormatter = function(tgl) {
  //       return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  //     };

  //     var uriSegment = {
  //       first: 'insert',
  //       second: 'tbl_activity'
  //     }, items = {
  //       "tanggal": dateFormatter(new Date()),
  //       "activity": activity,
  //       "user_id": usr_uid,
  //       "client_id": client_id,
  //       "article_id": article_id,
  //       "category_id": category_id,
  //       "token": token
  //     };

  //     nodeService(uriSegment, items, function(resp){
  //       if (!resp.success) {
  //         cb(resp.data, null);
  //       }

  //       cb(null, 'success');
  //     });
  //   },
  //   function(response, cb){
  //     var uriSegment = {
  //       first: 'summary',
  //       second: 'edit'
  //     }, items = {
  //       id: req.body.id,
  //       title: title,
  //       client_id: client_id,
  //       article_id: article_id,
  //       summary: summary,
  //       token: token
  //     };

  //     nodeService(uriSegment, items, function(resp){
  //       if (!resp.success) {
  //         cb(resp.data, null);
  //       }

  //       cb(null, 'success');
  //     });
  //   }
  // ], function(err, result){
  //   if (err) {
  //     res.status(400);
  //     return res.send(result.data);
  //   }

  //   return res.status(200).send('success');
  // });

};

exports.updateDataClient = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      usr_uid = req.user.user_detail.usr_uid,
      activity = req.body.activity,
      article_id = req.body.article_id,
      category_id = req.body.categories,
      _id = req.body._id,
      datee = (typeof req.body.detail.datee !== 'undefined') ? req.body.detail.datee.substring(0, 10) : "2010-01-01",
      media_id = (typeof req.body.detail.media_id !== 'undefined') ? parseInt(req.body.detail.media_id) : 0,
      mention_times = (typeof req.body.detail.mention_times !== 'undefined') ? parseInt(req.body.detail.mention_times) : 0,
      tone = parseInt(req.body.tone),
      exposure = (typeof req.body.detail.exposure !== 'undefined') ? parseInt(req.body.detail.exposure) : 0,
      advalue_fc = (typeof req.body.detail.advalue_fc !== 'undefined') ? parseInt(req.body.detail.advalue_fc) : 0,
      advalue_bw = (typeof req.body.detail.advalue_bw !== 'undefined') ? parseInt(req.body.detail.advalue_bw) : 0,
      circulation = (typeof req.body.detail.circulation !== 'undefined') ? parseInt(req.body.detail.circulation) : 0,
      data_input_date = req.body.data_input_date,
      usere = (typeof req.body.detail.usere !== 'undefined') ? req.body.detail.usere : 'Anonymous',
      pc_name = (typeof req.body.detail.pc_name !== 'undefined') ? req.body.detail.pc_name : '10.10.10.4';

  async.forEachOf(category_id, function(value, key, cb){

    var dateFormatter = function(tgl) {
      return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
    };

    var uriSegment = {
      first: 'insert',
      second: 'tbl_activity'
    }, items = {
      "tanggal": dateFormatter(new Date()),
      "activity": activity,
      "user_id": usr_uid,
      "client_id": client_id,
      "article_id": article_id,
      "category_id": value,
      "token": token
    };

    nodeService(uriSegment, items, function(resp){
      if (!resp.success) {
        cb(resp.data, null);
      }

      var uriSegment = {
        first: 'article_id',
        second: 'update'
      }, items = {
        "client_id": client_id,
        "id": _id[key],
        "article_id": article_id,
        "category_id": value,
        "datee": datee,
        "media_id": media_id,
        "mention_times": mention_times,
        "tone": tone,
        "exposure": exposure,
        "advalue_fc": advalue_fc,
        "circulation": circulation,
        "advalue_bw": advalue_bw,
        "data_input_date": data_input_date[key],
        "usere": usere,
        "pc_name": pc_name,
        "token": token
      };

      nodeService(uriSegment, items, function(resp){
        if (!resp.success) {
          cb(resp.data, null);
        }

        cb(null, 'success');
      });
    });

  }, function(err, result){
    if (err) {
      res.status(400);
      return res.send(result.data);
    }

    return res.status(200).send('success');
  });
};

exports.editArticle = function(req, res) {
  var token = req.user.token,
    title = req.body.title,
    summary = req.body.summary,
    client_id = req.user.user_detail.client_id,
    article_id = req.body.article_id,
    id = req.body.id,
    url = 'http://182.23.64.70:8000/summary/edit',
    auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');

  var items = {
    token: token,
    title: title,
    summary: summary,
    client_id: client_id,
    article_id: article_id,
    id: id
  };

  var params = {
    uri: url,
    method: 'POST',
    headers: {
      Authorization: 'Basic ' + auth,
      'Content-Type': 'application/json'
    },
    json: items
  };

  request(params, function(error, response, body) {
    if (!error && response.statusCode === 200) {
      return res.send(body);
    } else {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(error)
      });
    }
  });
};

exports.deleteArticles = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      articles = req.body.articles;

  var uriSegment = {},
      items = {};


  async.forEach(articles, function(item, cb){
      var tasksToGo = item._id.length;
      uriSegment = {
        first: 'data_client',
        second: 'delete'
      };

      for(var i=0;i<item._id.length;i++){
        items = {
          client_id: client_id,
          id: item._id[i],
          article_id: item.article_id,
          category_id: item.category_id[i],
          token: token
        };

        nodeService(uriSegment, items, function(resp){
          if (!resp.success) {
            return cb('error when deleting article', null);
          }

          if(--tasksToGo === 0){
            return cb(null, 'success');
          }
        });
      }
  }, function(err, result){
    if(err){
      return res.status(400);
    }
    
    return res.status(200).send(result);
  });
};

exports.getCategoryList = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      items = {
        token: token,
        client_id: client_id
      },
      uriSegment= {
        first: 'tb_category_list',
        second: ''
      };

  nodeService(uriSegment, items, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }

    return res.status(200).send(resp.data.data);
  });
};

exports.saveArticle = function(req, res){
  var dateFormatter = function(tgl){
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2) + ' ' + ('0' + tgl.getHours()).slice(-2) + ':' + ('0' + tgl.getMinutes()).slice(-2) + ':' + ('0' + tgl.getSeconds()).slice(-2);
  };

  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      article = req.body,
      uriSegment = {
        // index: '/article_id/insert'
        first: 'article_id',
        second: 'insert'
      }, items = {
        token: token,
        client_id: client_id,
        // id: 1, //hardcode!!!
        article_id: article.article_id,
        datee: article.datee.substring(0, 10),
        media_id: article.media_id,
        mention_times: 0,
        tone: 0,
        exposure: 0,
        advalue_fc: 0,
        circulation: 0,
        advalue_bw: 0,
        data_input_date: dateFormatter(new Date()),
        usere: 'client_id',
        pc_name: 'client'
      };

  async.forEach(article.categories, function(item, cb){
    items.category_id = item;

    nodeService(uriSegment, items, function(resp){
      if (!resp.success) {
        cb('error when saving article', null);
      }

      cb(null, 'success inserting article into data_client');
    });
  }, function(err, result){
    return res.status(400);
  });

  return res.status(200).send('success');
};



function dateFormatting(timeFrame, df, dt) {
  var currentDate = new Date(),
    dateFlag = new Date(),
    interval = 'day',
    date_from = df,
    date_to = dt;

  var dateFormatter = function(tgl) {
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  }

  if (timeFrame !== 0) {
    switch (timeFrame) {
      case 1:
        dateFlag.setDate(dateFlag.getDate() - 1);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 7:
        dateFlag.setDate(dateFlag.getDate() - 6);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 30:
        dateFlag.setDate(dateFlag.getDate() - 29);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 365:
        dateFlag.setDate(dateFlag.getDate() - 364);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        interval = 'month';
        break;
    }
  } else {
    if (date_to == '' || date_from == '') {
      dateFlag.setDate(dateFlag.getDate() - 7);
      date_from = dateFormatter(dateFlag);
      date_to = dateFormatter(currentDate);
    }
  }

  return {
    date_from: date_from,
    date_to: date_to,
    interval: interval
  };
}

function getMedia(cb) {
  var uriSegment = {
      index: 'media',
      type: 'datalist',
      method: 'POST'
    },
    params = jsonTemplates.getMedia();

  elasticService(uriSegment, params, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var resArr = JSON.parse(JSON.stringify(resp));
    var arrayForChart = {};

    if (resArr.data.hits.hits.length > 0) {
      var result = resArr.data.hits.hits;

      for (var i = 0; i < result.length; i++) {
        arrayForChart[result[i]._source.media_id] = result[i]._source.media_name;
      }

      return cb({
        success: true,
        data: arrayForChart
      });

    } else {
      return cb({
        success: false,
        data: []
      });
    }

  });
};

function getMediaId(client_id, medset, token, cb) {
  var reqJson = {
      client_id: client_id,
      user_media_type_id: parseInt(medset),
      token: token
    },
    uriSegment = {
      first: 'tb_media_set_choosen',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var result = [];
    var rawData = resp.data.data;

    for (var i = 0; i < rawData.length; i++) {
      result.push(rawData[i].user_media_id);
    }

    return cb({
      success: true,
      data: result,
      raw: rawData
    });
  });
}

function getCategorySet(client_id, catset, raw, token, cb) {
  var reqJson = {
      client_id: client_id,
      category_set: parseInt(catset),
      token: token
    },
    uriSegment = {
      first: 'tb_category_set',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var rawData = resp.data.data;
    var arrayForChart = [];


    for(var i=0;i<rawData.length;i++){
      var item = {
        "match": {
          "category_id": {
            "query": rawData[i].category_id,
            "operator": "and"
          }
        }
      };

      arrayForChart.push(item);
    }

    var rawArr = [];

    for (var i = 0; i < rawData.length; i++) {
      rawArr.push(rawData[i].category_id);
    }

    return cb({
      success: true,
      data: {
        processed: arrayForChart,
        raw: rawArr
      }
    });

  });
}