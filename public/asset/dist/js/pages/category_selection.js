$(function(){
	$( '#dl-menu' ).dlmenu();
	$(document).on('click','.closeFancybox',function(){
		$.fancybox.close();
	});
});

$(document).ready(function() {
	$('.fancybox').fancybox({
		closeBtn   : false,	
	});
});	

//Initialize 2nd demo:
ddaccordion.init({
	headerclass: "category", //Shared CSS class name of headers group
	contentclass: "categorycontent", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc]. [] denotes no content.
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	scrolltoheader: false, //scroll to header each time after it's been expanded by the user?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["closedlanguage", "openlanguage"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["prefix", " ", " "], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
});

var config = {
	'.chosen-select-no-single' : {disable_search_threshold:10,disable_search:true}
}
for (var selector in config) {
	$(selector).chosen(config[selector]);
}

(function($){
	$(window).load(function(){
		$("#ms1,#ms2,#ms3,#ms4,#ms5,#ms6,#ms7,#ms8,#group-selection").mCustomScrollbar({
			theme:"rounded-dots", 
		});
	});
})(jQuery);

$('#check-all').on('click', function(){
    // Change values
    $('div.main-5-2').find("input[type='checkbox']").prop('checked', ($(this).val() == 'Check'));
    // Change caption of this button
    $(this).val( ($(this).val() == 'Check' ? 'Uncheck' : 'Check') );
});