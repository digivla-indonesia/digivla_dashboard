'use strict';

module.exports = function (app) {
  // User Routes
  var users = require('../controllers/users.server.controller');

  // Setting up the users profile api
  app.route('/api/users/me').get(users.me);
  // app.route('/api/users').put(users.update);
  // app.route('/api/users/accounts').delete(users.removeOAuthProvider);
  // app.route('/api/users/password').post(users.changePassword);
  // app.route('/api/users/picture').post(users.changeProfilePicture);

  // Get user category & media list
  app.route('/api/users/getassets')
      .get(users.getClientAssets)
      .put(users.updateClientAssets)
      .delete(users.deleteClientAssets);
  app.route('/api/users/getsubassets')
      .get(users.getClientSubAssets)
      .put(users.updateClientSubAssets)
      .delete(users.deleteClientSubAssets);

  app.route('/api/users/getsubclientcategory')
      .get(users.getSubClientCategory);

  app.route('/api/users/getsubclientmedia')
      .get(users.getSubClientMedia);

  // Finish by binding the user middleware
  app.param('userId', users.userByID);
};
