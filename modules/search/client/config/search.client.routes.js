'use strict';

angular.module('search').config(['$stateProvider', '$sceDelegateProvider', 'cfpLoadingBarProvider',
  function($stateProvider, $sceDelegateProvider, cfpLoadingBarProvider) {
    $stateProvider
      .state('search', {
        abstract: true,
        url: '/search',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('search.print', {
        url: '',
        templateUrl: 'modules/search/views/print/search.client.view.html',
      })
      .state('search.tv', {
        url: '/tv',
        templateUrl: 'modules/search/views/tv/search-tv.client.view.html'
      })
      .state('search.radio', {
        url: '/radio',
        templateUrl: 'modules/search/views/radio/search-radio.client.view.html'
      });

    $sceDelegateProvider.resourceUrlWhitelist([
       // Allow same origin resource loads.
       'self',
       // Allow loading from our assets domain.  Notice the difference between * and **.
       'http://app.digivla.id/input/**'
    ]);

    cfpLoadingBarProvider.includeSpinner = false;
  }
]);
