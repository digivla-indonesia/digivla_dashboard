$(document).ready(function() {
    $('.fancybox').fancybox({});

    $('.periodcontent').fadeOut();

    $('#select').change(function() {
        $('.periodcontent').fadeOut();
        $('.' + $(this).val()).fadeIn();
    }).triggerHandler('change');
}); 

$(function() {
	$('#popupDatepicker').datepick();
	$('#popupDatepicker2').datepick();

	/* FLOT CHART - INDEX1 */
    var f1data1 = [[1446371471000, 130], [1446457871000, 40], [1446544271000, 80], [1446630671000, 160], [1446717071000, 159], [1446803471000, 370], [1446889871000, 330], [1446976271000, 350], [1447062671000, 370], [1447149071000, 400], [1447235471000, 330], [1447321871000, 350]];
    var f1data2 = [[1446371471000, 70], [1446457871000, 45], [1446544271000, 135], [1446630671000, 101], [1446717071000, 87], [1446803471000, 145], [1446889871000, 225], [1446976271000, 321], [1447062671000, 400], [1447149071000, 300], [1447235471000, 210], [1447321871000, 187]];
    var f1data3 = [[1446371471000, 150], [1446457871000, 190], [1446544271000, 248], [1446630671000, 333], [1446717071000, 300], [1446803471000, 247], [1446889871000, 221], [1446976271000, 194], [1447062671000, 176], [1447149071000, 101], [1447235471000, 98], [1447321871000, 54]];

    var f1dataset = [
        {label: "Importasi LNG",data: f1data1},
        {label: "PGN-Pertagas",data: f1data2},
        {label: "Kerjasama Bedhtel",data: f1data3},
    ];

    var f1options = {
        series: {
            lines: { show: true, fill: true },
            points: {
                radius: 3,
                show: true
            }
        },
        colors: ["#FFFF00", "#00B3FF", '#FF7300'],
        legend: {
            labelBoxBorderColor: "#cccccc",
            container: $("#legendcontainerf1"),
            noColumns: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            tickColor: "#D1D1D1",
            borderWidth: 1,
            borderColor: "#000"
        },
        xaxis: {
            mode: "time",
            timeformat: "%d-%b-%y"
        }
    };

    $.plot($("#media-visibility-flot-chart"), f1dataset, f1options);
    $("#media-visibility-flot-chart").bind("plotclick", function (event, pos, item) {
        if (item) { 
            /*************************************************
             *              ONCLICK CHARTNYA DISINI          *
            /*************************************************/

            /*
             *  untuk nama labelnya: item.series.label
             *  untuk valuenya: item.datapoint[1]
             */

            //alert('Label: ' + item.series.label + '\nValue: ' + item.datapoint[1]);
            $("#main-content").hide();

            $("body").oLoader({
                style: 0,
                wholeWindow: true,
                lockOverflow: true,
                image: loaderimage,
                background: '#000',
                fadeInTime: 500,
                fadeOutTime: 1000,
                fadeLevel: 0.4
            });

            setTimeout(function(){
                $("body").oLoader("hide");
                $("#result").show();
            }, 2500);
        }
    });
    /* END OF FLOT CHART - INDEX1 */

    $( '#dl-menu' ).dlmenu();
});

var config = {
	'.chosen-select-no-single' : {disable_search_threshold:10,disable_search:true}
}
for (var selector in config) {
	$(selector).chosen(config[selector]);
}

(function($){
    $("#result").hide();

	$(window).load(function(){
		$("#highlight").mCustomScrollbar({
			theme:"rounded-dots", 
		});
		$("#content-8").mCustomScrollbar({
			axis:"yx",
			theme:"rounded-dots"
		});
	});
})(jQuery);