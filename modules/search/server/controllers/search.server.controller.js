'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  async = require('async'),
  restService = require(path.resolve('./modules/search/server/models/search.server.api.model')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  nodeService = require(path.resolve('./modules/search/server/models/api-node.model.js')),
  request = require('request'),
  crypto = require('crypto'),
  os = require('os'),
  _ = require('lodash');

/**
 * Search Article
 */

exports.searchArticle = function(req, res) {
  var url = global.apiAddress + '/article/search';
  var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
  var articleLimit = 10;
  var offset = req.body.offset || 0;
  var token = req.user.token;
  var mediaType = req.body.media_type;
  var time_frame = req.body.time_frame;
  var date_from = req.body.date_from;
  var date_to = req.body.date_to;
  var mediaTypeArr = [];

  if(time_frame == 0 && (date_from == '' || date_to == '')){
    res.status(400).send({});
    return;
  }

  switch(mediaType){
    case 'Print Media':
      mediaTypeArr = [1, 2, 3, 6, 7, 9, 11];
      break;
    case 'Online Media':
      mediaTypeArr = [4, 5];
      break;
    case 'TV':
      mediaTypeArr = [12];
      break;
    case 'Radio':
      mediaTypeArr = [13];
      break;
    case 'All Media':
      mediaTypeArr = [];
      break;
    default:
      mediaTypeArr = [];
      break;
  }

  async.waterfall([

    function(cb){
      if(mediaTypeArr.length > 0){
        var items = {
          'token': token,
          'statuse': ['A'],
          'media_type_id': mediaTypeArr
        }, uriSegment= {
          index: '/tb_media'
        };

        restService(items, uriSegment, function(resp){
          if(!resp.success){
            cb(null, []);
          } else{
            var result = resp.data.data;
            var medIdArr = [];

            for(var i=0;i<result.length;i++){
              medIdArr.push(result[i].media_id);
            }

            cb(null, medIdArr);
          }

        });

      } else{
        cb(null, []);
      }

    },
    function(medIdArr, cb){
      var items = {
        'token': token,
        'statuse': ['A']
      }, uriSegment= {
        index: '/tb_media'
      };

      restService(items, uriSegment, function(resps){
        var results = resps.data.data;
        var mediaJson = {};

        for(var i=0;i<results.length;i++){
          mediaJson[results[i].media_id.toString()] = results[i].media_name;
        }

        cb(null, medIdArr, mediaJson);
      });
    },
    function(medIdArr, mediaJson, cb){
      var dateProcessing = dateFormatting(time_frame, date_from, date_to);

      var keyword = req.body.keyword;
      var cleanKeyword = [];
      var x;

      var selectedKeyword = keyword.match(/\w+|"[^"]+"/g);
      for(var k=0;k<selectedKeyword.length;k++){
        x = selectedKeyword[k];
        x = x.replace(/\"/g, "");
        x = x.replace(/\'/g, "");

        // temp = results.content.replace(new RegExp(x, "ig"), "<span style='color:#f89406; font-weight:bolder;'>" + x + "</span>");
        // results.content = temp;
        cleanKeyword.push(x);
      }

      var items = {
        'keyword': cleanKeyword,
        'limit': articleLimit,
        'offset': offset,
        'token': req.user.token,
        'media_id': medIdArr,
        'from': dateProcessing.date_from,
        'to': dateProcessing.date_to
      }, uriSegment= {
        index: '/article/search'
      };

      restService(items, uriSegment, function(resp){
        if(!resp.success){
          cb('Cannot search article', null);
        } else{
          var results = resp.data;

          for(var i=0;i<results.data.length;i++){
            results.data[i].media_name = mediaJson[results.data[i].media_id.toString()];
          }

          cb(null, {
            data: results.data,
            pagination: {
              total_articles: results.total,
              total_page: Math.ceil(results.total / articleLimit),
              offset: offset,
              limit: articleLimit
            }
          });
        }
      });
    }
  ],
  function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.getCategoryList = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      items = {
        token: token,
        client_id: client_id
      },
      uriSegment= {
        index: '/tb_category_list'
      };

  restService(items, uriSegment, function(resp){
    if(!resp.success){
      res.status(400).send(resp.data);
      return;
    }

    return res.status(200).send(resp.data.data);
  });
};

exports.saveArticle = function(req, res){
  var dateFormatter = function(tgl){
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2) + ' ' + ('0' + tgl.getHours()).slice(-2) + ':' + ('0' + tgl.getMinutes()).slice(-2) + ':' + ('0' + tgl.getSeconds()).slice(-2);
  };

  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      article = req.body,
      mmcol = parseInt(article.mmcol),
      rate_fc = parseInt(article.rate_fc),
      rate_bw = parseInt(article.rate_bw),
      tasksToGo = article.categories.length,
      uriSegment = {
        first: 'article_id',
        second: 'insert'
      }, items = {
        token: token,
        client_id: client_id,
        article_id: article.article_id,
        datee: article.datee.substring(0, 10),
        media_id: parseInt(article.media_id),
        mention_times: 0,
        tone: 0,
        exposure: 50,
        advalue_fc: mmcol * rate_fc,
        circulation: article.circulation,
        advalue_bw: mmcol * rate_bw,
        data_input_date: dateFormatter(new Date()),
        usere: req.user.user_detail.client_id,
        pc_name: os.hostname()
      };

  async.forEach(article.categories, function(item, cb){
    items.category_id = item;
    items.id = article.article_id.toString().concat(crypto.createHash('md5').update(item).digest("hex"));

    nodeService(uriSegment, items, function(resp){
      if (!resp.success) {
        cb('error when saving article', null);
      }

      if(--tasksToGo === 0){
        return cb(null, 'success inserting article into data_client');
      }

    });
  }, function(err, result){
    return res.status(400);
  });

  return res.status(200).send('success');
};

/**
 * Detail Article
 */
exports.detailArticle = function(req, res) {
  var token = req.user.token,
      article_id = req.query._id,
      client_id = req.user.user_detail.client_id;


  async.waterfall([
    function(cb){
      var items = {
        'token': token,
        'statuse': ['A']
      }, uriSegment= {
        index: '/tb_media'
      };

      restService(items, uriSegment, function(resps){
        var results = resps.data.data;
        var mediaJson = {};

        for(var i=0;i<results.length;i++){
          mediaJson[results[i].media_id.toString()] = results[i].media_name;
        }

        cb(null, mediaJson);
      });
    },
    function(mediaJson, cb){
      var uriSegment= {
        first: 'article',
        second: 'detail'
      };

      var apiItem = {
        token: token,
        article_id: parseInt(article_id),
        client_id: client_id
      };

      nodeService(uriSegment, apiItem, function(response){
        if(!response.success){
          cb(err, null);
        }

        response.data.data.media_name = mediaJson[response.data.data.media_id.toString()];

        cb(null, response.data.data);
      });
    },
    function(result, cb){
      var uriSegment = {
        first: 'client',
        second: 'category'
      };

      var apiItem = {
        token: token,
        article_id: article_id,
        client_id: client_id
      };

      nodeService(uriSegment, apiItem, function(response){
        var categories = [];
        response = response.data.data;

        for(var i=0;i<response.length;i++){
          categories.push(response[i]._source.category_id);
        }

        result.categories = categories;

        cb(null, result);
      });
    },
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.generatePDF = function(req, res) {
  var fs = require('fs');
  var compLogo = 'http://admin.antara-insight.id/asset/images/' + req.user.user_detail.logo_head;
  var token = req.user.token;
  var type = req.body.type;
  var article_id = req.body.article_id;
  var article_detail = req.body.article_detail;
  var client_id = req.user.user_detail.client_id;
  var type = req.body.type || 'teks_pdf';

  request.defaults({
    encoding: null
  }).get(compLogo, function(error, response, body) {
    var data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');

    var dd = {
      content: [{
          style: 'tableExample',
          color: '#444',
          table: {
            body: [
              [{
                rowSpan: 4,
                image: data,
                fit: [100, 100]
              }, {
                colSpan: 2,
                text: 'Judul  : ' + article_detail.title + ''
              }, ''],
              ['', 'Media  : ' + article_detail.media_name + '', 'Wartawan          : ' + article_detail.journalist + ''],
              ['', 'Tanggal  : ' + article_detail.datee.substring(0, 10) + '', 'Nada Pemberitaan  : '],
              ['', 'Halaman  : ' + article_detail.page + '', '']
            ]
          }
        },
        ' ',
        ' '
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
      defaultStyle: {
        alignment: 'justify'
      }
    };


    // Getting the content
    if (type === 'teks_pdf') {
      var reqJson = {
        "token": token,
        "client_id": client_id,
        "article_id": [article_id.toString()]
      };
      var uriSegment = {
        first: 'summary',
        second: 'search'
      };

      nodeService(uriSegment, reqJson, function(resp) {
        var summary = 'Belum ada summary';
        if (typeof resp.data === 'array' && resp.data.length > 0) {
          summary = resp.data[0]._source.summary;
        }

        // dd.content.push(summary);

        // sementara pake content dulu
        dd.content.push(article_detail.content);

        return res.status(200).send(dd);
      });
    }
  });
};


exports.generateWord = function(req, res) {
  var chartsData = req.query.data || [];
  var comp_logo = req.user.user_detail.logo_head;
  var comp_name = req.user.user_detail.usr_comp_name;
  var tmpfolder = './modules/dashboards/server/temp/';
  var docx = officegen('docx');
  var slide;
  var filename = req.user.user_detail.client_id + '_' + req.user.user_detail.usr_uid.toString() + '_' + Date.now().toString() + '.docx';

  chartsData = new Buffer(chartsData, 'base64').toString('utf8');
  chartsData = JSON.parse(chartsData);
  // console.log(JSON.stringify(chartsData));

  /*Downloading image*/
  var download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
      // console.log('content-type:', res.headers['content-type']);
      // console.log('content-length:', res.headers['content-length']);

      request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
  };
  /*Downloading image*/

  res.writeHead(200, {
    "x-filename": filename,
    "Content-Type": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    'Content-disposition': 'attachment; filename=' + filename
  });


  var pObj = docx.createP();

  pObj.addText('Judul      : ' + chartsData.title);
  pObj.addLineBreak();
  pObj.addText('Media      : ' + chartsData.media_name);
  pObj.addLineBreak();
  pObj.addText('Wartawan   : ' + chartsData.journalist);
  pObj.addLineBreak();
  pObj.addText('Tanggal      : ' + chartsData.datee.substring(0, 10));
  // pObj.addLineBreak ();
  // pObj.addText ('Nada Pemberitaan : Negatif');
  pObj.addLineBreak();
  pObj.addText('Halaman    : ' + chartsData.page);


  var pObj = docx.createP({
    align: 'center'
  });
  pObj.addLineBreak();
  pObj.addLineBreak();
  pObj.addLineBreak();
  pObj.addLineBreak();
  pObj.addText(chartsData.content);

  // download('http://app.digivla.id/admin/asset/images/' + comp_logo, path.resolve(tmpfolder + comp_logo), function(){
  // console.log('image downloaded');
  // adding comp logo
  // slide = pptx.makeNewSlide();
  // slide.addText(comp_name);
  // slide.addImage('http://app.digivla.id/admin/asset/images/' + comp_logo);

  async.parallel([function(chartInfo, callback) {
      docx.on('finalize', function(written) {
        console.log('Finish to create a PowerPoint file.\nTotal bytes created: ' + written + '\n');
      });

      docx.generate(res);
    }],
    function(err) {
      docx.on('error', function(err) {
        console.log(err);
        return res.status(400);
      });

      // return docx.generate(res);
    });
};

function dateFormatting(timeFrame, df, dt){
  var currentDate = new Date(),
    dateFlag = new Date(),
    interval = 'day',
    date_from = df,
    date_to = dt;

  var dateFormatter = function(tgl){
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  }

  // fixing for not matched date
  currentDate.setDate(currentDate.getDate() + 1);

  if(timeFrame !== 0){
      switch(timeFrame){
          case 1:
            dateFlag.setDate(dateFlag.getDate() - 1);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 7:
            dateFlag.setDate(dateFlag.getDate() - 7);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 30:
            dateFlag.setDate(dateFlag.getDate() - 30);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 365:
            dateFlag.setDate(dateFlag.getDate() - 365);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            interval = 'month';
            break;
      }
  } else{
    var oneDay = 24*60*60*1000;
    var firstDate = new Date(date_from);
    var secondDate = new Date(date_to);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

    // fixing for not matched date
    // date_to = dateFormatter(currentDate);

    if(diffDays > 90){
      interval = 'month';
    }
  }

  return {
    date_from: date_from,
    date_to: date_to,
    interval: interval
  };
}
