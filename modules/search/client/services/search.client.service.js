'use strict';

angular.module('search').factory('SearchArticle', ['$resource', function($resource) {
    // Posts service logic
    return $resource('/api/search_article', {
	update: {
	    method: 'GET'
	}
    });
  }
]);
