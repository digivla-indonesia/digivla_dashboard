'use strict';

angular.module('summary').config(['$stateProvider', '$sceDelegateProvider', 'cfpLoadingBarProvider',
    function($stateProvider, $sceDelegateProvider, cfpLoadingBarProvider) {
        $stateProvider
            .state('summary', {
                abstract: true,
                url: '/summary',
                template: '<ui-view/>',
                data: {
                    roles: ['user', 'admin']
                }
            })
            .state('summary.print', {
                url: '',
                templateUrl: 'modules/summary/views/print/summary.client.view.html'
            })
            .state('summary.tv', {
                url: '/tv',
                templateUrl: 'modules/summary/views/tv/summary-tv.client.view.html'
            })
            .state('summary.radio', {
                url: '/radio',
                templateUrl: 'modules/summary/views/radio/summary-radio.client.view.html'
            });

        $sceDelegateProvider.resourceUrlWhitelist([
           // Allow same origin resource loads.
           'self',
           // Allow loading from our assets domain.  Notice the difference between * and **.
           'http://app.digivla.id/input/**'
        ]);


        cfpLoadingBarProvider.includeSpinner = false;
    }
]);
