'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  elasticSearch = require('elasticsearch'),
  request = require('request'),
  fs = require('fs'),
  officegen = require('officegen'),
  async = require('async'),
  accounting = require('accounting'),
  restService = require(path.resolve('./modules/dashboards/server/models/dashboard.server.api.model')),
  nodeService = require(path.resolve('./modules/summary/server/models/api-node.model.js')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  elasticService = require(path.resolve('./modules/dashboards/server/models/elastic.model.js')),
  // nodeService = require(path.resolve('./modules/dashboards/server/models/api-node.model.js')),
  jsonTemplates = require(path.resolve('./modules/dashboards/server/models/template-json.model.js'));


/**
 *  Controller for dashboard menu
 *  Based on app.digivla.id
 */

var client = new elasticSearch.Client({
  host: 'elastic.antara-insight.id',
  log: 'trace'
});

exports.getProcess = function(req, res){
  var client_id = req.user.user_detail.client_id,
      token = req.user.token,
      group_category = req.query.gc,
      media_set = req.query.ms,
      category_id = req.query.sc,
      media_id = req.query.mi.toString(),
      time_frame = parseInt(req.query.tf),
      date_from = req.query.df,
      date_to = req.query.dt,
      uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

  if(time_frame == 0 && (date_from == '' || date_to == '')){
    res.status(400).send({});
    return;
  }

  async.waterfall([
    function(cb){
      // getMediaId(client_id, media_set, function(medArr){
      //   getCategorySet(client_id, group_category, false, function(catArr){
      getMediaId(client_id, media_set, token, function(medArr){
        getCategorySet(client_id, group_category, true, token, function(catArr){
          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          if(category_id !== 'All Category'){
            catArr = {
              success: true,
              data: {
                processed: [{
                  match: {
                    category_id: {
                      query: category_id,
                      operator: 'and'
                    }
                  }
                }],
                raw: [category_id]
              }
            };
          }

          cb(null, medArr, catArr);
        });
      });
    },
    function(medArr, catArr, cb){
      var dateProcessing = dateFormatting(time_frame, date_from, date_to);

      var items = {
        category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
        media: (medArr.data.length > 0) ? medArr.data : [],
        date_from: dateProcessing.date_from,
          date_to: dateProcessing.date_to,
          interval: dateProcessing.interval
      };

      var params = jsonTemplates.getProcess(items);
      cb(null, catArr, params);
    },
    function(catArr, params, cb){
      elasticService(uriSegment, params, function(resp){
        var arrayForChart = [];
        var result = resp.data.aggregations.category_id.buckets;

        for(var i=0;i<result.length;i++){
          if(result[i].doc_count > 0 && catArr.data.raw.indexOf(result[i].key) > -1){
            var buckets = result[i];
            var bucketsArray = [buckets.key, buckets.doc_count];

            arrayForChart.push(bucketsArray);
          }
        }

        cb(null, arrayForChart);
      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.getProcess3 = function(req, res){
  var client_id = req.user.user_detail.client_id,
      token = req.user.token,
      group_category = req.query.gc,
      media_set = req.query.ms,
      category_id = req.query.sc,
      media_id = req.query.mi.toString(),
      time_frame = parseInt(req.query.tf),
      date_from = req.query.df,
      date_to = req.query.dt,
      code = req.query.code,
      currentDate = new Date(),
      dateFlag = new Date(),
      interval = 'day',
      uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

  async.waterfall([
    function(cb){
      // getMediaId(client_id, media_set, function(medArr){
      //   getCategorySet(client_id, group_category, false, function(catArr){
      getMediaId(client_id, media_set, token, function(medArr){
        getCategorySet(client_id, group_category, false, token, function(catArr){
          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          if(category_id !== 'All Category'){
            catArr = {
              success: true,
              data: [{
                match: {
                  category_id: {
                    query: category_id,
                    operator: 'and'
                  }
                }
              }]
            };
          }

          cb(null, medArr, catArr);
        });
      });
    },
    function(medArr, catArr, cb){
      var dateProcessing = dateFormatting(time_frame, date_from, date_to);

      var items = {
        category: (catArr.data.length > 0) ? catArr.data : [],
        media: (medArr.data.length > 0) ? medArr.data : [],
        date_from: dateProcessing.date_from,
        date_to: dateProcessing.date_to,
        interval: dateProcessing.interval,
        code: code
      };

      var params = jsonTemplates.getProcess3(items);

      cb(null, params);
    },
    function(params, cb){
      elasticService(uriSegment, params, function(resp){
        var arrayForCategory = [];
        var arrayForSeries = [
          {
            name: 'Positive',
            data: []
          }, {
            name: 'Neutral',
            data: []
          }, {
            name: 'Negative',
            data: []
          }
        ];
              var result = resp.data.aggregations.datagroup.buckets;
console.log(JSON.stringify(result));
        if(code === '050'){
          var categoryDetail = {};

          getMedia(function(medList){
            for(var i=0;i<result.length;i++){
              var buckets = result[i]
              var bucketsArray = [];
              var tempJSON = {};

              arrayForCategory.push(medList.data[buckets.key.toString()]);
              categoryDetail[medList.data[buckets.key.toString()]] = buckets.key.toString();

              for(var j=0;j<buckets.tone.buckets.length;j++){
                tempJSON[buckets.tone.buckets[j].key.toString()] = buckets.tone.buckets[j].doc_count;
              }

              arrayForSeries[0].data.push((tempJSON.hasOwnProperty('1')) ? tempJSON['1'] : 0);
              arrayForSeries[1].data.push((tempJSON.hasOwnProperty('0')) ? tempJSON['0'] : 0);
              arrayForSeries[2].data.push((tempJSON.hasOwnProperty('-1')) ? tempJSON['-1'] : 0);
            }

            cb(null, {
              category: arrayForCategory,
              series: arrayForSeries,
              category_detail: categoryDetail
            });
          });

        } else if(code === '018'){

          for(var i=0;i<result.length;i++){
            var buckets = result[i]
            var bucketsArray = [];
            var tempJSON = {};
            var dateFormatter = function(tgl){
              return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
            }

            arrayForCategory.push(dateFormatter(new Date(buckets.key)));

            for(var j=0;j<buckets.tone.buckets.length;j++){
              tempJSON[buckets.tone.buckets[j].key.toString()] = buckets.tone.buckets[j].doc_count;
            }

            arrayForSeries[0].data.push((tempJSON.hasOwnProperty('1')) ? tempJSON['1'] : 0);
            arrayForSeries[1].data.push((tempJSON.hasOwnProperty('0')) ? tempJSON['0'] : 0);
            arrayForSeries[2].data.push((tempJSON.hasOwnProperty('-1')) ? tempJSON['-1'] : 0);
          }

          cb(null, {
            category: arrayForCategory,
            series: arrayForSeries
          });

        } else{
          for(var i=0;i<result.length;i++){
            var buckets = result[i]
            var bucketsArray = [];
            var tempJSON = {};

            arrayForCategory.push(buckets.key);

            for(var j=0;j<buckets.tone.buckets.length;j++){
              tempJSON[buckets.tone.buckets[j].key.toString()] = buckets.tone.buckets[j].doc_count;
            }

            arrayForSeries[0].data.push((tempJSON.hasOwnProperty('1')) ? tempJSON['1'] : 0);
            arrayForSeries[1].data.push((tempJSON.hasOwnProperty('0')) ? tempJSON['0'] : 0);
            arrayForSeries[2].data.push((tempJSON.hasOwnProperty('-1')) ? tempJSON['-1'] : 0);
          }

          cb(null, {
            category: arrayForCategory,
            series: arrayForSeries
          });
        }
      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.getProcess4 = function(req, res){
  var client_id = req.user.user_detail.client_id,
      token = req.user.token,
      group_category = req.query.gc,
      media_set = req.query.ms,
      category_id = req.query.sc,
      media_id = req.query.mi.toString(),
      time_frame = parseInt(req.query.tf),
      date_from = req.query.df,
      date_to = req.query.dt,
      uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

  if(time_frame == 0 && (date_from == '' || date_to == '')){
    res.status(400).send({});
    return;
  }

  async.waterfall([
    function(cb){
      // getMediaId(client_id, media_set, function(medArr){
      //   getCategorySet(client_id, group_category, false, function(catArr){
      getMediaId(client_id, media_set, token, function(medArr){
        getCategorySet(client_id, group_category, true, token, function(catArr){
          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          if(category_id !== 'All Category'){
            catArr = {
              success: true,
              data: {
                processed: [{
                  match: {
                    category_id: {
                      query: category_id,
                      operator: 'and'
                    }
                  }
                }],
                raw: [category_id]
              }
            };
          }

          cb(null, medArr, catArr);
        });
      });
    },
    function(medArr, catArr, cb){
      var dateProcessing = dateFormatting(time_frame, date_from, date_to);

      var items = {
        // category: (catArr.data.length > 0) ? catArr.data : [],
        category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
        media: (medArr.data.length > 0) ? medArr.data : [],
        date_from: dateProcessing.date_from,
          date_to: dateProcessing.date_to,
          interval: dateProcessing.interval
      };

      var params = jsonTemplates.getProcess4(items);

      cb(null, catArr, params);
    },
    function(catArr, params, cb){
      elasticService(uriSegment, params, function(resp){
        
        // console.log('params: ' + JSON.stringify(params));
        // console.log('response: ' + JSON.stringify(resp));
        // console.log('catArr: ' + JSON.stringify(catArr));

        var jsonForChart = {};
        var categories = [];
        var arrayForChart = [];
        var result = resp.data.aggregations.datagroup.buckets;
        var formatDate = function(tgl){
          return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
        };
        var monthString = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var firstDate = '';
        var lastDate = '';
        var highestDate = '';
        var highestValue = 0;
        var highestCategory = '';

        //for looping xAxis category
        for(var i=0;i<result.length;i++){
          var buckets = result[i]
          var categoryBuckets = buckets.category.buckets;

          for(var j=0;j<categoryBuckets.length;j++){
            //var dateKey = formatDate(new Date(categoryBuckets[j].key));
            var dateKey = categoryBuckets[j].key_as_string;
            //console.log(categories.indexOf(dateKey));
            // console.log(categoryBuckets[j].key_as_string);
            if(categories.indexOf(dateKey) == -1){
              //firstDate = dateKey.substring(0, 5);
              //lastDate = dateKey.length - 3;
              //lastDate = dateKey.substring(lastDate, dateKey.length);
              categories.push(dateKey);
            }
          }
        }
        categories.sort();

        for(var i=0;i<result.length;i++){
          if(catArr.data.raw.indexOf(result[i].key) > -1){

            var buckets = result[i];
            var categoryBuckets = buckets.category.buckets;
            var resDateArr = [];
            var tempArray = [];

            for(var j=0;j<categoryBuckets.length;j++){
              resDateArr.push({
                date: categoryBuckets[j].key_as_string,
                value: categoryBuckets[j].doc_count
              });
            }

            for(var j=0;j<categories.length;j++){
              var tempValue = 0;
              for(var k=0;k<resDateArr.length;k++){
                if(resDateArr[k].date === categories[j]){
                  // GET HIGHEST
                  if(resDateArr[k].value > highestValue){
                    highestCategory = result[i].key;
                    highestDate = resDateArr[k].date;
                    highestValue = resDateArr[k].value;
                  }

                  tempValue = resDateArr[k].value;
                  break;
                }
              }
              tempArray.push(tempValue);
            }

            arrayForChart.push({
              name: buckets.key,
              data: tempArray
            });
          }
        }

        cb(null, {
          categories: categories,
          series: arrayForChart,
          highest: {
            category: highestCategory,
            name: highestDate,
            value: highestValue
          }
        });
      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.getProcess6 = function(req, res){
  var client_id = req.user.user_detail.client_id,
      group_category = req.query.gc,
      media_set = req.query.ms,
      token = req.user.token,
      category_id = req.query.sc,
      media_id = req.query.mi.toString(),
      // time_frame = (parseInt(req.query.tf) > 7 || parseInt(req.query.tf) < 7) ? 30 : parseInt(req.query.tf),
      time_frame = (parseInt(req.query.tf) > 30) ? 30 : parseInt(req.query.tf),
      // time_frame = 30,
      date_from = req.query.df,
      date_to = req.query.dt,
      uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      },
      currentDate = new Date(),
      dateFlag = new Date(),
      interval_ = 'day',
      today_ = {},
      arr_loop = [],
      agg_date = {},
      interval_ = 'day',
      arry_juma = [],
      arry_jumb = [],
      pivot_tone;

  var dateFormatter = function(tgl){
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  };

  if(time_frame == 0){
    var start = new Date(date_from);
    var end = new Date(date_to);
    var days_between = Math.abs(end.getTime() - start.getTime());
    days_between = Math.ceil(days_between / (1000*3600*24));
    interval_ = (days_between > 90) ? "month" : "day";
  } else{
    interval_ = (time_frame == 365) ? "month" : "day";
  }

  if(time_frame > 0){

    var min_days = time_frame,
      max_days = min_days * 2,
      currentDate = new Date();

    for(var i=1;i<=max_days;i++){
      var tempDate = new Date();
      var arrIndex = 'today_' + i.toString();

      tempDate.setDate(tempDate.getDate()-i+1);

      today_[arrIndex] = dateFormatter(tempDate);
    }

    for(var i=1;i<=min_days;i++){
      arr_loop.push(min_days + i);
    }

  } else{
    var days_between = Math.abs(end.getTime() - start.getTime());
    days_between = Math.ceil(days_between / (1000*3600*24));

    if(days_between > 31){
      days_between = 31;
    }

    var min_days = days_between + 1,
      max_days = min_days * 2,
      currentDate = new Date();

    for(var i=1;i<=max_days;i++){
      var tempDate = new Date(date_to);
      var arrIndex = 'today_' + i.toString();

      tempDate.setDate(tempDate.getDate()-i + 1);

      today_[arrIndex] = dateFormatter(tempDate);
    }

    for(var i=1;i<=min_days;i++){
      arr_loop.push(min_days + i);
    }
  }

  for(var i=0;i<arr_loop.length;i++){
    var x = i + 1;
    var nmvar1 = 'today_' + x.toString();
    var nmvar2 = 'today_' + arr_loop[i];

    agg_date[today_[nmvar1]] = {
      'filter': {
        'range': getMediaDate(0, today_[nmvar2], today_[nmvar1], 'datee')
      }, 'aggs': {
        'datagroup': {'terms': {'field': 'category_id.raw', 'size': 0}}
      }
    }
  }

  async.waterfall([
    function(cb){
      // getMediaId(client_id, media_set, function(medArr){
      //   getCategorySet(client_id, group_category, true, function(catArr){
      getMediaId(client_id, media_set, token, function(medArr){
        getCategorySet(client_id, group_category, true, token, function(catArr){
          if(!medArr.success || !catArr.success){
            res.status(400);
            return cb('Error getting media or category', null);
          }

          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          if(category_id !== 'All Category'){
            catArr = {
              success: true,
              data: {
                processed: [{
                  match: {
                    category_id: {
                      query: category_id,
                      operator: 'and'
                    }
                  }
                }],
                raw: [category_id]
              }
            };
          }

          cb(null, medArr, catArr);
        });
      });
    },
    function(medArr, catArr, cb){
      var items = {
        category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
        media: (medArr.data.length > 0) ? medArr.data : [],
        aggregations: agg_date,
        step: 1
      };
      var params = jsonTemplates.getProcess6(items);

      cb(null, params, medArr, catArr);
    },
    function(params, medArr, catArr, cb){
      elasticService(uriSegment, params, function(resp){
        /************************************/
        var firstAggregations = resp.data.aggregations;

        /****** OLAH PENCARIAN PERTAMA ******/
        for(var i=0;i<arr_loop.length;i++){
          var x = i + 1;
          var nmvar1 = 'today_' + x.toString();
          //var valjj = resp.data.aggregations[today_[nmvar1]].datagroup.buckets;
          var valjj = firstAggregations[today_[nmvar1]].datagroup.buckets;

          if(valjj.length > 0){
            var arrTmp = [];
            for(var j=0;j<valjj.length;j++){
              arrTmp.push(valjj[j].key);
              arry_juma.push({
                tgl: today_[nmvar1],
                category_id: valjj[j].key,
                juma: valjj[j].doc_count
              });
            }

            for(var j=0;j<catArr.data.raw.length;j++){
              if(arrTmp.indexOf(catArr.data.raw[j]) == -1){
                arry_juma.push({
                  tgl: today_[nmvar1],
                  category_id: catArr.data.raw[j],
                  juma: 0
                });
              }
            }
          } else{
            for(var j=0;j<catArr.data.raw.length;j++){
              arry_juma.push({
                tgl: today_[nmvar1],
                category_id: catArr.data.raw[j],
                juma: 0
              });
            }
          }
        }
        /****** OLAH PENCARIAN PERTAMA ******/

        // Step 2
        var items = {
          category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
          media: (medArr.data.length > 0) ? medArr.data : [],
          aggregations: agg_date,
          step: 2
        };

        params = jsonTemplates.getProcess6(items);
        /************************************/

        cb(null, params, medArr, catArr);
      });
    },
    function(params, medArr, catArr, cb){
      elasticService(uriSegment, params, function(resp){

        var secondAggregations = resp.data.aggregations;
        for(var i=0;i<arr_loop.length;i++){
          var x = i + 1;
          var nmvar1 = 'today_' + x.toString();
          var valjj = secondAggregations[today_[nmvar1]].datagroup.buckets;

          if(valjj.length > 0){
            var arrTmp = [];
            for(var j=0;j<valjj.length;j++){
              arrTmp.push(valjj[j].key);
              arry_jumb.push({
                tgl: today_[nmvar1],
                category_id: valjj[j].key,
                jumb: valjj[j].doc_count
              });
            }

            for(var j=0;j<catArr.data.raw.length;j++){
              if(arrTmp.indexOf(catArr.data.raw[j]) == -1){
                arry_jumb.push({
                  tgl: today_[nmvar1],
                  category_id: catArr.data.raw[j],
                  jumb: 0
                });
              }
            }
          } else{
            for(var j=0;j<catArr.data.raw.length;j++){
              arry_jumb.push({
                tgl: today_[nmvar1],
                category_id: catArr.data.raw[j],
                jumb: 0
              });
            }
          }
        }

        pivot_tone = mergingPivot(arry_juma, arry_jumb);
        var index_pivot = 'pivot_tone_' + Date.now().toString();
        var type_pivot = 'listdata';

        cb(null, index_pivot, type_pivot, pivot_tone);
      });
    },
    function(index_pivot, type_pivot, pivot_tone, cb){
      mappingPivotTone(index_pivot, type_pivot, pivot_tone, function(pivotResp){
        if(pivotResp){
          elasticService({index: index_pivot + '/_refresh', type: false, method: 'GET'}, {}, function(refResp){
            var items = {
              interval: interval_
            };
            var paramPivot = jsonTemplates.getPivotTones(items);

            if(refResp.data._shards.failed == 0){
              elasticService({index: index_pivot, type: type_pivot, method: 'POST'}, paramPivot, function(pivotResp){
                var resultForEws = pivotResp.data.aggregations.category.buckets;

                elasticService({index: index_pivot + '/', type: false, method: 'DELETE'}, {}, function(delResp){

                  // for trending highlight
                  var thCategory = '';
                  var yesterdate = new Date();
                  yesterdate.setDate(yesterdate.getDate() - 1);
                  yesterdate = dateFormatter(yesterdate);
                  var highestValueForTh = 0;

                  var categories = [];
                  var series = [];

                  // ews summary assets
                  var potentialCategoriesArray = [];
                  var emergingCategoriesArray = [];
                  var currentCategoriesArray = [];
                  var crisisCategoriesArray = [];

                  for(var i=0;i<resultForEws.length;i++){
                    var tglBuckets = resultForEws[i].tgl.buckets;
                    var ewsItem = {};
                    var tempData = [];
                    var sumFromJumlah = 0;

                    ewsItem.name = resultForEws[i].key;
                    ewsItem.data = [];

                    for(var j=0;j<tglBuckets.length;j++){
                      // Get categories
                      if(categories.indexOf(tglBuckets[j].key_as_string) == -1){
                        categories.push(tglBuckets[j].key_as_string);
                      }

                      // Get Trending Highlight category
                      if(yesterdate === tglBuckets[j].key_as_string){
                        if(highestValueForTh < tglBuckets[j].sum_jumlah.value){
                          highestValueForTh = tglBuckets[j].sum_jumlah.value;
                          thCategory = resultForEws[i].key;
                        }
                      }

                      sumFromJumlah += tglBuckets[j].sum_jumlah.value;

                      ewsItem.data.push(tglBuckets[j].sum_jumlah.value);
                    }

                    var meanFromJumlah = sumFromJumlah / tglBuckets.length;

                    if(meanFromJumlah <= 1){
                      potentialCategoriesArray.push(resultForEws[i].key);
                    } else if(meanFromJumlah > 1 && meanFromJumlah <= 2){
                      emergingCategoriesArray.push(resultForEws[i].key);
                    } else if(meanFromJumlah > 2 && meanFromJumlah <= 3){
                      currentCategoriesArray.push(resultForEws[i].key);
                    } else if(meanFromJumlah > 2){
                      crisisCategoriesArray.push(resultForEws[i].key);
                    }

                    series.push(ewsItem);
                  }

                  cb(null, {
                    categories: categories,
                    series: series,
                    highlight: {
                      category: thCategory
                    },
                    summary: {
                      potential: potentialCategoriesArray,
                      emerging: emergingCategoriesArray,
                      current: currentCategoriesArray,
                      crisis: crisisCategoriesArray
                    }
                  });
                });
              });
            }
          });
        }
      });
    }
  ], function(err, result){
    if(err){
      elasticService({index: index_pivot + '/', type: false, method: 'DELETE'}, {}, function(delResp){
        console.log(err);
        return res.status(400);
      });
    }

    return res.status(200).send(result);
  });
};

exports.getArticleListFromCharts = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      time_frame = (typeof req.body.time_frame !== 'undefined') ? parseInt(req.body.time_frame) : 7,
      date_from = (typeof req.body.date_from !== 'undefined') ? req.body.date_from : '',
      date_to = (typeof req.body.date_to !== 'undefined') ? req.body.date_to : '',
      original_date_from = (typeof req.body.original_date_from !== 'undefined') ? req.body.original_date_from : '',
      original_date_to = (typeof req.body.original_date_to !== 'undefined') ? req.body.original_date_to : '',
      media_set = (typeof req.body.media_set !== 'undefined') ? req.body.media_set : '',
      media_id = (typeof req.body.media_id !== 'undefined') ? req.body.media_id : '',
      group_category = (typeof req.body.group_category !== 'undefined') ? req.body.group_category : '',
      sub_category = (typeof req.body.sub_category !== 'undefined') ? req.body.sub_category : '',
      type = (typeof req.body.type !== 'undefined') ? req.body.type.toLowerCase() : '',
      offset = (typeof req.body.offset !== 'undefined') ? parseInt(req.body.offset) : 0,
      tone = (typeof req.body.tone !== 'undefined') ? parseInt(req.body.tone) : '',
      uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      },
      subsArray = [],
      articleLimit = 10,
      items = {};

  async.waterfall([
    function(cb){

      if(type === '050'){
        // getCategorySet(client_id, group_category, false, function(catArr){
        getCategorySet(client_id, group_category, false, token, function(catArr){

          var medArr = {
            success: true,
            data: [media_id.id]
          };

          if(sub_category !== 'All Category'){
            catArr = {
              success: true,
              data: [{
                match: {
                  category_id: {
                    query: category_id,
                    operator: 'and'
                  }
                }
              }]
            };
          }

          return cb(null, medArr, catArr);
        });

      } else if(type === '018'){
        // getMediaId(client_id, media_set, function(medArr){
        //   getCategorySet(client_id, group_category, false, function(catArr){
        getMediaId(client_id, media_set, token, function(medArr){
          getCategorySet(client_id, group_category, false, token, function(catArr){
            if(media_id !== '00'){
              medArr = {
                success: true,
                data: [media_id]
              };
            }

            if(sub_category !== 'All Category'){
              catArr = {
                success: true,
                data: [{
                  match: {
                    category_id: {
                      query: category_id,
                      operator: 'and'
                    }
                  }
                }]
              };
            }

            return cb(null, medArr, catArr);
          });
        });
      } else{
        // getMediaId(client_id, media_set, function(medArr){
        getMediaId(client_id, media_set, token, function(medArr){
          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          var catArr = {
            success: true,
            data: [{
              match: {
                category_id: {
                  query: sub_category,
                  operator: 'and'
                }
              }
            }]
          };

          return cb(null, medArr, catArr);
        });

      }

    },
    function(medArr, catArr, cb){
      var params = {};

      if(type === '0'){
        var dateProcessing = dateFormatting(time_frame, date_from, date_to);

        var items = {
          category: (catArr.data.length > 0) ? catArr.data : [],
          media: (medArr.data.length > 0) ? medArr.data : [],
          date_from: dateProcessing.date_from,
          date_to: dateProcessing.date_to,
          interval: dateProcessing.interval,
          size: articleLimit,
          offset: offset
        };

        params = jsonTemplates.getProcessWithSize(items);

      } else if(type === '4'){

        var interval = 'day';
        var dateFormatter = function(tgl){
          return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
        };

        switch(time_frame){
          case 365:
            var dateFlag = new Date(date_to);
            dateFlag.setMonth(dateFlag.getMonth() + 1);
            dateFlag.setDate(dateFlag.getDate() - 1);
            date_to = dateFormatter(dateFlag);
            break;
          case 0:
            var oneDay = 24*60*60*1000;
            var firstDate = new Date(original_date_from);
            var secondDate = new Date(original_date_to);
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

            if(diffDays > 90){
              var dateFlag = new Date(date_to);
              dateFlag.setMonth(dateFlag.getMonth() + 1);
              dateFlag.setDate(dateFlag.getDate() - 1);
              date_to = dateFormatter(dateFlag);
            }
            break;
        } 

        var items = {
          category: (catArr.data.length > 0) ? catArr.data : [],
          media: (medArr.data.length > 0) ? medArr.data : [],
          date_from: date_from,
          date_to: date_to,
          interval: interval,
          size: articleLimit,
          offset: offset
        };

        params = jsonTemplates.getProcess4WithSize(items);

      } else if(type === '050' || type === '019'){
        var dateProcessing = dateFormatting(time_frame, date_from, date_to);

        var tonee = '';
        switch(req.body.tone){
          case 'Positive':
            tonee = '1';
            break;
          case 'Neutral':
            tonee = '0';
            break;
          case 'Negative':
            tonee = '-1';
            break;
        }

        var items = {
          category: (catArr.data.length > 0) ? catArr.data : [],
          media: (medArr.data.length > 0) ? medArr.data : [],
          date_from: dateProcessing.date_from,
          date_to: dateProcessing.date_to,
          interval: dateProcessing.interval,
          size: articleLimit,
          offset: offset,
          code: type,
          tone: tonee
        };

        params = jsonTemplates.getProcess3WithSize(items);
      } else if(type === '018'){

        var interval = 'day';
        var dateFormatter = function(tgl){
          return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
        };

        switch(time_frame){
          case 365:
            var dateFlag = new Date(date_to);
            dateFlag.setMonth(dateFlag.getMonth() + 1);
            dateFlag.setDate(dateFlag.getDate() - 1);
            date_to = dateFormatter(dateFlag);
            break;
          case 0:
            var oneDay = 24*60*60*1000;
            var firstDate = new Date(original_date_from);
            var secondDate = new Date(original_date_to);
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

            if(diffDays > 90){
              var dateFlag = new Date(date_to);
              dateFlag.setMonth(dateFlag.getMonth() + 1);
              dateFlag.setDate(dateFlag.getDate() - 1);
              date_to = dateFormatter(dateFlag);
            }
            break;
        } 

        var tonee = '';
        switch(req.body.tone){
          case 'Positive':
            tonee = '1';
            break;
          case 'Neutral':
            tonee = '0';
            break;
          case 'Negative':
            tonee = '-1';
            break;
        }

        var items = {
          category: (catArr.data.length > 0) ? catArr.data : [],
          media: (medArr.data.length > 0) ? medArr.data : [],
          date_from: date_from,
          date_to: date_to,
          interval: interval,
          size: articleLimit,
          offset: offset,
          code: type,
          tone: tonee
        };

        params = jsonTemplates.getProcess3WithSize(items);
      }

      cb(null, params);
    },
    function(params, cb){
      elasticService(uriSegment, params, function(resp){
        var result = (typeof resp.data.hits.hits !== 'undefined') ? resp.data.hits.hits : [];
        var arrayForCategory = [];
        var articleIdArr = [];
        var totalResult = resp.data.hits.total;
        var totalArticles = resp.data.hits.total;

        for(var i=0;i<result.length;i++){
          articleIdArr.push(parseInt(result[i]._source.article_id));
        }

        var apiItem = {
          token: token,
          article_id: articleIdArr
        };

        cb(null, apiItem, totalResult, totalArticles);
      });
    },
    function(apiItem, totalResult, totalArticles, cb){
      nodeService({first: 'article', second: 'search'}, apiItem, function(resps){
          var response = resps.data.data;

          getMedia(function(medList){

            for(var i=0;i<response.length;i++){
              var tempJson = {
                "article_id": response[i].article_id,
                "media_name": medList.data[response[i].media_id],
                "media_id": response[i].media_id,
                "datee": response[i].datee,
                "content": response[i].content,
                "journalist": response[i].journalist,
                "title": response[i].title
              };
              subsArray.push(tempJson);
            }

            cb(null, {
              data: subsArray,
              pagination: {
                total_articles: totalArticles,
                total_page: Math.ceil(totalArticles / articleLimit),
                offset: offset
              }
            });
          });
      });
    }

  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });

};

exports.getArticleList = function(req, res){
  var token = req.user.token,
      client_id = req.user.user_detail.client_id,
      date_from = (typeof req.body.date_from !== 'undefined') ? req.body.date_from : '',
      date_to = (typeof req.body.date_to !== 'undefined') ? req.body.date_to : '',
      media = (typeof req.body.media !== 'undefined') ? req.body.media : '',
      category_id = (typeof req.body.category !== 'undefined') ? req.body.category : '',
      type = (typeof req.body.type !== 'undefined') ? req.body.type.toLowerCase() : '',
      time_frame = (typeof req.body.media !== 'undefined') ? parseInt(req.body.time_frame) : 7,
      offset = (typeof req.body.offset !== 'undefined') ? parseInt(req.body.offset) : 0,
      tone = (typeof req.body.tone !== 'undefined') ? parseInt(req.body.tone) : '',
      uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      },
      subsArray = [],
      articleLimit = 10,
      items = {};

      items.client_id = client_id;
      items.token = token;
      items.limit = articleLimit;
      items.offset = offset;

  if(tone === 'Neutral')
    tone = 0;
  else if(tone === 'Positive')
    tone = 1;
  else if(tone === 'Negative')
    tone = -1;

  if(type === 'date'){
    if(time_frame == 365){
      var tempDateTo = new Date(date_to);
      tempDateTo.setMonth(tempDateTo.getMonth() + 1);
      date_to = tempDateTo.getFullYear() + '-' + ('0' + (tempDateTo.getMonth() + 1)).slice(-2) + '-' + ('0' + tempDateTo.getDate()).slice(-2);
    }

    items.category_id = [category_id];
    items.date_from = date_from;
    items.date_to = date_to;
    items.tone = [tone];

  } else if(type === 'category'){
    items.category_id = [category_id];

  } else if(type === '018'){
    if(time_frame == 365){
      var tempDateTo = new Date(date_to);
      tempDateTo.setMonth(tempDateTo.getMonth() + 1);
      date_to = tempDateTo.getFullYear() + '-' + ('0' + (tempDateTo.getMonth() + 1)).slice(-2) + '-' + ('0' + tempDateTo.getDate()).slice(-2);
    }

    items.date_from = date_from;
    items.date_to = date_to;
    items.tone = [tone];

  } else if(type === '019'){
    items.category_id = [category_id];
    items.tone = [tone];

  } else if(type === '050'){
    items.media_id = [media.id];
    items.tone = [tone];
  }

  async.waterfall([
    function(cb){
      nodeService({first: 'data_client', second: 'search'}, items, function(resp){
        var result = (typeof resp.data.data.hits.hits !== 'undefined') ? resp.data.data.hits.hits : [];
        var arrayForCategory = [];
        var articleIdArr = [];
        var totalResult = resp.data.data.hits.total;
        var totalArticles = resp.data.data.hits.total;

        for(var i=0;i<result.length;i++){
          articleIdArr.push(parseInt(result[i]._source.article_id));
          // result[i]._source.tone itu buat tonalitas, valuenya -1 0 1
        }

        var apiItem = {
          token: token,
          article_id: articleIdArr
        };

        cb(null, apiItem, totalResult, totalArticles);
      });
    },
    function(apiItem, totalResult, totalArticles, cb){
      nodeService({first: 'article', second: 'search'}, apiItem, function(resps){
          var response = resps.data.data;

          getMedia(function(medList){

            for(var i=0;i<response.length;i++){
              var tempJson = {
                "article_id": response[i].article_id,
                "media_name": medList.data[response[i].media_id],
                "media_id": response[i].media_id,
                "datee": response[i].datee,
                "content": response[i].content,
                "journalist": response[i].journalist,
                "title": response[i].title,
                "page": response[i].page.replace(/^0+/, '')
              };
              subsArray.push(tempJson);
            }

            cb(null, {
              data: subsArray,
              pagination: {
                total_articles: totalArticles,
                total_page: Math.ceil(totalArticles / articleLimit),
                offset: offset
              }
            });
          });
      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.getArticleDetail = function(req, res){
  var token = req.user.token,
      article_id = req.query._id,
      client_id = req.user.user_detail.client_id,
      uriSegment= {
        first: 'article',
        second: 'detail'
      };

  var apiItem = {
    token: token,
    article_id: parseInt(article_id)
  };

  async.waterfall([
    function(cb){
      nodeService(uriSegment, apiItem, function(detail){
        cb(null, detail);
      });
    },
    function(detail, cb){
      var items = {
        "statuse": ["A"],
        "token": token
      };

      nodeService({first: 'tb_media', second: ''}, items, function(resps){
        var results = resps.data.data;
        var mediaJson = {};

        for(var i=0;i<results.length;i++){
          mediaJson[results[i].media_id.toString()] = results[i].media_name;
        }

        cb(null, detail, mediaJson);
      });
    },
    function(detail, medLists, cb){
      var dateFormatter = function(tgl){
        return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
      };
      var date_from = '2010-01-01';
      var date_to = dateFormatter(new Date());

      uriSegment = {
        first: 'data_client',
        second: 'search'
      }, apiItem = {
        article_id: [article_id],
        token: token,
        client_id: client_id,
        date_from: date_from,
        date_to: date_to
      };

      nodeService(uriSegment, apiItem, function(resps){
        var client_detail = resps.data.data.hits.hits[0];
        var categories = [];
        var _ids = [];
        var advalue_fc = 0;
        var advalue_bw = 0;
        var newsvalue_fc = 0;
        var newsvalue_bw = 0;

        if(resps.data.data.hits.hits.length < 1){
          return cb(null, detail);
        }

        newsvalue_fc = parseInt(client_detail._source.advalue_fc);
        newsvalue_bw = parseInt(client_detail._source.advalue_bw);
        advalue_fc = Math.ceil(newsvalue_fc * 0.34);
        advalue_bw = Math.ceil(newsvalue_bw * 0.34);

        for(var i=0;i<resps.data.data.hits.hits.length;i++){
          categories.push(resps.data.data.hits.hits[i]._source.category_id);
          _ids.push(resps.data.data.hits.hits[i]._source.id);
        }

        var values = {
          newsvalue_bw: accounting.formatMoney(newsvalue_bw, "Rp ", 2, ".", ","),
          newsvalue_fc: accounting.formatMoney(newsvalue_fc, "Rp ", 2, ".", ","),
          advalue_bw: accounting.formatMoney(advalue_bw, "Rp ", 2, ".", ","),
          advalue_fc: accounting.formatMoney(advalue_fc, "Rp ", 2, ".", ",")
        };

        detail.data.data.values = values;
        detail.data.data.categories = categories;
        detail.data.data._id = _ids;
        // media name
        detail.data.data.media_name = medLists[detail.data.data.media_id];

        cb(null, categories, detail);
      });

    },
    function(categories, detail, cb){
      uriSegment = {
        first: 'tb_keyword',
        second: ''
      }, apiItem = {
        token: token,
        client_id: client_id,
        category_id: categories
      };

      nodeService(uriSegment, apiItem, function(resps){
        var keywords = resps.data.data;
        var cleanKeywords = [];

        for(var i=0;i<keywords.length;i++){
          // cleanKeywords.push(keywords[i].keyword.replace(/"/g, ''));
          cleanKeywords.push(keywords[i].keyword);
        }

        detail.data.data.keywords = cleanKeywords;
        cb(null, detail);
      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    return res.status(200).send(result);
  });
};

exports.downloadPPTPost = function(req, res, next){
  // var chartsData = req.query.data || [];
  var chartsData = req.body.data || [];
  var comp_logo = req.user.user_detail.logo_head;
  var comp_name = req.user.user_detail.usr_comp_name;
  var tmpfolder = './modules/dashboards/server/temp/';
  var pptx = officegen('pptx');
  var slide;
  var filename = req.user.user_detail.client_id + '_' + req.user.user_detail.usr_uid.toString() + '_' + Date.now().toString() + '.pptx';

  // chartsData = new Buffer(chartsData, 'base64').toString('utf8');
  // chartsData = JSON.parse(chartsData);

  /*Downloading image*/
  var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
      // console.log('content-type:', res.headers['content-type']);
      // console.log('content-length:', res.headers['content-length']);

      request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
  };
  /*Downloading image*/

  res.writeHead ( 200, {
    "x-filename": filename,
    "Content-Type": "application/vnd.openxmls-officedocument.presentationml.presentation;",
    'Content-disposition': 'attachment; filename='+ filename
  });

  // download('http://app.digivla.id/admin/asset/images/' + comp_logo, path.resolve(tmpfolder + comp_logo), function(){
    // console.log('image downloaded');
    // adding comp logo
    // slide = pptx.makeNewSlide();
    // slide.addText(comp_name);
    // slide.addImage('http://app.digivla.id/admin/asset/images/' + comp_logo);

    async.each(chartsData, function(chartInfo, callback){
      slide = pptx.makeNewSlide();
      slide.name = 'OfficeChart slide';
      slide.back = 'ffffff';
      slide.addChart(chartInfo, callback, callback);
    }, function(err){
      pptx.on ( 'finalize', function ( written ) {
        console.log ( 'Finish to create a PowerPoint file.\nTotal bytes created: ' +  written + '\n' );
      });

      pptx.on ( 'error', function ( err ) {
        console.log ( err );
        return res.status(400);
      });

      return pptx.generate(res);
    });

  // });
};

exports.downloadPPT = function(req, res, next){
  var chartsData = req.query.data || [];
  var comp_logo = req.user.user_detail.logo_head;
  var comp_name = req.user.user_detail.usr_comp_name;
  var tmpfolder = './modules/dashboards/server/temp/';
  var pptx = officegen('pptx');
  var slide;
  var filename = req.user.user_detail.client_id + '_' + req.user.user_detail.usr_uid.toString() + '_' + Date.now().toString() + '.pptx';

  chartsData = new Buffer(chartsData, 'base64').toString('utf8');
  chartsData = JSON.parse(chartsData);
console.log(JSON.stringify(chartsData));
  /*Downloading image*/
  var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
      // console.log('content-type:', res.headers['content-type']);
      // console.log('content-length:', res.headers['content-length']);

      request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
  };
  /*Downloading image*/

  res.writeHead ( 200, {
    "x-filename": filename,
    "Content-Type": "application/vnd.openxmls-officedocument.presentationml.presentation",
    'Content-disposition': 'attachment; filename='+ filename
  });

  // download('http://app.digivla.id/admin/asset/images/' + comp_logo, path.resolve(tmpfolder + comp_logo), function(){
    // console.log('image downloaded');
    // adding comp logo
    // slide = pptx.makeNewSlide();
    // slide.addText(comp_name);
    // slide.addImage('http://app.digivla.id/admin/asset/images/' + comp_logo);

    async.each(chartsData, function(chartInfo, callback){
      slide = pptx.makeNewSlide();
      slide.name = 'OfficeChart slide';
      slide.back = 'ffffff';
      slide.addChart(chartInfo, callback, callback);
    }, function(err){
      pptx.on ( 'finalize', function ( written ) {
        console.log ( 'Finish to create a PowerPoint file.\nTotal bytes created: ' +  written + '\n' );
      });

      pptx.on ( 'error', function ( err ) {
        console.log ( err );
        return res.status(400);
      });

      return pptx.generate(res);
    });

  // });
};

exports.downloadExcel = function(req, res, next){
  var client_id = req.user.user_detail.client_id,
      token = req.user.token,
      group_category = req.query.gc,
      media_set = req.query.ms,
      category_id = req.query.sc,
      media_id = req.query.mi.toString(),
      time_frame = parseInt(req.query.tf),
      date_from = req.query.df,
      date_to = req.query.dt,
      uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

  if(time_frame == 0 && (date_from == '' || date_from == '')){
    res.status(400).send({});
    return;
  }

  async.waterfall([
    function(cb){
      getMediaId(client_id, media_set, token, function(medArr){
        getCategorySet(client_id, group_category, true, token, function(catArr){
          if(media_id !== '00'){
            medArr = {
              success: true,
              data: [media_id]
            };
          }

          if(category_id !== 'All Category'){
            catArr = {
              success: true,
              data: {
                processed: [{
                  match: {
                    category_id: {
                      query: category_id,
                      operator: 'and'
                    }
                  }
                }],
                raw: [category_id]
              }
            };
          }

          cb(null, medArr, catArr);
        });
      });
    },
    function(medArr, catArr, cb){
      var items = {
        "statuse": ["A"],
        "token": token
      }

      nodeService({first: 'tb_media', second: ''}, items, function(resps){
        var results = resps.data.data;
        var mediaJson = {};
        mediaJson.media_name = {};
        mediaJson.media_type_id = {};

        var mediaTypeList = {
          '1': 'National Daily Newspaper',
          '2': 'Weekly Magazine',
          '3': 'Monthly Magazines',
          '4': 'Online Media',
          '5': 'Newswire',
          '6': 'Journals',
          '7': 'Tabloids',
          '8': 'Others',
          '9': 'Biweekly Magazines',
          '10': 'Promotions',
          '11': 'Local Daily News Paper',
          '12': 'TV',
          '13': 'Radio',
          '14': 'Test Media'
        };

        for(var i=0;i<results.length;i++){
          mediaJson.media_name[results[i].media_id.toString()] = results[i].media_name;
          mediaJson.media_type_id[results[i].media_id.toString()] = mediaTypeList[results[i].media_type_id.toString()];
        }

        cb(null, mediaJson, medArr, catArr);
      });
    },
    function(medList, medArr, catArr, cb){
      var dateProcessing = dateFormatting(time_frame, date_from, date_to);

      var items = {
        category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
        media: (medArr.data.length > 0) ? medArr.data : [],
        date_from: dateProcessing.date_from,
        date_to: dateProcessing.date_to,
        interval: dateProcessing.interval,
        size: 1000
      };

      var params = jsonTemplates.getProcess4WithSize(items);

      elasticService(uriSegment, params, function(resp){

        if(resp.data.hits.total > 0){
          var result = resp.data.hits.hits;
          var rest = [];

          for(var i=0;i<result.length;i++){
            rest.push(result[i]._source);
          }

          cb(null, medList, medArr, catArr, rest);
        } else{
          cb('Cannot get the data', null);
        }

      });
    },
    function(medList, medArr, catArr, dataClient, cb){
      var items = {
        articles: getArticleIds(dataClient),
        size: 1000
      },
      uriSegment = {
        index: 'digivlaschema',
        type: 'content',
        method: 'POST'
      };

      var params = jsonTemplates.getArticleDetail(items);

      elasticService(uriSegment, params, function(resp){

        if(resp.data.hits.total > 0){
          var result = resp.data.hits.hits;
          var rest = {};

          // make json from digivlaschema
          for(var i=0;i<result.length;i++){
            rest[result[i]._source.article_id] = result[i]._source;
          }

          // enrich the data_client search result
          for(var i=0;i<dataClient.length;i++){
            dataClient[i].file_pdf = rest[dataClient[i].article_id.toString()].file_pdf;
            dataClient[i].page = rest[dataClient[i].article_id.toString()].page;
            dataClient[i].column = rest[dataClient[i].article_id.toString()].columne;
            dataClient[i].color = rest[dataClient[i].article_id.toString()].is_colour;
            dataClient[i].title = rest[dataClient[i].article_id.toString()].title;
            dataClient[i].journalist = rest[dataClient[i].article_id.toString()].journalist;
            dataClient[i].media_id = rest[dataClient[i].article_id.toString()].media_id;

            dataClient[i].media_name = medList.media_name[dataClient[i].media_id.toString()];
            dataClient[i].media_type = medList.media_type_id[dataClient[i].media_id.toString()];
          }

          cb(null, dataClient);

        } else{
          cb('Cannot get the data', null);
        }

      });
    }

  ], function(err, result){
    if(err){
      console.log(err);
      return res.status(400);
    }

    for(var i=0;i<result.length;i++) {
      if(result[i].tone === '-1'){
        result[i].tone = 'Negative';
      } else if(result[i].tone === '1'){
        result[i].tone = 'Positive';
      } else{
        result[i].tone = 'Neutral';
      }

      if(result[i].file_pdf.indexOf('.pdf') !== -1){
        var tanggal = result[i].file_pdf.substring(8, 10),
          bulan = result[i].file_pdf.substring(5, 7),
          tahun = result[i].file_pdf.substring(0, 4);
        result[i].file_pdf = 'http://pdf.antara-insight.id/pdf_images/' + tahun + '/' + bulan + '/' + tanggal + '/' + result[i].file_pdf;
      }

      var newsvalue_fc = parseInt(result[i].advalue_fc);
      var newsvalue_bw = parseInt(result[i].advalue_bw);
      var advalue_fc = Math.ceil(newsvalue_fc * 0.34);
      var advalue_bw = Math.ceil(newsvalue_bw * 0.34);

      result[i].prvalue = accounting.formatMoney(newsvalue_fc, "Rp ", 2, ".", ",");
      result[i].advalue = accounting.formatMoney(advalue_fc, "Rp ", 2, ".", ",");
      result[i].color = (result[i].color === '0') ? 'BW' : 'FC';
    }

    // console.log(JSON.stringify(result));
    var filename = req.user.user_detail.client_id + '_' + req.user.user_detail.usr_uid.toString() + '_' + Date.now().toString() + '.xlsx';
    // res.writeHead ( 200, {
    //   "x-filename": filename,
    //   "Content-Type": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    //   'Content-disposition': 'attachment; filename='+ filename
    // });

    return res.status(200).send(result);

    // var xlsx = officegen('xlsx');

    // var sheet = xlsx.makeNewSheet ();
    // sheet.name = 'Antara Insight - Media Monitoring';

    // sheet.data[0] = [];
    // sheet.data[0].push('No');
    // sheet.data[0].push('Date');
    // sheet.data[0].push('Category');
    // sheet.data[0].push('Title');
    // sheet.data[0].push('Media Type');
    // sheet.data[0].push('Media Name');
    // sheet.data[0].push('Page / Link');
    // sheet.data[0].push('Column');
    // sheet.data[0].push('Color');
    // sheet.data[0].push('Tone');
    // sheet.data[0].push('Journalist');
    // sheet.data[0].push('News Value BW');
    // sheet.data[0].push('News Value FC');
    // sheet.data[0].push('AdValue BW');
    // sheet.data[0].push('AdValue FC');

    // for(var i=0;i<result.length;i++){
    //   if(result[i].tone === '-1'){
    //     result[i].tone = 'Negative';
    //   } else if(result[i].tone === '1'){
    //     result[i].tone = 'Positive';
    //   } else{
    //     result[i].tone = 'Neutral';
    //   }

    //   if(result[i].page.indexOf('.pdf') !== -1){
    //     var tanggal = result[i].page.substring(8, 10),
    //       bulan = result[i].page.substring(5, 7),
    //       tahun = result[i].page.substring(0, 4);
    //     result[i].page = 'http://app.digivla.id/pdf/pdf_image/' + tahun + '/' + bulan + '/' + tanggal + '/' + result[i].page;
    //   }

    //   var newsvalue_fc = parseInt(result[i].advalue_fc);
    //   var newsvalue_bw = parseInt(result[i].advalue_bw);
    //   var advalue_fc = Math.ceil(newsvalue_fc * 0.34);
    //   var advalue_bw = Math.ceil(newsvalue_bw * 0.34);

    //   var x = i+1;
    //   sheet.data[x] = [];
    //   sheet.data[x].push(x);
    //   sheet.data[x].push(result[i].datee);
    //   sheet.data[x].push(result[i].sub_client);
    //   sheet.data[x].push(result[i].title);
    //   sheet.data[x].push(result[i].media_type);
    //   sheet.data[x].push(result[i].media_name);
    //   // sheet.data[x].push('=HYPERLINK("'+ result[i].page +'";"Source")');
    //   sheet.data[x].push(result[i].page);
    //   sheet.data[x].push(result[i].column);
    //   sheet.data[x].push( (result[i].color === '0') ? 'BW' : 'FC' );
    //   sheet.data[x].push(result[i].tone);
    //   sheet.data[x].push(result[i].journalist);
    //   sheet.data[x].push(accounting.formatMoney(newsvalue_bw, "Rp ", 2, ".", ","));
    //   sheet.data[x].push(accounting.formatMoney(newsvalue_fc, "Rp ", 2, ".", ","));
    //   sheet.data[x].push(accounting.formatMoney(advalue_bw, "Rp ", 2, ".", ","));
    //   sheet.data[x].push(accounting.formatMoney(advalue_fc, "Rp ", 2, ".", ","));
    // }

    // var filename = req.user.user_detail.client_id + '_' + req.user.user_detail.usr_uid.toString() + '_' + Date.now().toString() + '.xlsx';
    // res.writeHead ( 200, {
    //   "x-filename": filename,
    //   "Content-Type": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    //   'Content-disposition': 'attachment; filename='+ filename
    // });

    // xlsx.on('finalize', function(written){
    //   console.log('Finish to create Excel file.\nTotal bytes created: ' + written + '\n');
    // });

    // xlsx.on('error', function(err){
    //   console.log(err);
    // });

    // // return res.status(200).send(result);
    // return xlsx.generate(res);
  });

};




/*******************/
function dateFormatting(timeFrame, df, dt){
  var currentDate = new Date(),
    dateFlag = new Date(),
    interval = 'day',
    date_from = df,
    date_to = dt;

  var dateFormatter = function(tgl){
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  }

    if(timeFrame !== 0){
      switch(timeFrame){
          case 1:
            dateFlag.setDate(dateFlag.getDate() - 1);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 7:
            dateFlag.setDate(dateFlag.getDate() - 6);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 30:
            dateFlag.setDate(dateFlag.getDate() - 29);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 365:
            dateFlag.setDate(dateFlag.getDate() - 364);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            interval = 'month';
            break;
      }
  } else{
    var oneDay = 24*60*60*1000;
    var firstDate = new Date(date_from);
    var secondDate = new Date(date_to);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

    if(diffDays > 90){
      interval = 'month';
    }
  }

  return {
    date_from: date_from,
    date_to: date_to,
    interval: interval
  };
}

function getMedia(cb){
  var reqJson = {
      statuse: 'A'
    },
    uriSegment = {
      first: 'media',
      second: 'datalist'
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var arrayForChart = {};
    var result = resp.data.data;

    for(var i=0;i<result.length;i++){
      arrayForChart[result[i].media_id] = result[i].media_name;
    }

    return cb({
      success: true,
      data: arrayForChart
    });


  });
};

function getMediaId(client_id, medset, token, cb) {
  var reqJson = {
      client_id: client_id,
      user_media_type_id: parseInt(medset),
      token: token
    },
    uriSegment = {
      first: 'tb_media_set_choosen',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var result = [];
    var rawData = resp.data.data;

    for (var i = 0; i < rawData.length; i++) {
      result.push(rawData[i].user_media_id);
    }

    return cb({
      success: true,
      data: result,
      raw: rawData
    });
  });
}

function getMediaDate(tf, df, dt, field){
  var range = {};
  var current = new Date();
  var date_from_ = new Date();

  if(tf == 0 || tf == '0'){
    range[field] = {
      gte: df,
      lte: dt,
      format: "yyyy-MM-dd||yyyy-MM-dd"
    };
  } else{
    var dateFormatter = function(tgl){
      return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
    };

    switch(tf){
      case 1:
            date_from_.setDate(date_from_.getDate() - 1);
            date_from_ = dateFormatter(date_from_);
            break;
          case 7:
            date_from_.setDate(date_from_.getDate() - 7);
            date_from_ = dateFormatter(date_from_);
            break;
          case 30:
            date_from_.setDate(date_from_.getDate() - 30);
            date_from_ = dateFormatter(date_from_);
            break;
          case 365:
            date_from_.setDate(date_from_.getDate() - 365);
            date_from_ = dateFormatter(date_from_);
            break;
          default:
            date_from_.setDate(date_from_.getDate() - 7);
            date_from_ = dateFormatter(date_from_);
        break;
    }

    range[field] = {
      gte: date_from_,
      lte: dateFormatter(current),
      format: "yyyy-MM-dd||yyyy-MM-dd"
    };
  }

  return range;
}

// function getCategorySet(client_id, catset, raw, cb){
//   var uriSegment = {
//         index: 'categoryset',
//         type: 'datalist',
//         method: 'POST'
//       },
//       params = jsonTemplates.getCategorySet(client_id, catset);

//   elasticService(uriSegment, params, function(resp){
//     if(!resp.success){
//       return cb({
//         success: false,
//         data: []
//       });
//     }

//     var resArr = JSON.parse(JSON.stringify(resp));
//     var arrayForChart = [];

//     if(resArr.data.hits.hits.length > 0){
//       var result = resArr.data.hits.hits;

//       for(var i=0;i<result.length;i++){
//         var _source;
//         if(result[i]["_source"]){
//           _source = result[i]["_source"];
//         }

//         var item = {
//         "match": {
//         "category_id": {
//                   "query": _source.category_id,
//                   "operator": "and"
//               }
//             }
//         }

//         arrayForChart.push(item);
//       }

//       if(!raw){
//         return cb({
//           success: true,
//           data: arrayForChart
//         });
//       } else{
//         var rawArr = [];

//         for(var i=0;i<result.length;i++){
//           rawArr.push(result[i]._source.category_id);
//         }

//         return cb({
//           success: true,
//           data: {
//             processed: arrayForChart,
//             raw: rawArr
//           }
//         });
//       }

//     } else{
//       return cb({
//         success: false,
//         data: []
//       });
//     }
//   });
// };
function getCategorySet(client_id, catset, raw, token, cb) {
  var reqJson = {
      client_id: client_id,
      category_set: parseInt(catset),
      token: token
    },
    uriSegment = {
      first: 'tb_category_set',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var rawData = resp.data.data;
    var arrayForChart = [];


    for(var i=0;i<rawData.length;i++){
      var item = {
        "match": {
          "category_id": {
            "query": rawData[i].category_id,
            "operator": "and"
          }
        }
      };
      // var item = {
      //   "term": {
      //     "category_id": rawData[i].category_id
      //   }
      // };

      arrayForChart.push(item);
    }

    if(!raw){
      return cb({
        success: true,
        data: arrayForChart
      });
    } else{
      var rawArr = [];

      for (var i = 0; i < rawData.length; i++) {
        rawArr.push(rawData[i].category_id);
      }

      return cb({
        success: true,
        data: {
          processed: arrayForChart,
          raw: rawArr
        }
      });
    }

  });
}

function mergingPivot(juma, jumb){
  var result = [],
    jumlah;

  for(var i=0;i<juma.length;i++){
    var jumaTgl = juma[i].tgl;
    var jumaCategoryId = juma[i].category_id;
    var jumaJumlah = juma[i].juma;

    for(var j=0;j<jumb.length;j++){
      var jumbTgl = jumb[j].tgl;
      var jumbCategoryId = jumb[j].category_id;
      var jumbJumlah = jumb[j].jumb;

      if(jumaTgl === jumbTgl && jumaCategoryId === jumbCategoryId){
        jumlah = (jumaJumlah === 0) ? 0 : (jumaJumlah / jumbJumlah);

        if(jumaJumlah >= 10){
          if(jumlah >= 0 && jumlah <= 0.5){
            jumlah += 2;
          } else if(jumlah > 0.5 && jumlah <= 0.75){
            jumlah += 3;
          } else{
            jumlah += 4;
          }
        } else{
          jumlah += 1;
        }

        if(jumlah > 4){
          jumlah = 4;
        }

        result.push({
          jumlah: jumlah.toFixed(4),
          category_id: jumaCategoryId,
          tgl: jumaTgl
        });
      }
    }
  }

  return result;
}

function mappingPivotTone(index_pivot, type_pivot, pivot_tone, cb){
  var items = {
    index: index_pivot,
    type: type_pivot
  };
  var params = jsonTemplates.mappingPivotTones(items),
    uriSegment = {
        index: index_pivot,
        type: false,
        method: 'PUT'
    };
// console.log('mappingPivotTones');
// console.log(JSON.stringify(params));
  // check pivot indexing status
  elasticService({index: index_pivot, type: false, method: 'GET'}, {}, function(resp){
    var firstResp = JSON.parse(JSON.stringify(resp));
    if(firstResp.data.hasOwnProperty('status') || (firstResp.data.status == 404 || firstResp.data.status == '404')){

      elasticService(uriSegment, params, function(resp2){
        if(resp2.success){

          /***** BULKING *****/

          // Untuk membatasi hanya kirim 500
          /*var totalBulk = 500;
          var totalindexing = pivot_tone.length;
          var counttt = Math.ceil(totalindexing / totalbulk);
          var lastkey = 0;
          var bulkArr = [];

          for(var i=0;i<=counttt;i++){
            if(pivot_tone.length > 0){
              for(var j=lastkey;j<totalBulk;++j){
                bulkArr.push({
                  index: {
                    _index: index_pivot,
                    _type: type_pivot
                  }
                });
                bulkArr.push(pivot_tone[j]);
                lastkey = j;
                if(j>=(totalindexing-1)){ break; }
              }
            }

            client.bulk({
              body: bulkArr
            }, function(err, resp){
              cb(true);
            });
          }*/

          // Kirim semua
          var bulkArr = [];
          for(var i=0;i<pivot_tone.length;i++){
            bulkArr.push({
              index: {
                _index: index_pivot,
                _type: type_pivot
              }
            });
            bulkArr.push({
              jumlah: parseFloat(pivot_tone[i].jumlah),
              category_id: pivot_tone[i].category_id,
              tgl: pivot_tone[i].tgl
            });
          }

          client.bulk({
            body: bulkArr
          }, function(err, resp){
            return cb(true);
          });

          /***** BULKING *****/

        } else{
          return cb(false);
        }
      });

    }
  });

  return params;
}
/*******************/


function getArticleIds(data){
  if(data.length > 0){
    var arrayForArt = [];
    for(var i=0; i < data.length; i++){
      arrayForArt.push(data[i].article_id); 
    }

    return arrayForArt;
  } else{
    return [];
  }
}

exports.oldEWS = function(req, res){
  var http = require('http');
  var datas = req.body;

  // var querystring = require("querystring");
  // var qs = querystring.stringify(body); console.log(JSON.stringify(qs));
  // var qslength = qs.length;
  // var options = {
  //     hostname: "http://mskapi.antara-insight.id/",
  //     port: 80,
  //     path: "api-new.php",
  //     method: 'POST',
  //     headers:{
  //         'Content-Type': 'application/x-www-form-urlencoded',
  //         'Content-Length': qslength
  //     }
  // };

  // var buffer = "";
  // var req = http.request(options, function(respon) {
  //     res.on('data', function (chunk) {
  //        buffer+=chunk;
  //     });
  //     res.on('end', function() {
  //         console.log(buffer);
  //         return res.status(200).send(buffer);
  //     });
  // });

  // req.write(qs);
  // req.end();

  // request.post({url: "http://mskapi.antara-insight.id/api-new.php", form: datas}, function(err, httpResponse, body){
  request.post({url: "http://localhost/mskapi/api-new.php", form: datas}, function(err, httpResponse, body){
    console.log("httpResponse: " + JSON.stringify(httpResponse));

    var resultVar = JSON.parse(JSON.stringify(body));
    console.log(JSON.stringify(resultVar));

    return res.status(200).send(body);
  });
};