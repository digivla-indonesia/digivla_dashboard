'use strict';

angular.module('news').controller('NewsController', ['$scope', '$http', '$rootScope', '$state', '$timeout', '$window', '$sce', '$filter', '$q', 'NgTableParams', 'Authentication',
  function($scope, $http, $rootScope, $state, $timeout, $window, $sce, $filter, $q, NgTableParams, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    // Pagination
    $scope.currentPage = 1;
    $scope.maxSize = 5;

    $scope.clear = function() {
      $scope.dateFrom = null;
      $scope.dateTo = null;
    };

    $timeout(function() {
      $scope.$on('reloadChart', function() {
        $scope.tableData = [];
        $scope.loadingData = true;
        $scope.getTableDataError = false;
        $scope.selectedAll = false;
        $scope.selection = [];
        getTableData();
      });
    }, 0);

    // Call getTableData()
    $scope.initData = function(){
      $timeout(function(){
        $scope.selectedAll = false;
        $scope.selection = [];
        $scope.tableData = [];
        $scope.loadingData = true;
        getTableData();
      }, 1500);
    };

    var td = [];
    var deferred = $q.defer();
    var filterBucket = false;
    $scope.tableData;
    $scope.totalArticles = 0;

    function getTableData(val) {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var datas = {
        time_frame: $rootScope.selectedPeriodOptions.value,
        date_from: from,
        date_to: to,
        category: $rootScope.selectedGroupCategory.category_set,
        category_id: $rootScope.selectedSubCategory.category_id,
        media: $rootScope.selectedGroupMedia.media_set_id,
        media_id: $rootScope.selectedMediaSelection.user_media_id,
        offset: val,
        limit: $scope.selectedLimit
      }

      $scope.tableData = new NgTableParams({
        page: 1,
        count: 25,
        sorting: {
          datee: 'desc'     
        }
      }, {
        total: td.length,
        getData: function(params){
          var tdfrom = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
            tdto = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

          if (tdfrom !== '' && tdto !== '') {
            tdfrom = tdfrom.getFullYear() + '-' + ('0' + (tdfrom.getMonth() + 1)).slice(-2) + '-' + ('0' + tdfrom.getDate()).slice(-2);
            tdto = tdto.getFullYear() + '-' + ('0' + (tdto.getMonth() + 1)).slice(-2) + '-' + ('0' + tdto.getDate()).slice(-2);
          }
          
          if(td.length > 0 && $rootScope.selectedPeriodOptions.value == filterBucket.time_frame && tdfrom == filterBucket.date_from && tdto == filterBucket.date_to && $rootScope.selectedGroupCategory.category_set == filterBucket.category && $rootScope.selectedSubCategory.category_id == filterBucket.category_id && $rootScope.selectedGroupMedia.media_set_id == filterBucket.media && $rootScope.selectedMediaSelection.user_media_id == filterBucket.media_id ){
            td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
            var searchedData = searchData();
            params.total(searchedData.length);
            return searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
          } else{
            filterBucket = datas;
            return $http.post('/api/news/gettablelist', datas).then(function(response){
              var results = response.data;
              td = results.data;
              td = params.sorting() ? $filter('orderBy')(td, params.orderBy()) : td;
              var searchedData = searchData();
              $scope.totalArticles = searchedData.length;
              params.total(searchedData.length);
              return searchedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            });
          }
        },
        $scope: { $data: {} }
      });

      return;
    }

    $scope.$watch("searchTable", function () {
      if($scope.tableData)
        $scope.tableData.reload();
    });

    function searchData(){
      if($scope.searchTable)
         return $filter('filter')(td,$scope.searchTable);
      return td;
    }

    $scope.pageChanged = function() {
      $scope.offset = ($scope.currentPage - 1) * $scope.selectedLimit;
      $scope.loadingData = true;
      $scope.tableData = [];
      getTableData($scope.offset);
    };

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.getDetailArticle = function(val, mn, categories) {
      var url = '/api/dashboards/getarticledetail?_id=' + val;
      $scope.mediaHTML = "";
      $http.get(url).success(function(response) {
        var results = response.data.data;

        var keywords = results.keywords;
        var selectedKeyword;
        // var res = results.content.split(" ");
        // console.log(results);
        // for(var i=0;i<res.length;i++){
        //   for(var j=0;j<keywords.length;j++){
        //     // selectedKeyword = keywords[j].split(" ");
        //     selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
        //     for(var k=0;k<selectedKeyword.length;k++){
        //       if(res[i].toLowerCase() == selectedKeyword[k].toLowerCase()){
        //         res[i] = "<span style='color:#f89406; font-weight:bolder;'>" + res[i] + "</span>";
        //       } else{
        //         var a = res[i].toLowerCase().indexOf(selectedKeyword[k].toLowerCase());
        //         if(a != -1){
        //             var separatedString = "<span style='color:#f89406; font-weight:bolder;'>" + res[i].substr(a, selectedKeyword[k].length) + "</span>";
        //             var b = res[i].replace(res[i].substr(a, selectedKeyword[k].length), separatedString);
        //             res[i] = b;
        //         }
        //       }

        //     }
        //   }
        // }
        // results.content = res.join(" ");
        var temp;
        var x, y;
        for(var j=0;j<keywords.length;j++){
          selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
          for(var k=0;k<selectedKeyword.length;k++){
            x = selectedKeyword[k];
            x = x.replace(/\"/g, "");
            x = x.replace(/\'/g, "");

            temp = results.content.replace(new RegExp('('+x+')', "ig"), '<span style="color:#f89406; font-weight:bolder;">$1</span>');
            results.content = temp;
          }
        }

        results.content = results.content.replace(/(?:\r\n|\r|\n)/g, '<br />');

        if(results.file_pdf.indexOf('.mp4') !== -1){
          var media_type = (results.file_pdf.indexOf('.mp3') !== -1) ? 'media_radio' : 'media_tv';
          var file = response.data.data.file_pdf;
          var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);

          $scope.mediaHTML = '<video width="25%" controls>' +
            '<source src="' + 'http://pdf.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="video/mp4">' +
            'Your browser does not support the video tag.' +
          '</video>';

        } else if(results.file_pdf.indexOf('.mp3') !== -1){
          var file = response.data.data.file_pdf;
          var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);

          $scope.mediaHTML = '<audio width="25%" controls>' +
            '<source src="' + 'http://pdf.antara-insight.id/media_radio/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="audio/mpeg">' +
            'Your browser does not support the audio tag.' +
          '</audio>';
        } else{
          results.file_pdf = results.file_pdf;
        }

        $scope.articleDetail = results;
        $scope.articleDetail.media_name = mn;
        $scope.articleDetail.selectedCategories = categories;

        $.fancybox.open({href: '#dd1'});
      }).error(function(response) {
        $scope.articleDetail = null;
      });
    };

    $scope.openDeleteArticlePopup = function(data){
      $scope.articleProperties = data;
      $.fancybox.open({href: '#warning-box'});
    };

    // generate pdf
    $scope.getPDF = function(type, ai){
      var items = {
        type: type,
        article_id: ai,
        article_detail: $scope.articleDetail
      };
      // return console.log(JSON.stringify(items));
      // $http.post('/api/news/generatepdf').success(function(data, status, headers, config){
      $http.post('/api/news/generatepdf', items).success(function(resp){
        var fn = 'pdf_' + Date.now().toString() + '.pdf';
        pdfMake.createPdf(resp).download(fn);
      }).error(function(resp){
        alert('failed: ' + JSON.stringify(resp));
      });
    };

    $scope.getScan = function(type, file){
      var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);
      var fileUrl = 'http://pdf.antara-insight.id/' + type + '/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;

      window.open(fileUrl, '_blank');
    };

    $scope.getWord = function(){
      var xx = btoa(JSON.stringify($scope.articleDetail));

      var downloadWordURL = '/api/news/generatedoc?data=' + xx;
      window.open(downloadWordURL, '_blank');
    };

    /**
     * List articles for sent by email
     */
    var checkedArticle = [];

    // $scope.selection = [];
    // $scope.toggleSelection = function toggleSelection(articleId) {
    //   var idx = $scope.selection.indexOf(articleId);
      
    //   // is currently selected
    //   if (idx > -1) {
    //     $scope.selection.splice(idx, 1);
    //   }
      
    //   // is newly selected
    //   else {
    //     $scope.selection.push(articleId);
    //   }

    //   if($scope.selection.length > 0){
    //     $scope.showButtonEmail = true;
    //   } else{
    //     $scope.showButtonEmail = false;
    //   }
    // };
    // checkbox function
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(article) {
      var found = false;
      var index = null;
      
      if($scope.selection.length > 0){
        for(var i=0;i<$scope.selection.length;i++) {
          if($scope.selection[i].article_id.toString() === article.article_id.toString() && ($scope.selection[i].category_id.length == article.categories.length && $scope.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && ($scope.selection[i]._id.length == article._id.length && $scope.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
              index = i;
              break;
          }
        }
      }

      // is currently selected
      if(index !== null){
        $scope.selection.splice(index, 1);
      // is newly selected
      } else{
        $scope.selection.push({
          _id: article._id,
          article_id: article.article_id,
          category_id: article.categories
        });
      }

      if($scope.selection.length > 0){
        $scope.showButtonEmail = true;
      } else{
        $scope.showButtonEmail = false;
      }
    };

    $scope.checkSelectionExists = function(article){
      var found = false;
      for(var i=0;i<$scope.selection.length;i++) {
        // if($scope.selection[i].article_id.toString() === article.article_id.toString() && $scope.selection[i].category_id === article.category_id){
        //   found = true;
        //   break;
        // }
        if($scope.selection[i].article_id.toString() === article.article_id.toString() && ($scope.selection[i].category_id.length == article.categories.length && $scope.selection[i].category_id.every(function(u, i) {return u === article.categories[i]})) && ($scope.selection[i]._id.length == article._id.length && $scope.selection[i]._id.every(function(u, i) {return u === article._id[i]})) ){
          found = true;
          break;
        }
      }

      return found;
    };

    function checkCheckedAll(){
      var result = false;
      var counterFlag = 0;

      if($scope.selection.length > 0){
        angular.forEach($scope.tableData, function (value, key) {
          for(var i=0;i<$scope.selection.length;i++){
            if(value.article_id.toString() === $scope.selection[i].article_id.toString()){
              // $scope.selection.splice(i, 1);
              ++counterFlag;
              break;
            }
          }
        });
      }

      if(counterFlag == $scope.selectedLimit){
        result = true;
      }

      return result;
    };

    $scope.checkAll = function(){
      if ($scope.selectedAll) {
        $scope.selectedAll = true;
        $scope.showButtonEmail = true;
        // $scope.selection = [];
        angular.forEach(td, function (value, key) {
          if(!$scope.checkSelectionExists(value)){
            $scope.selection.push({
              _id: value._id,
              article_id: value.article_id,
              category_id: value.categories
            });
          }
        });
      } else {
        $scope.selectedAll = false;
        $scope.showButtonEmail = false;
        // for(var j=0;j<$scope.tableData.length;j++){
        //   for(var i=0;i<$scope.selection.length;i++){
        //     if($scope.tableData[j].article_id.toString() === $scope.selection[i].article_id.toString()){
        //       $scope.selection.splice(i, 1);
        //       break;
        //     }
        //   }
        // }
        $scope.selection = [];
      }
    };

    $scope.deleteArticles = function(){
      $.fancybox.open({href: '#dialog-form-delete-many'});
    };

    $scope.deleteArticle = function(article){
      $scope.selection = [];
      $scope.selection = [{
        _id: article._id,
        article_id: parseInt(article.article_id),
        category_id: article.categories
      }];
      $.fancybox.open({href: '#dialog-form-delete-single'});
    };    

    $scope.doDeleteArticles = function(){
      // hapus artikel disini
      // return alert(JSON.stringify($scope.selection));
      if($scope.selection.length > 0){
        $http.post('/api/news/deletearticle', {articles: $scope.selection}).success(function(data){
          $scope.selectedIndex = null;
          $scope.selection = [];
          $scope.selectedAll = false;
          $scope.showButtonEmail = false;
          td = [];
          // $scope.currentPage = 1;
          $scope.initData();
          $.fancybox.close();
        }).error(function(data){
          alert(data);
          $scope.selection = [];
          $scope.selectedAll = false;
          $scope.showButtonEmail = false;
          $.fancybox.close();
        });
      }
    };

    $scope.saveActivity = function(article, type){
      var items = [{
        article_id: article.article_id,
        category_id: article.category_id,
        activity: type
      }];

      $http.post('/api/news/saveactivity', items).success(function(response){
        return;
      });
    };

    $scope.emailAll = function(){
      var items = {
        article_id: $scope.selection,
        email: $scope.destinationEmail,
        media_set: $rootScope.selectedGroupMedia.media_set_id
      };

      $http.post('/api/news/sendemail', items).success(function(response){
        alert('success');
        // $scope.selection = [];
        $.fancybox.close();
      }).error(function(response){
        alert('failed');
      });
    };

    $scope.originale = function(stat, formate, type){
      var articles = [];
      for(var i=0;i<$scope.selection.length;i++){
        articles.push($scope.selection[i].article_id);
        $scope.selection[i].activity = type;
      }

      $http.post('/api/news/saveactivity', $scope.selection).success(function(response){
        var url = 'http://pdf.antara-insight.id?stat=' + stat;
        url += '&article=' + articles.join();
        url += '&formate=' + formate;
        url += '&logo_head=' + Authentication.user.user_detail.logo_head;
        url += '&client_id=' + Authentication.user.user_detail.client_id;

        $window.open(url, '_blank');
      }).error(function(response){
        alert(response);
      });
    };

    $scope.originaleSingle = function(stat, formate, article, type){
      // var items = {
      //   article: $scope.selection,
      //   stat: stat,
      //   formate: formate,
      //   logo_head: Authentication.user.user_detail.logo_head
      // };

      var items = [{
        article_id: article.article_id,
        category_id: article.category_id,
        activity: type
      }];
      
      $http.post('/api/news/saveactivity', items).success(function(response){
        var url = 'http://pdf.antara-insight.id?stat=' + stat;
        url += '&article=' + article.article_id;
        url += '&formate=' + formate;
        url += '&logo_head=' + Authentication.user.user_detail.logo_head;
        url += '&client_id=' + Authentication.user.user_detail.client_id;

        $window.open(url, '_blank');
      }).error(function(response){
        alert(response);
      });

    };

    // limit
    $scope.showLimit = [10, 20, 50, 100];
    $scope.selectedLimit = $scope.showLimit[1];

    $scope.limitChanging = function(){
      $scope.currentPage = 1;
      $scope.totalItems = null;
      $scope.loadingData = true;
      $scope.offset = 0;
      $scope.tableData = [];
      getTableData();
    };



    /*testing ngTable*/
    // var x = [{name: "Moroni", age: 50},
    //             {name: "Tiancum", age: 43},
    //             {name: "Jacob", age: 27},
    //             {name: "Nephi", age: 29},
    //             {name: "Enos", age: 34},
    //             {name: "Tiancum", age: 43},
    //             {name: "Jacob", age: 27},
    //             {name: "Nephi", age: 29},
    //             {name: "Enos", age: 34},
    //             {name: "Tiancum", age: 43},
    //             {name: "Jacob", age: 27},
    //             {name: "Nephi", age: 29},
    //             {name: "Enos", age: 34},
    //             {name: "Tiancum", age: 43},
    //             {name: "Jacob", age: 27},
    //             {name: "Nephi", age: 29},
    //             {name: "Enos", age: 34}];
    // $scope.adata = x;
    // $scope.a = new NgTableParams({}, {dataset: x});
  }
]);