'use strict';

/**
 * Module dependencies.
 */
var configurationsPolicy = require('../policies/configurations.server.policy'),
  configurations = require('../controllers/configurations.server.controller');

module.exports = function (app) {
  // Configurations collection routes
  // app.route('/api/configurations').all(configurationsPolicy.isAllowed)
  //   .get(configurations.list)
  //   .post(configurations.create);

  // Single configuration routes
  // app.route('/api/configurations/:configurationId').all(configurationsPolicy.isAllowed)
  //   .get(configurations.read)
  //   .put(configurations.update)
  //   .delete(configurations.delete);

  // Finish by binding the configuration middleware
  // app.param('configurationId', configurations.configurationByID);

  app.route('/api/configurations/categorysetdetail')
    .get(configurations.getCategorySetDetail)
    .put(configurations.updateCategoryIdList);

  app.route('/api/configurations/subcategorysetdetail')
    .get(configurations.getSubCategorySetDetail);

  app.route('/api/configurations/mediadetail')
    .get(configurations.getMediaDetail)
    .put(configurations.updateMediaIdList);

  app.route('/api/configurations/tb_category_list')
    .post(configurations.createCategoryList)
    .delete(configurations.deleteCategoryList);

  app.route('/api/configurations/tb_media_set')
    .post(configurations.addMediaSet)
    .delete(configurations.deleteMediaSet);

  app.route('/api/configurations/keyword')
    .post(configurations.addKeyword)
    .delete(configurations.deleteKeyword);

  app.route('/api/configurations/tb_category_set_type')
    .post(configurations.addCategorySet)
    .delete(configurations.deleteCategorySet);
};
