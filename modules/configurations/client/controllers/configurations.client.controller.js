'use strict';

// Configurations controller
var appModule = angular.module('configurations');
appModule.controller('ConfigurationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Configurations',
  function($scope, $stateParams, $location, Authentication, Configurations) {
    $scope.authentication = Authentication;

    // Create new Configuration
    $scope.create = function() {
      // Create new Configuration object
      var configuration = new Configurations({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      configuration.$save(function(response) {
        $location.path('configurations/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Configuration
    $scope.remove = function(configuration) {
      if (configuration) {
        configuration.$remove();

        for (var i in $scope.configurations) {
          if ($scope.configurations[i] === configuration) {
            $scope.configurations.splice(i, 1);
          }
        }
      } else {
        $scope.configuration.$remove(function() {
          $location.path('configurations');
        });
      }
    };

    // Update existing Configuration
    $scope.update = function() {
      var configuration = $scope.configuration;

      configuration.$update(function() {
        $location.path('configurations/' + configuration._id);
      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Configurations
    $scope.find = function() {
      $scope.configurations = Configurations.query();
    };

    // Find existing Configuration
    $scope.findOne = function() {
      $scope.configuration = Configurations.get({
        configurationId: $stateParams.configurationId
      });
    };
  }
]);

appModule.controller('GroupMediaController', ['$scope', '$stateParams', '$rootScope', '$http', '$location', 'Authentication', 'Configurations',
  function($scope, $stateParams, $rootScope, $http, $location, Authentication, Configurations) {
    $scope.authentication = Authentication;

    function initData(){
      $http.get('/api/users/getassets?t=media').success(function (response) {
        //$scope.groupMedias = response.data;
        //$rootScope.selectedGroupMedia = $scope.groupMedias[0];
        $scope.dataSet = response.data;
        $scope.dataSetError = false;
      }).error(function (response) {
        //$scope.groupMedia = [{media_set_id: '', media_set_name: 'Choose Media Group'}];
        $scope.dataSetError = true;
      });
    };

    $scope.initSetData = function(){
      initData();
    };

    function getDetailData(ms){
      $scope.chosenMediaSet = ms.media_set_id;
      $scope.dataDetail = [];
      $scope.showDetail = true;
      $scope.mediaSetName = ms.media_set_name;
      var url = '/api/configurations/mediadetail?media_set=' + ms.media_set_id;
      $http.get(url).success(function (response) {
        // $scope.dataDetail = response.data;
        $scope.dataDetail = response;
        $scope.dataDetailError = false;
      }).error(function (response) {
        $scope.dataDetail = response;
        $scope.dataDetailError = true;
      });
    };

    $scope.getDetail = function(ms){
      // $scope.chosenMediaSet = ms.media_set_id;
      // $scope.dataDetail = [];
      // $scope.showDetail = true;
      // $scope.mediaSetName = ms.media_set_name;
      // var url = '/api/configurations/mediadetail?media_set=' + ms.media_set_id;
      // $http.get(url).success(function (response) {
      //   // $scope.dataDetail = response.data;
      //   $scope.dataDetail = response;
      //   $scope.dataDetailError = false;
      // }).error(function (response) {
      //   $scope.dataDetailError = true;
      // });
      getDetailData(ms);
    };

    // edit
    $scope.openEditMedset = function(val){
      // alert(JSON.stringify(val));
      $scope.medsetValue = val.media_set_name;
      $scope.medsetId = val.media_set_id;
    };

    $scope.editMedsetName = function(){
      //alert('clisetValue: ' + $scope.clisetValue + ', clisetId: ' + $scope.clisetId);
      var url = '/api/users/getassets?t=media';
      var items = {
        _id: $scope.medsetId,
        name: $scope.medsetValue
      };

      $http.put(url, items).success(function(resp){
        initData();
        // $rootScope.$broadcast('reloadFilterOption');
        // when submit is done
        $scope.medsetValue = '';
        $scope.medsetId = null;
        $.fancybox.close();
      }).error(function(resp){
        alert('Failed to update');
      });
    };

    // update category
    $scope.updateMediaSetList = function(){
      var uri = '/api/configurations/mediadetail';
      var uriItem = {
        media_set_id: $scope.chosenMediaSet,
        list: $scope.dataDetail
      };

      // return alert(JSON.stringify($scope.dataDetail));

      $http.put(uri, uriItem).success(function(resp){
        getDetailData({media_set_id: $scope.chosenMediaSet, media_set_name: $scope.mediaSetName});
        // $rootScope.$broadcast('reloadFilterOption');
        // $scope.keyword = '';
        // $scope.dateFrom = '';
        // $scope.dateTo = '';
      }).error(function(resp){
        alert('Error adding keyword');
      });
    };

    // new media set
    $scope.newMediaSet = '';
    $scope.addNewMediaSet = function(){
      if($scope.newMediaSet == ''){
        return alert('You have to fill media set name');
      } else{
        var url = '/api/configurations/tb_media_set';
        var items = {
          media_name: $scope.newMediaSet
        };
        $http.post(url, items).success(function(resp){
          initData();
          // $rootScope.$broadcast('reloadFilterOption');
          $scope.newMediaSet = '';
        }).error(function(resp){
          alert('Failed to add category set');
        });
      }
    };

    // delete media set
    $scope.getDeleteMediaSetId = function(val){
      $scope.deleteMediaSetId = val;
    };
    $scope.deleteMediaSet = function(){
      var url = '/api/configurations/tb_media_set?_id=' + $scope.deleteMediaSetId;
      var items = {
        media_set_id: $scope.deleteMediaSetId
      };
      $http.delete(url, items).success(function(resp){
        initData();
        // $rootScope.$broadcast('reloadFilterOption');
        $scope.deleteMediaSetId = null; 
        $scope.dataDetail = [];
        $scope.showDetail = false;
      }).error(function(resp){
        alert('Failed to delete media');
      });
    };

    $scope.selectAll = function () {
      angular.forEach($scope.dataDetail, function (obj) {
          angular.forEach(obj.media_list, function(obj){
            obj.chosen = true;
          });
      });
    };

    $scope.clearAll = function () {
      angular.forEach($scope.dataDetail, function (obj) {
          angular.forEach(obj.media_list, function(obj){
            obj.chosen = false;
          });
      });
    };
  }
]);

appModule.controller('GroupCategoryController', ['$scope', '$stateParams', '$rootScope', '$http', '$location', 'Authentication', 'Configurations',
  function($scope, $stateParams, $rootScope, $http, $location, Authentication, Configurations) {
    $scope.authentication = Authentication;

    function initData(){
      $http.get('/api/users/getassets?t=groupcategory').success(function (response) {
        $scope.dataSet = response.data;
        $scope.dataSetError = false;
      }).error(function (response) {
        $scope.dataSetError = true;
      });
    };

    function getDetailData(cs){
      $scope.chosenGroupCategory = cs.category_set;
      $scope.dataDetail = [];
      $scope.showDetail = true;
      $scope.clisetName = cs.descriptionz;
      var url = '/api/configurations/categorysetdetail?group_category_id=' + cs.category_set;
      $http.get(url).success(function (response) {
        // $scope.dataDetail = response.data;
        $scope.dataDetail = response;
        $scope.dataDetailError = false;
      }).error(function (response) {
        $scope.dataDetailError = true;
      });
    };

    $scope.initSetData = function(){
      initData();
    };

    $scope.showDetail = false;

    $scope.getDetail = function(cs){
      getDetailData(cs);
    };

    // edit
    $scope.openEditCliset = function(val){
      $scope.clisetValue = val.descriptionz;
      $scope.clisetId = val.category_set;
    };

    $scope.editClisetName = function(){
      //alert('clisetValue: ' + $scope.clisetValue + ', clisetId: ' + $scope.clisetId);
      var url = '/api/users/getassets?t=groupcategory';
      var items = {
        _id: $scope.clisetId,
        name: $scope.clisetValue
      };

      $http.put(url, items).success(function(resp){
        initData();
        // $rootScope.$broadcast('reloadFilterOption');

        // when submit is done
        $scope.clisetValue = '';
        $scope.clisetId = null;
        $.fancybox.close();
      }).error(function(resp){
        alert('Failed to update');
      });
    };

    // update category
    $scope.updateCategoryList = function(){
      var uri = '/api/configurations/categorysetdetail';
      var uriItem = {
        category_set: $scope.chosenGroupCategory,
        list: $scope.dataDetail
      };
      $http.put(uri, uriItem).success(function(resp){
        // alert("success");
        getDetailData({category_set: $scope.chosenGroupCategory, descriptionz: $scope.clisetName});
        // $rootScope.$broadcast('reloadFilterOption');
        // $scope.keyword = '';
        // $scope.dateFrom = '';
        // $scope.dateTo = '';
      }).error(function(resp){
        alert('Error adding keyword');
      });
    };

    // new category set
    $scope.newCategorySet = '';
    $scope.addNewCategorySet = function(){
      if($scope.newCategorySet == ''){
        return alert('You have to fill category set name');
      } else{
        var url = '/api/configurations/tb_category_set_type';
        var items = {
          descriptionz: $scope.newCategorySet
        };
        $http.post(url, items).success(function(resp){
          initData();
          // $rootScope.$broadcast('reloadFilterOption');
          $scope.newCategorySet = '';
        }).error(function(resp){
          alert('Failed to add category set');
        });
      }
    };

    // delete category set
    $scope.getDeleteCategorySetId = function(val){
      $scope.deleteCategorySetId = val;
    };
    $scope.deleteCategorySet = function(){
      var url = '/api/configurations/tb_category_set_type?_id=' + $scope.deleteCategorySetId;
      var items = {
        category_id_str: $scope.deleteCategorySetId
      };
      $http.delete(url, items).success(function(resp){
        initData();
        // $rootScope.$broadcast('reloadFilterOption');
        $scope.deleteCategorySetId = null; 
      }).error(function(resp){
        alert('Failed to delete sub category');
      });
    };
  }
]);

appModule.controller('SubCategoryController', ['$scope', '$stateParams', '$rootScope', '$http', '$location', 'Authentication', 'Configurations',
  function($scope, $stateParams, $rootScope, $http, $location, Authentication, Configurations) {
    $scope.authentication = Authentication;

    function initData(){
      $http.get('/api/users/getsubassets?t=groupcategory').success(function (response) {
        $scope.dataSet = response.data;
        $scope.dataSetError = false;
      }).error(function (response) {
        $scope.dataSetError = true;
      });
    };

    function getSubcliDetail(cs){
      $scope.chosenSubCategory = cs.category_id;
      $scope.dataDetail = [];
      $scope.showDetail = true;
      $scope.clisetName = cs.descriptionz;
      var url = '/api/configurations/subcategorysetdetail?category_id=' + cs.category_id;

      $http.get(url).success(function (response) {
        $scope.dataDetail = response;
        $scope.dataDetailError = false;
      }).error(function (response) {
        $scope.dataDetailError = true;
      });
    };

    $scope.initSetData = function(){
      initData();
    };

    $scope.showDetail = false;

    $scope.getDetail = function(cs){
      getSubcliDetail(cs);
      // $scope.chosenSubCategory = cs.category_id;
      // $scope.dataDetail = [];
      // $scope.showDetail = true;
      // $scope.clisetName = cs.descriptionz;
      // var url = '/api/configurations/subcategorysetdetail?category_id=' + cs.category_id;

      // $http.get(url).success(function (response) {
      //   $scope.dataDetail = response;
      //   $scope.dataDetailError = false;
      // }).error(function (response) {
      //   $scope.dataDetailError = true;
      // });
    };

    // Datepicker
    $scope.clear = function () {
      $scope.dateFrom = null;
      $scope.dateTo = null;
    };

    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened1 = true;
      $scope.opened = false;
    };

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
      $scope.opened1 = false;
    };


    $scope.maxDate = new Date();
    $scope.minDate = '2010-01-01';
    $scope.format = 'yyyy-MM-dd';
    $scope.dateFrom = '';
    $scope.dateTo = '';


    // edit
    $scope.openEditSubCliset = function(val){
      // alert(JSON.stringify(val));
      $scope.subClisetValue = val.category_id;
      $scope.subClisetId = val.category_id;
    };

    $scope.editSubClisetName = function(){
      //alert('clisetValue: ' + $scope.clisetValue + ', clisetId: ' + $scope.clisetId);
      var url = '/api/users/getsubassets?t=groupcategory';
      var items = {
        _id: $scope.subClisetId,
        name: $scope.subClisetValue
      };

      $http.put(url, items).success(function(resp){
        initData();
        // $rootScope.$broadcast('reloadFilterOption');

        // when submit is done
        $scope.subClisetValue = '';
        $scope.subClisetId = null;
        $.fancybox.close();
      }).error(function(resp){
        alert('Failed to update');
      });
    };

    // add new sub category
    $scope.newSubCategoryValue = '';
    $scope.addNewSubCategory = function(){
      if($scope.newSubCategoryValue == ''){
        return alert('You have to fill sub category name');
      } else{
        var url = '/api/configurations/tb_category_list';
        var items = {
          category_id_str: $scope.newSubCategoryValue
        };
        $http.post(url, items).success(function(resp){
          initData();
          // $rootScope.$broadcast('reloadFilterOption');
          $scope.newSubCategoryValue = '';
        }).error(function(resp){
          alert('Failed to add sub category');
        });
      }
    };

    // delete sub category
    $scope.getDeleteSubCategoryId = function(type, val){
      if(type == 0){
        $scope.deleteType = type;
        $scope.deleteSubCategoryId = val;
      } else if(type == 1){
        $scope.deleteType = type;
        $scope.deleteKeywordString = val;
      }
    };

    $scope.deleteSubCategory = function(){
      if($scope.deleteType == 0){
        var url = '/api/configurations/tb_category_list?_id=' + $scope.deleteSubCategoryId;
        var items = {
          category_id_str: $scope.deleteSubCategoryId
        };
        $http.delete(url, items).success(function(resp){
          initData();
          // $rootScope.$broadcast('reloadFilterOption');
          $scope.deleteSubCategoryId = null; 
        }).error(function(resp){
          alert('Failed to delete sub category');
        });
      } else if($scope.deleteType == 1){
        var url = '/api/configurations/keyword?category_id_str=' + $scope.chosenSubCategory + '&keyword=' + encodeURIComponent($scope.deleteKeywordString);
        var items = {
          category_id_str: $scope.chosenSubCategory,
          keyword: $scope.deleteKeywordString
        };

        $http.delete(url, items).success(function(resp){
          // getSubcliDetail($scope.chosenSubCategory);
          getSubcliDetail({category_id: $scope.chosenSubCategory, descriptionz: $scope.clisetName});
          // $rootScope.$broadcast('reloadFilterOption');
          $scope.deleteKeywordString = null; 
        }).error(function(resp){
          alert('Failed to delete sub category');
        });
      }
    };

    // insert category
    $scope.keyword = '';
    
    $scope.addNewKeyword = function(){
      var uri = '/api/configurations/keyword';
      var uriItem = {
        category_id: $scope.chosenSubCategory,
        keyword: $scope.keyword,
        date_from: $scope.dateFrom,
        date_to: $scope.dateTo
      };

      if($scope.dateFrom === '' || $scope.dateTo === ''){
        return alert('Harap isi tanggal');
      }

      $http.post(uri, uriItem).success(function(resp){
        getSubcliDetail({category_id: $scope.chosenSubCategory, descriptionz: $scope.clisetName});
        $scope.keyword = '';
        $scope.dateFrom = '';
        $scope.dateTo = '';
      }).error(function(resp){
        alert('Error adding keyword');
      });
    }

    $scope.getDeleteKeyword = function(val){
      $scope.deleteKeywordString = val;
    };

    $scope.deleteKeyword = function(){
      alert($scope.chosenSubCategory);
      alert($scope.deleteKeywordString);
    };

  }
]);
