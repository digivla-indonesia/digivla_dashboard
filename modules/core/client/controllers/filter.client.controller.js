'use strict';

angular.module('core').controller('FilterController', ['$scope', '$rootScope', '$state', '$http', 'Authentication',
  function($scope, $rootScope, $state, $http, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    function getSubCategory(cs){
      $http.get('/api/users/getsubclientcategory?cs=' + cs).success(function(response) {
        var scArr = [{
          'category_set': '00',
          'category_id': 'All Category'
        }];

        for(var i=0;i<response.length;i++){
          scArr.push(response[i]);
        }

        $scope.subCategories = scArr;
        $rootScope.selectedSubCategory = $scope.subCategories[0];
      }).error(function(response) {
        $scope.subCategories = [{ category_set: '00', category_id: 'Choose Sub' }];
        $rootScope.selectedSubCategory = $scope.subCategories[0];
      });
    };

    function getSubMedia(ms){
      $http.get('/api/users/getsubclientmedia?ms=' + ms).success(function (response) {
        var msArr = [{
          'user_media_id': '00',
          'media_name': 'All Media'
        }];

        for(var i=0;i<response.length;i++){
          msArr.push(response[i]);
        }

        $scope.mediaSelections = msArr;
        $rootScope.selectedMediaSelection = $scope.mediaSelections[0];
      }).error(function (response) {
        $scope.mediaSelection = [{media_set_id: '', media_set_name: 'Choose Media Group'}];
      });
    }

    function initOption(){
      // Get Group Category
      $http.get('/api/users/getassets?t=groupcategory').success(function(response) {
        $scope.groupCategories = response.data;
        $rootScope.selectedGroupCategory = $scope.groupCategories[0];
        getSubCategory($rootScope.selectedGroupCategory.category_set);
      }).error(function(response) {
        $scope.groupCategories = [{ category_set: '', descriptionz: 'Choose Group Category' }];
      });

      // Get Group Media
      $http.get('/api/users/getassets?t=media').success(function(response) {
        $scope.groupMedias = response.data;
        $rootScope.selectedGroupMedia = $scope.groupMedias[0];
        getSubMedia($rootScope.selectedGroupMedia.media_set_id);
      }).error(function(response) {
        $scope.groupMedia = [{ media_set_id: '', media_set_name: 'Choose Media Group' }];
      });

      // Set Period
      $scope.periodOptions = [
        { name: 'Kemarin', value: 1 },
        { name: 'Minggu Lalu', value: 7 },
        { name: 'Bulan Lalu', value: 30 },
        { name: 'Tahun Lalu', value: 365 },
        { name: 'Pilih', value: 0 },
      ];
      $rootScope.selectedPeriodOptions = $scope.periodOptions[1];

      $rootScope.$broadcast('reloadChart');
    };

    $scope.initFilterOption = initOption();

    $scope.$on('reloadFilterOption', function() {
      initOption();
    });

    $rootScope.hiddenPeriod = true;

    $scope.periodChange = function() {
      switch ($rootScope.selectedPeriodOptions.value) {
        case 0:
          $rootScope.hiddenPeriod = false;
          break;
        default:
          $rootScope.hiddenPeriod = true;
          break;
      }
    };


    $scope.changeCategorySet = function(){
      getSubCategory($rootScope.selectedGroupCategory.category_set);
    };

    $scope.changeMediaSet = function(){
      getSubMedia($rootScope.selectedGroupMedia.media_set_id);
    };

    // Reload Data
    $scope.reloadPage = function() {
      //$state.reload();
      $rootScope.$broadcast('reloadChart');
    };
  }
]);
