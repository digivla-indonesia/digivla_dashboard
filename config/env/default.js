'use strict';

module.exports = {
  app: {
    title: 'Antara Insight - Media Insight',
    description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
    keywords: 'mongodb, express, angularjs, node.js, mongoose, passport',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
  },
  port: process.env.PORT || 3000,
  templateEngine: 'swig',
  sessionSecret: 'MEAN',
  sessionCollection: 'sessions',
  logo: 'modules/core/img/brand/logo.png',
  // favicon: 'modules/core/img/brand/favicon.ico',
  favicon: 'public/asset/favicon.ico',

  apiAddress: 'http://172.16.0.4:8080',
  elasticAddress: 'http://el.antara-insight.id/',
  ipElastic: 'el.antara-insight.id',
  sessionAddress: ['172.16.0.3:11211'],
  botApiPost: 'http://128.199.245.199:9080/',
  botApiGet: 'http://128.199.245.199:9070/',
  digitalOceanAPI: 'http://139.59.245.121:3035'
};
