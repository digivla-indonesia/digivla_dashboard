'use strict';

var newsPolicy = require('../policies/news.server.policy'),
  news = require('../controllers/news.server.controller');

module.exports = function(app) {
  // Routing logic
  app.route('/api/news/gettablelist').all(newsPolicy.isAllowed)
    .get(news.getTableList)
    .post(news.getTableList);

  app.route('/api/news/generatepdf')
  .post(news.generatePDF);

  app.route('/api/news/generatedoc')
    .get(news.generateWord);

  app.route('/api/news/sendemail')
  	.post(news.sendEmail);

  app.route('/api/news/deletearticle')
    .post(news.deleteArticles);

  app.route('/api/news/saveactivity')
    .post(news.saveActivity);
};
