'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Dashboard = mongoose.model('Dashboard'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, dashboard;

/**
 * Dashboard routes tests
 */
describe('Dashboard CRUD tests', function () {
  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'password'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new dashboard
    user.save(function () {
      dashboard = {
        title: 'Dashboard Title',
        content: 'Dashboard Content'
      };

      done();
    });
  });

  it('should be able to save an dashboard if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new dashboard
        agent.post('/api/dashboards')
          .send(dashboard)
          .expect(200)
          .end(function (dashboardSaveErr, dashboardSaveRes) {
            // Handle dashboard save error
            if (dashboardSaveErr) {
              return done(dashboardSaveErr);
            }

            // Get a list of dashboards
            agent.get('/api/dashboards')
              .end(function (dashboardsGetErr, dashboardsGetRes) {
                // Handle dashboard save error
                if (dashboardsGetErr) {
                  return done(dashboardsGetErr);
                }

                // Get dashboards list
                var dashboards = dashboardsGetRes.body;

                // Set assertions
                (dashboards[0].user._id).should.equal(userId);
                (dashboards[0].title).should.match('Dashboard Title');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an dashboard if not logged in', function (done) {
    agent.post('/api/dashboards')
      .send(dashboard)
      .expect(403)
      .end(function (dashboardSaveErr, dashboardSaveRes) {
        // Call the assertion callback
        done(dashboardSaveErr);
      });
  });

  it('should not be able to save an dashboard if no title is provided', function (done) {
    // Invalidate title field
    dashboard.title = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new dashboard
        agent.post('/api/dashboards')
          .send(dashboard)
          .expect(400)
          .end(function (dashboardSaveErr, dashboardSaveRes) {
            // Set message assertion
            (dashboardSaveRes.body.message).should.match('Title cannot be blank');

            // Handle dashboard save error
            done(dashboardSaveErr);
          });
      });
  });

  it('should be able to update an dashboard if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new dashboard
        agent.post('/api/dashboards')
          .send(dashboard)
          .expect(200)
          .end(function (dashboardSaveErr, dashboardSaveRes) {
            // Handle dashboard save error
            if (dashboardSaveErr) {
              return done(dashboardSaveErr);
            }

            // Update dashboard title
            dashboard.title = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing dashboard
            agent.put('/api/dashboards/' + dashboardSaveRes.body._id)
              .send(dashboard)
              .expect(200)
              .end(function (dashboardUpdateErr, dashboardUpdateRes) {
                // Handle dashboard update error
                if (dashboardUpdateErr) {
                  return done(dashboardUpdateErr);
                }

                // Set assertions
                (dashboardUpdateRes.body._id).should.equal(dashboardSaveRes.body._id);
                (dashboardUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of dashboards if not signed in', function (done) {
    // Create new dashboard model instance
    var dashboardObj = new Dashboard(dashboard);

    // Save the dashboard
    dashboardObj.save(function () {
      // Request dashboards
      request(app).get('/api/dashboards')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single dashboard if not signed in', function (done) {
    // Create new dashboard model instance
    var dashboardObj = new Dashboard(dashboard);

    // Save the dashboard
    dashboardObj.save(function () {
      request(app).get('/api/dashboards/' + dashboardObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('title', dashboard.title);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single dashboard with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/dashboards/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Dashboard is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single dashboard which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent dashboard
    request(app).get('/api/dashboards/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No dashboard with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an dashboard if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new dashboard
        agent.post('/api/dashboards')
          .send(dashboard)
          .expect(200)
          .end(function (dashboardSaveErr, dashboardSaveRes) {
            // Handle dashboard save error
            if (dashboardSaveErr) {
              return done(dashboardSaveErr);
            }

            // Delete an existing dashboard
            agent.delete('/api/dashboards/' + dashboardSaveRes.body._id)
              .send(dashboard)
              .expect(200)
              .end(function (dashboardDeleteErr, dashboardDeleteRes) {
                // Handle dashboard error error
                if (dashboardDeleteErr) {
                  return done(dashboardDeleteErr);
                }

                // Set assertions
                (dashboardDeleteRes.body._id).should.equal(dashboardSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an dashboard if not signed in', function (done) {
    // Set dashboard user
    dashboard.user = user;

    // Create new dashboard model instance
    var dashboardObj = new Dashboard(dashboard);

    // Save the dashboard
    dashboardObj.save(function () {
      // Try deleting dashboard
      request(app).delete('/api/dashboards/' + dashboardObj._id)
        .expect(403)
        .end(function (dashboardDeleteErr, dashboardDeleteRes) {
          // Set message assertion
          (dashboardDeleteRes.body.message).should.match('User is not authorized');

          // Handle dashboard error error
          done(dashboardDeleteErr);
        });

    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Dashboard.remove().exec(done);
    });
  });
});
