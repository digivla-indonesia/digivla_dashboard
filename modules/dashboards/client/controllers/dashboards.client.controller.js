'use strict';

// Dashboards controller
var appModule = angular.module('dashboards');

appModule.controller('DashboardsHome1Controller', ['$scope', '$rootScope', '$state', '$stateParams', '$location', '$http', '$timeout', '$window', '$sce', 'Authentication', 'ArticleListData', 'FileSaver', 'Blob',
  function($scope, $rootScope, $state, $stateParams, $location, $http, $timeout, $window, $sce, Authentication, ArticleListData, FileSaver, Blob) {
    $scope.authentication = Authentication;

    $scope.initData = function(){
      $timeout(function(){
        getProcess();
        getProcess4();
        
        // $timeout(function(){
        //   getProcess3('018');
        //   getProcess3('019');
        //   getProcess3('050');
        //   getProcess6();
        // }, 1500);
      }, 1500);
    };

    $timeout(function() {
      $scope.$on('reloadChart', function() {
        getProcess();
        getProcess4();

        // $timeout(function(){
        //   getProcess3('018');
        //   getProcess3('019');
        //   getProcess3('050');
        //   getProcess6();
        // }, 1500);
      });
    }, 0);

    $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
       //assign the "from" parameter to something
       $rootScope.previousState = from;
    });

    /* For option 'Pilih' */
    // setup clear
    $scope.clear = function() {
      $scope.dateFrom = null;
      $scope.dateTo = null;
    };

    // open min-cal
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.maxDate = new Date();
    $scope.minDate = '2010-01-01';
    $scope.format = 'yyyy-MM-dd';

    $scope.dateFrom = '';
    $scope.dateTo = '';
    /* For option 'Pilih' */

    /* Home 1 */
    function getProcess() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $scope.summaryConfig = {
        loading: true
      };

      var chartConfig = function(series) {
        var cfg = {
          options: {
            chart: {
              type: 'pie',
              options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
              }
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                  enabled: true,
                  format: '{point.name}'
                }
              }
            }
          },
          title: {
            text: ''
          },
          tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          size: {
            height: 220,
            width: 300
          },
          series: [{
            type: 'pie',
            name: 'Mentions',
            data: series,
            point: {
              events: {
                click: function(event) {
                  var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
                    to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

                  if (from !== '' && to !== '') {
                    from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
                    to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
                  }

                  ArticleListData.setObject({
                    type: '0',
                    date_to: to,
                    date_from: from,
                    media_set: $rootScope.selectedGroupMedia.media_set_id,
                    media_id: $rootScope.selectedMediaSelection.user_media_id,
                    group_category: $rootScope.selectedGroupCategory.category_set,
                    sub_category: this.name,
                    tone: '',
                    original_date_from: from,
                    original_date_to: to,
                    old_url: ''
                  });
                  $state.go('dashboards.list');
                }
              }
            }
          }]
        };

        return cfg;
      };

      $http.get(url).success(function(response) {
        $scope.summaryConfigData = JSON.parse(JSON.stringify(response));
        $scope.summaryConfig = chartConfig($scope.summaryConfigData);
      }).error(function(response) {
        $scope.summaryConfig = chartConfig({});
      });
    }

    function getProcess3(code) {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess3?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to +
        '&code=' + code;

      $http.get(url).success(function(response) {
        if (code === '018'){
          $scope.coverageTonalityConfigData = JSON.parse(JSON.stringify(response));
          // $scope.coverageTonalityConfig = chartConfig($scope.coverageTonalityConfigData);
        } else if (code === '019'){
          $scope.categoryConfigData = JSON.parse(JSON.stringify(response));
          // $scope.categoryConfig = chartConfig($scope.categoryConfigData);
        } else if (code === '050'){
          var result = JSON.parse(JSON.stringify(response));
          $scope.mediaSelectionConfigData = result;
          $scope.mediaList = response.category_detail;
          // $scope.mediaSelectionConfig = chartConfig(result);
        }
      }).error(function(response) {
        if (code === '018')
          $scope.coverageTonalityConfig = chartConfig({});
        else if (code === '019')
          $scope.categoryConfig = chartConfig({});
        else if (code === '050')
          $scope.mediaSelectionConfig = chartConfig({});
      });
    };

    function getProcess4() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess4?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $scope.mediaVisibilityConfig = {
        loading: true
      };

      var chartConfig = function(series) {
        var cfg = {
          options: {
            chart: {
              type: 'area'
            },
            plotOptions: {
              series: {
                cursor: 'pointer',
                point: {
                  events: {
                    click: function() {
                      ArticleListData.setObject({
                        type: '4',
                        date_to: this.category,
                        date_from: this.category,
                        media_set: $rootScope.selectedGroupMedia.media_set_id,
                        media_id: $rootScope.selectedMediaSelection.user_media_id,
                        group_category: $rootScope.selectedGroupCategory.category_set,
                        sub_category: this.series.name,
                        tone: '',
                        original_date_from: from,
                        original_date_to: to,
                        old_url: ''
                      });
                      $state.go('dashboards.list');
                    }
                  }
                }
              }
            }
          },
          xAxis: {
            title: {
              text: 'Date'
            },
            categories: series.categories
          },
          yAxis: {
            title: {
              text: 'Number of Article'
            },
            min: 0
          },
          series: series.series,
          title: {
            text: ''
          },
          size: {
            //height: 220
            height: 300
          },
          loading: false
        };

        return cfg;
      };

      $http.get(url).success(function(response) {
        $scope.mediaVisibilityConfigData = JSON.parse(JSON.stringify(response));
        $scope.mediaVisibilityConfig = chartConfig($scope.mediaVisibilityConfigData);
        
        // Getting trending highlight
        var params = {
          "type":"date",
          "date_to": response.highest.name,
          "date_from": response.highest.name,
          "category": response.highest.category,
          "offset": 0,
          "time_frame": $rootScope.selectedPeriodOptions.value
        };
        $http.post('/api/dashboards/getarticlelist', params).success(function(response) {
          $scope.trendingHighlightList = response.data;
        }).error(function(response) {
          $scope.trendingHighlightList = null;
        });
      }).error(function(response) {
        $scope.mediaVisibilityConfig = chartConfig({});
      });
    }

    function getProcess6() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      var sentimentLabels = ["0", "Potential", "Emerging", "Current", "Crisis"];

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess6?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(response) {
        $scope.ewsConfigData = JSON.parse(JSON.stringify(response));
        // $scope.ewsConfig = chartConfig($scope.ewsConfigData);
        // getTrendingHighlight();
      }).error(function(response) {
        $scope.ewsConfigData = {};
      });
    }

    $scope.getDetailArticle = function(val) {
      var url = '/api/dashboards/getarticledetail?_id=' + val;
      $http.get(url).success(function(response) {
        response.data.data.content = response.data.data.content.replace(/(?:\r\n|\r|\n)/g, '<br />');
        $scope.articleDetail = response.data.data;


        $.fancybox.open({href: '#dd1'});
      }).error(function(response) {
        $scope.articleDetail = null;
      });
    };
    /* Home 1 */

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.downloadPPT = function(){
      /* Home 1 */
      // area chart
      var pptItem = [];
      var mv = $scope.mediaVisibilityConfigData;
      var mvjson = {};
      var colors = ['ff0000', '00ff00', '0000ff', 'ffff00', 'ff00ff', '00ffff', '000000', '90C3D4', 'D4A190', 'A1D490', 'C390D4', 'E39DDD', '9DE3BA', 'D4505E', 'D4CF50'];
      mvjson.title = 'Media Visibility';
      mvjson.renderType = 'column';
      mvjson.data = [];

      for(var i=0;i<mv.series.length;i++){
        mvjson.data.push({
          name: mv.series[i].name,
          labels: mv.categories,
          values: mv.series[i].data,
          color: colors[i]
        });
      }

      pptItem.push(mvjson);

      // pie chart (summary)
      var sc = $scope.summaryConfigData;
      var scjson = {};
      var labels = [],
          values = [];
      scjson.title = 'Summary';
      scjson.renderType = 'pie';
      scjson.data = [];

      for(var i=0;i<sc.length;i++){
        if(sc[i][1] > 0){
          labels.push(sc[i][0]);
          values.push(sc[i][1]);
        }
      }
      scjson.data.push({
        name: 'Summary',
        labels: labels,
        values: values,
        colors: colors
      });

      pptItem.push(scjson);
      /* Home 1 */

      /* Home 2 */

      /* Home 2 */

      var xx = btoa(JSON.stringify(pptItem));
      var downloadPPTURL = '/api/dashboards/downloadppt?data=' + xx;
      window.open(downloadPPTURL, '_blank');
      // var items = {data: pptItem};
      // $http.post('/api/dashboards/downloadppt', items).success(function(data, status, headers, config){
      //   headers = headers();

      //   var fileData = new Blob([data], { type: headers['content-type']});
      //   FileSaver.saveAs(fileData, headers['x-filename'], false);

      // }).error(function(response){
      //   alert('gagal eh');
      // });
    };

    $scope.downloadExcel = function(){
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/downloadexcel?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(data, status, headers, config){
        var ws_name = "Antara Insight";
        var wb = new Workbook(), ws = sheet_from_json_array(data);
        wb.SheetNames.push(ws_name);
        wb.Sheets[ws_name] = ws;
        var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
        var filename = "summary - " + new Date() + ".xlsx";
        FileSaver.saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
      }).error(function(data){
        alert(JSON.stringify(data));
      });
      // window.open(url, '_blank');
      // var data = new Blob([text], { type: 'text/plain;charset=utf-8' });

      /*******************/
      /* original data */
    };

    function s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    function datenum(v, date1904) {
      if(date1904) v+=1462;
      var epoch = Date.parse(v);
      return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }
    
    function sheet_from_json_array(data, opts){
      var ws = {};
      // s = start of range, e = end of range
      // c = column, r = row
      var range = {s: {c:0, r:10000000}, e: {c:0, r:0}};
      var cell_ref = XLSX.utils.encode_cell({c: 0, r: 0});
      ws[cell_ref] = {
        v: 'No',
        t: 's',
        s: {
          patternType: 'solid',
          fgColor: {
            theme: 8,
            tint: 0.3999755851924192,
            rgb: '9ED2E0'
          },
          bgColor: {
            indexed: 64
          }
        }
      };

      cell_ref = XLSX.utils.encode_cell({c: 1, r: 0});
      ws[cell_ref] = {v: 'Date', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 2, r: 0});
      ws[cell_ref] = {v: 'Client Set', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 3, r: 0});
      ws[cell_ref] = {v: 'Sub Client Set', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 4, r: 0});
      ws[cell_ref] = {v: 'Title', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 5, r: 0});
      ws[cell_ref] = {v: 'Media Type', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 6, r: 0});
      ws[cell_ref] = {v: 'Media Name', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 7, r: 0});
      ws[cell_ref] = {v: 'Page', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 8, r: 0});
      ws[cell_ref] = {v: 'Source', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 9, r: 0});
      ws[cell_ref] = {v: 'Tone', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 10, r: 0});
      ws[cell_ref] = {v: 'Journalist', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 11, r: 0});
      ws[cell_ref] = {v: 'PR Value', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 12, r: 0});
      ws[cell_ref] = {v: 'Ad Value', t: 's'};

      range.s.r = 0;
      range.e.c = 14;

      for(var R=0;R<data.length;R++){
        if(range.s.r > R) range.s.r = R+1;
        if(range.e.r < R) range.e.r = R+1;

        var cell = {v: R+1, t: 'n'};
        var cell_ref = XLSX.utils.encode_cell({c: 0, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].datee, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 1, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: $rootScope.selectedGroupCategory.descriptionz, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 2, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].category_id, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 3, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].title, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 4, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].media_type, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 5, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].media_name, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 6, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].page, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 7, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].file_pdf, f: '=HYPERLINK("'+ data[R].file_pdf +'","Link")'};
        cell_ref = XLSX.utils.encode_cell({c: 8, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].tone, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 9, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].journalist, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 10, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].prvalue, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 11, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].advalue, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 12, r: R+1});
        ws[cell_ref] = cell;
      }
      ws['!cols'] = [
        {wch:3}, // "characters"
        {wpx:75}, // "pixels"
        {wch:12},
        {wpx:100},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:12},
        {wpx:100},
        {wch:12},
        {wpx:100}
      ];
      if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
      return ws;
    }
     
    function Workbook() {
      if(!(this instanceof Workbook)) return new Workbook();
      this.SheetNames = [];
      this.Sheets = {};
    }

  }
]);

// Home2

appModule.controller('DashboardsHome2Controller', ['$scope', '$rootScope', '$state', '$stateParams', '$location', '$http', '$timeout', '$sce', 'Authentication', 'ArticleListData', 'FileSaver', 'Blob',
  function($scope, $rootScope, $state, $stateParams, $location, $http, $timeout, $sce, Authentication, ArticleListData, FileSaver, Blob) {
    $scope.authentication = Authentication;

    $scope.trustAsHtml = $sce.trustAsHtml;

    $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
       //assign the "from" parameter to something
       $rootScope.previousState = from;
    });

    $timeout(function() {
      $scope.$on('reloadChart', function() {
        getProcess3('018');
        getProcess3('019');
        getProcess3('050');

        // $timeout(function(){
        //   getProcess();
        //   getProcess4();
        //   getProcess6();
        // }, 1500);
      });
    }, 0);

    $scope.initData = function(){
      $timeout(function(){
        getProcess3('018');
        getProcess3('019');
        getProcess3('050');

        // $timeout(function(){
        //   getProcess();
        //   getProcess4();
        //   getProcess6();
        // }, 1500);
      }, 1500);
    };

    /* For option 'Pilih' */
    // setup clear
    $scope.clear = function() {
      $scope.dateFrom = null;
      $scope.dateTo = null;
    };

    // open min-cal
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.maxDate = new Date();
    $scope.minDate = '2010-01-01';
    $scope.format = 'yyyy-MM-dd';

    $scope.dateFrom = '';
    $scope.dateTo = '';
    /* For option 'Pilih' */

    function getProcess3(code) {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess3?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to +
        '&code=' + code;

      $scope.coverageTonalityConfig = {
        loading: true
      };
      $scope.categoryConfig = {
        loading: true
      };
      $scope.mediaSelectionConfig = {
        loading: true
      };

      var chartConfig = function(series) {
        var cfg = {
          title: {
            text: ''
          },
          xAxis: {
            categories: series.category
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Number of Article'
            },
            stackLabels: {
              enabled: true,
              style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
              }
            }
          },
          options: {
            chart: {
              type: 'column'
            },
            colors: ['#00BD00', '#0019BD', '#FF000D'],
            legend: {
              align: 'right',
              x: 0,
              verticalAlign: 'top',
              y: -10,
              floating: false,
              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
              borderColor: '#CCC',
              borderWidth: 1,
              shadow: false
            },
            tooltip: {
              formatter: function() {
                return '<b>' + this.x + '</b><br/>' +
                  this.series.name + ': ' + this.y + '<br/>' +
                  'Total: ' + this.point.stackTotal;
              }
            },
            plotOptions: {
              column: {
                stacking: 'normal',
                dataLabels: {
                  enabled: true,
                  color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                  style: {
                    textShadow: '0 0 3px black, 0 0 3px black'
                  }
                }
              },
              series: {
                cursor: 'pointer'
              }
            }
          },
          series: series.series,
          size: {
            height: 300
          }
        };

        if(code === '050'){
          cfg.options.plotOptions.series.point = {
            events: {
              click: function() {
                ArticleListData.setObject({
                  type: '050',
                  date_to: '',
                  date_from: '',
                  media_set: $rootScope.selectedGroupMedia.media_set_id,
                  media_id: {
                    name: this.category,
                    id: $scope.mediaList[this.category]
                  },
                  group_category: $rootScope.selectedGroupCategory.category_set,
                  sub_category: $rootScope.selectedSubCategory.category_id,
                  tone: this.series.name,
                  original_date_from: from,
                  original_date_to: to,
                  old_url: ''
                });
                $state.go('dashboards.list');
              }
            }
          };
        } else if(code === '019'){  // category_id
          cfg.options.plotOptions.series.point = {
            events: {
              click: function() {
                ArticleListData.setObject({
                  type: '019',
                  date_to: '',
                  date_from: '',
                  media_set: $rootScope.selectedGroupMedia.media_set_id,
                  media_id: $rootScope.selectedMediaSelection.user_media_id,
                  group_category: $rootScope.selectedGroupCategory.category_set,
                  sub_category: this.category,
                  tone: this.series.name,
                  original_date_from: from,
                  original_date_to: to,
                  old_url: ''
                });
                $state.go('dashboards.list');
              }
            }
          };
        } else if(code === '018'){  // datee
          cfg.options.plotOptions.series.point = {
            events: {
              click: function() {
                ArticleListData.setObject({
                  type: '018',
                  date_to: this.category,
                  date_from: this.category,
                  media_set: $rootScope.selectedGroupMedia.media_set_id,
                  media_id: $rootScope.selectedMediaSelection.user_media_id,
                  group_category: $rootScope.selectedGroupCategory.category_set,
                  sub_category: $rootScope.selectedSubCategory.category_id,
                  tone: this.series.name,
                  original_date_from: from,
                  original_date_to: to,
                  old_url: ''
                });
                $state.go('dashboards.list');
              }
            }
          };
        }

        return cfg;
      };

      $http.get(url).success(function(response) {
        if (code === '018'){
          $scope.coverageTonalityConfigData = JSON.parse(JSON.stringify(response));
          $scope.coverageTonalityConfig = chartConfig($scope.coverageTonalityConfigData);
        } else if (code === '019'){
          $scope.categoryConfigData = JSON.parse(JSON.stringify(response));
          $scope.categoryConfig = chartConfig($scope.categoryConfigData);
        } else if (code === '050'){
          var result = JSON.parse(JSON.stringify(response));
          $scope.mediaSelectionConfigData = result;
          $scope.mediaList = response.category_detail;
          $scope.mediaSelectionConfig = chartConfig(result);
        }
      }).error(function(response) {
        if (code === '018')
          $scope.coverageTonalityConfig = chartConfig({});
        else if (code === '019')
          $scope.categoryConfig = chartConfig({});
        else if (code === '050')
          $scope.mediaSelectionConfig = chartConfig({});
      });
    };

    function getProcess() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(response) {
        $scope.summaryConfigData = JSON.parse(JSON.stringify(response));
        // $scope.summaryConfig = chartConfig($scope.summaryConfigData);
      }).error(function(response) {
        $scope.summaryConfigData = {};
      });
    }

    function getProcess4() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess4?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(response) {
        $scope.mediaVisibilityConfigData = JSON.parse(JSON.stringify(response));
        // $scope.mediaVisibilityConfig = chartConfig($scope.mediaVisibilityConfigData);
      }).error(function(response) {
        $scope.mediaVisibilityConfigData = {};
      });
    };

    function getProcess6() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      var sentimentLabels = ["0", "Potential", "Emerging", "Current", "Crisis"];

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess6?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(response) {
        $scope.ewsConfigData = JSON.parse(JSON.stringify(response));
        // $scope.ewsConfig = chartConfig($scope.ewsConfigData);
        // getTrendingHighlight();
      }).error(function(response) {
        $scope.ewsConfigData = {};
      });
    }

    $scope.downloadPPT = function(){
      var pptItem = [];
      var labelsarr = ['Positive', 'Neutral', 'Negative'];

      // Coverage Tonality
      var ct = $scope.coverageTonalityConfigData;
      var ctjson = {};
      ctjson.title = 'Coverage Tonality';
      ctjson.renderType = 'bar';
      ctjson.data = [];

      for(var i=0;i<ct.category.length;i++){
        var category = ct.category[i];
        var tempjson = {};
        var posvalue = 0,
            neuvalue = 0,
            negvalue = 0;
        tempjson.name = category;

        for(var j=0;j<ct.series.length;j++){
          switch(ct.series[j].name){
            case labelsarr[0]:
              posvalue = ct.series[j].data[i];
              break;
            case labelsarr[1]:
              neuvalue = ct.series[j].data[i];
              break;
            case labelsarr[2]:
              negvalue = ct.series[j].data[i];
              break;
          }
        }

        tempjson.labels = labelsarr;
        tempjson.values = [];
        tempjson.values.push(posvalue);
        tempjson.values.push(neuvalue);
        tempjson.values.push(negvalue);

        ctjson.data.push(tempjson);
      }

      pptItem.push(ctjson);

      // Tone by Category chart
      var tc = $scope.categoryConfigData;
      var tcjson = {};
      tcjson.title = 'Tone by Category';
      tcjson.renderType = 'bar';
      tcjson.data = [];

      for(var i=0;i<tc.category.length;i++){
        var category = tc.category[i];
        var tempjson = {};
        var posvalue = 0,
            neuvalue = 0,
            negvalue = 0;
        tempjson.name = category;

        for(var j=0;j<tc.series.length;j++){
          switch(tc.series[j].name){
            case labelsarr[0]:
              posvalue = tc.series[j].data[i];
              break;
            case labelsarr[1]:
              neuvalue = tc.series[j].data[i];
              break;
            case labelsarr[2]:
              negvalue = tc.series[j].data[i];
              break;
          }
        }

        tempjson.labels = labelsarr;
        tempjson.values = [];
        tempjson.values.push(posvalue);
        tempjson.values.push(neuvalue);
        tempjson.values.push(negvalue);

        tcjson.data.push(tempjson);
      }

      pptItem.push(tcjson);

      // Tone by Media Selection
      var ms = $scope.mediaSelectionConfigData;
      var msjson = {};
      msjson.title = 'Tone by Media Selection';
      msjson.renderType = 'bar';
      msjson.data = [];

      for(var i=0;i<ms.category.length;i++){
        var category = ms.category[i];
        var tempjson = {};
        var posvalue = 0,
            neuvalue = 0,
            negvalue = 0;
        tempjson.name = category;

        for(var j=0;j<ms.series.length;j++){
          switch(ms.series[j].name){
            case labelsarr[0]:
              posvalue = ms.series[j].data[i];
              break;
            case labelsarr[1]:
              neuvalue = ms.series[j].data[i];
              break;
            case labelsarr[2]:
              negvalue = ms.series[j].data[i];
              break;
          }
        }

        tempjson.labels = labelsarr;
        tempjson.values = [];
        tempjson.values.push(posvalue);
        tempjson.values.push(neuvalue);
        tempjson.values.push(negvalue);

        msjson.data.push(tempjson);
      }

      pptItem.push(msjson);

      var xx = btoa(JSON.stringify(pptItem));
      var downloadPPTURL = '/api/dashboards/downloadppt?data=' + xx;
      window.open(downloadPPTURL, '_blank');
    };

    $scope.downloadExcel = function(){
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/downloadexcel?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(data, status, headers, config){
        var ws_name = "Antara Insight";
        var wb = new Workbook(), ws = sheet_from_json_array(data);
        wb.SheetNames.push(ws_name);
        wb.Sheets[ws_name] = ws;
        var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
        var filename = "summary - " + new Date() + ".xlsx";
        FileSaver.saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
      }).error(function(data){
        alert(JSON.stringify(data));
      });
      // window.open(url, '_blank');
      // var data = new Blob([text], { type: 'text/plain;charset=utf-8' });

      /*******************/
      /* original data */
    };

    function s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    function datenum(v, date1904) {
      if(date1904) v+=1462;
      var epoch = Date.parse(v);
      return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }
    
    function sheet_from_json_array(data, opts){
      var ws = {};
      // s = start of range, e = end of range
      // c = column, r = row
      var range = {s: {c:0, r:10000000}, e: {c:0, r:0}};
      var cell_ref = XLSX.utils.encode_cell({c: 0, r: 0});
      ws[cell_ref] = {
        v: 'No',
        t: 's',
        s: {
          patternType: 'solid',
          fgColor: {
            theme: 8,
            tint: 0.3999755851924192,
            rgb: '9ED2E0'
          },
          bgColor: {
            indexed: 64
          }
        }
      };

      cell_ref = XLSX.utils.encode_cell({c: 1, r: 0});
      ws[cell_ref] = {v: 'Date', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 2, r: 0});
      ws[cell_ref] = {v: 'Client Set', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 3, r: 0});
      ws[cell_ref] = {v: 'Sub Client Set', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 4, r: 0});
      ws[cell_ref] = {v: 'Title', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 5, r: 0});
      ws[cell_ref] = {v: 'Media Type', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 6, r: 0});
      ws[cell_ref] = {v: 'Media Name', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 7, r: 0});
      ws[cell_ref] = {v: 'Source', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 8, r: 0});
      ws[cell_ref] = {v: 'Tone', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 9, r: 0});
      ws[cell_ref] = {v: 'Journalist', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 10, r: 0});
      ws[cell_ref] = {v: 'PR Value', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 11, r: 0});
      ws[cell_ref] = {v: 'Ad Value', t: 's'};

      range.s.r = 0;
      range.e.c = 14;

      for(var R=0;R<data.length;R++){
        if(range.s.r > R) range.s.r = R+1;
        if(range.e.r < R) range.e.r = R+1;

        var cell = {v: R+1, t: 'n'};
        var cell_ref = XLSX.utils.encode_cell({c: 0, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].datee, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 1, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: $rootScope.selectedGroupCategory.descriptionz, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 2, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].sub_client, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 3, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].title, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 4, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].media_type, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 5, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].media_name, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 6, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].page, f: '=HYPERLINK("'+ data[R].page +'","Link")'};
        cell_ref = XLSX.utils.encode_cell({c: 7, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].tone, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 8, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].journalist, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 9, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: 14000000, t: 'n'};
        cell_ref = XLSX.utils.encode_cell({c: 10, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: 14000000, t: 'n'};
        cell_ref = XLSX.utils.encode_cell({c: 11, r: R+1});
        ws[cell_ref] = cell;
      }
      ws['!cols'] = [
        {wch:3}, // "characters"
        {wpx:75}, // "pixels"
        {wch:12},
        {wpx:100},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:12},
        {wpx:100},
        {wch:12},
        {wpx:100}
      ];
      if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
      return ws;
    }
     
    function Workbook() {
      if(!(this instanceof Workbook)) return new Workbook();
      this.SheetNames = [];
      this.Sheets = {};
    }

  }
]);


// Home 3

appModule.controller('DashboardsHome3Controller', ['$scope', '$rootScope', '$state', '$stateParams', '$location', '$http', '$timeout', '$sce', 'Authentication', 'ArticleListData', 'FileSaver', 'Blob',
  function($scope, $rootScope, $state, $stateParams, $location, $http, $timeout, $sce, Authentication, ArticleListData, FileSaver, Blob) {
    $scope.authentication = Authentication;

    $scope.trustAsHtml = $sce.trustAsHtml;
    
    $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
       //assign the "from" parameter to something
       $rootScope.previousState = from;
    });

    $timeout(function() {
      $scope.$on('reloadChart', function() {
        getProcess6();
        
        // $timeout(function(){
        //   getProcess();
        //   getProcess4();
        //   getProcess3('018');
        //   getProcess3('019');
        //   getProcess3('050');
        // }, 1500);
      });
    }, 0);

    $scope.initData = function(){
      $timeout(function(){
        getProcess6();
// migrateEWS();
        // $timeout(function(){
        //   getProcess();
        //   getProcess4();
        //   getProcess3('018');
        //   getProcess3('019');
        //   getProcess3('050');
        // }, 1500);
      }, 1500);
    };


    function migrateEWS(){
      var categoryArr = [], mediaArr = [];
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      var sentimentLabels = ["0", "Potential", "Emerging", "Current", "Crisis"];

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }


      // get categories
      if($rootScope.selectedSubCategory.category_id === 'All Category'){
        for(var i=0;i<$rootScope.allCategories.length;i++){
          if(i > 0) categoryArr.push($rootScope.allCategories[i].category_id);
        }
      } else{
        categoryArr = [$rootScope.selectedSubCategory.category_id];
      }

      // get medias
      if($rootScope.selectedMediaSelection.user_media_id === '00'){
        for(var i=0;i<$rootScope.allMedias.length;i++){
          if(i > 0) mediaArr.push($rootScope.allMedias[i].user_media_id);
        }
      } else{
        mediaArr = [$rootScope.selectedMediaSelection.user_media_id];
      }

      var assets = {
        'category_id': categoryArr,
        'media_id': mediaArr,
        'best': 20,
        'time_frame': $rootScope.selectedPeriodOptions.value,
        'date_from': from,
        'date_to': to
      };

      var abs = {
        'a': '054B',
        'keys': $scope.authentication.user.user_detail.usr_keys,
        'uid': $scope.authentication.user.user_detail.usr_uid,
        'client_id': $scope.authentication.user.user_detail.usr_client_id,
        'data': btoa(JSON.stringify(assets))
      };
      // var mskapiURL = "http://mskapi.antara-insight.id/api-new.php?";
      var mskapiURL = "/api/dashboards/oldews";

      // console.log(JSON.stringify(abs));
      $http({
        url: mskapiURL,
        method: "POST",
        // headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        // data: $.param(abs)
        data: abs
      }).success(function(data, status, headers, config) {
        console.log('success');
        console.log(JSON.stringify(data));
      }).error(function(data, status, headers, config) {
        console.log('error');
        console.log(JSON.stringify(data));
      });
    }

    function serializeObj(obj) {
      var result = [];

      for (var property in obj)
          result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));

      return result.join("&");
    }

    function getProcess6() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      var sentimentLabels = ["0", "Potential", "Emerging", "Current", "Crisis"];

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess6?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $scope.ewsConfig = {
        loading: true
      };

      var chartConfig = function(series) {
        var cfg = {
          title: {
            text: ''
          },
          tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          size: {
            //height: 220
            height: 300
          },
          xAxis: {
            title: {
              text: 'Date'
            },
            categories: series.categories
          },
          yAxis: {
            title: {
              text: 'Stage'
            },
            labels: {
              formatter: function(){
                return sentimentLabels[this.value];
              }
            },
            tickInterval: 1,
            min: 0,
            max: 4
          },
          series: series.series,
          options: {
            chart: {
              type: 'line'
            },
            plotOptions: {
              series: {
                cursor: 'pointer',
                point: {
                  events: {
                    click: function() {
                      ArticleListData.setObject({
                        type: 'ews',
                        date_to: this.category,
                        date_from: this.category,
                        media_set: $rootScope.selectedGroupMedia.media_set_id,
                        media_id: $rootScope.selectedMediaSelection.user_media_id,
                        group_category: $rootScope.selectedGroupCategory.category_set,
                        sub_category: $rootScope.selectedSubCategory.category_id,
                        tone: this.series.name,
                        original_date_from: from,
                        original_date_to: to,
                        old_url: {
                          type: 'date',
                          date_to: this.category,
                          date_from: this.category,
                          media: '',
                          category: this.series.name,
                          tone: 'Negative'
                        }
                      });
                      $state.go('dashboards.list');
                    }
                  }
                }
              }
            }
          },
        };

        return cfg;
      };

      $http.get(url).success(function(response) {
        $scope.ewsConfigData = JSON.parse(JSON.stringify(response));
        $scope.ewsConfig = chartConfig($scope.ewsConfigData);
        getTrendingHighlight();
      }).error(function(response) {
        $scope.ewsConfig = chartConfig({});
      });
    }

    function getProcess() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(response) {
        $scope.summaryConfigData = JSON.parse(JSON.stringify(response));
        // $scope.summaryConfig = chartConfig($scope.summaryConfigData);
      }).error(function(response) {
        $scope.summaryConfigData = {};
      });
    }

    function getProcess3(code) {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess3?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to +
        '&code=' + code;

      $http.get(url).success(function(response) {
        if (code === '018'){
          $scope.coverageTonalityConfigData = JSON.parse(JSON.stringify(response));
          // $scope.coverageTonalityConfig = chartConfig($scope.coverageTonalityConfigData);
        } else if (code === '019'){
          $scope.categoryConfigData = JSON.parse(JSON.stringify(response));
          // $scope.categoryConfig = chartConfig($scope.categoryConfigData);
        } else if (code === '050'){
          var result = JSON.parse(JSON.stringify(response));
          $scope.mediaSelectionConfigData = result;
          $scope.mediaList = response.category_detail;
          // $scope.mediaSelectionConfig = chartConfig(result);
        }
      }).error(function(response) {
        if (code === '018')
          $scope.coverageTonalityConfig = chartConfig({});
        else if (code === '019')
          $scope.categoryConfig = chartConfig({});
        else if (code === '050')
          $scope.mediaSelectionConfig = chartConfig({});
      });
    };

    function getProcess4() {
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/getprocess4?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(response) {
        $scope.mediaVisibilityConfigData = JSON.parse(JSON.stringify(response));
        // $scope.mediaVisibilityConfig = chartConfig($scope.mediaVisibilityConfigData);
      }).error(function(response) {
        $scope.mediaVisibilityConfigData = {};
      });
    };




    function getTrendingHighlight(){
      // Getting trending highlight
      var dateFormatter = function(tgl){
        return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
      };
      var params = {
        // "type":"date",
        "type": "019",
        "date_to": dateFormatter(new Date()),
        "date_from": dateFormatter(new Date()),
        "category_set": $scope.ewsConfigData.highlight.category,
        "offset": 0,
        "time_frame": $rootScope.selectedPeriodOptions.value
      };
      $http.post('/api/dashboards/getarticlelist', params).success(function(response) {
        $scope.trendingHighlightList = response.data;
      }).error(function(response) {
        $scope.trendingHighlightList = null;
      });
    };

    $scope.getDetailArticle = function(val) {
      var url = '/api/dashboards/getarticledetail?_id=' + val;
      $http.get(url).success(function(response) {
        response.data.data.content = response.data.data.content.replace(/(?:\r\n|\r|\n)/g, '<br />');
        $scope.articleDetail = response.data.data;
        $.fancybox.open({href: '#dd1'});
      }).error(function(response) {
        $scope.articleDetail = null;
      });
    };

    $scope.downloadPPT = function(){
      // area chart
      var pptItem = [];
      var ews = $scope.ewsConfigData;
      var ewsjson = {};
      var colors = ['ff0000', '00ff00', '0000ff', 'ffff00', 'ff00ff', '00ffff', '000000', '90C3D4', 'D4A190', 'A1D490', 'C390D4', 'E39DDD', '9DE3BA', 'D4505E', 'D4CF50'];
      ewsjson.title = 'Early Warning System';
      ewsjson.renderType = 'column';
      ewsjson.data = [];

      for(var i=0;i<ews.series.length;i++){
        ewsjson.data.push({
          name: ews.series[i].name,
          labels: ews.categories,
          values: ews.series[i].data,
          color: colors[i]
        });
      }

      pptItem.push(ewsjson);

      var xx = btoa(JSON.stringify(pptItem));
      var downloadPPTURL = '/api/dashboards/downloadppt?data=' + xx;
      window.open(downloadPPTURL, '_blank');

      // render chart to base64
    //   var svg = document.getElementById('container_chart_007').children[0].innerHTML;
    //   var base_image = new Image();
    //   svg = "data:image/svg+xml,"+svg;
    //   base_image.src = svg;
    //   return alert(svg);


    //   var chart = '';
    //   var title = '';
    //   var group_category = '';
    //   var sub_client = '';

    //   var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
    //     to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

    //   if (from !== '' && to !== '') {
    //     from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
    //     to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
    //   }

    //   var periods = dateFormatting($rootScope.selectedPeriodOptions.value, from, to);

    //   var items = {
    //     chart: chart,
    //     title: 'Mekanisme Peringatan Dini / Early Warning System',
    //     cliset: $rootScope.selectedGroupCategory.descriptionz,
    //     subcli: $rootScope.selectedSubCategory.category_id,
    //     time_frame: $rootScope.selectedPeriodOptions.value,
    //     from: periods.date_from,
    //     to: periods.date_to,
    //     logo_comp: Authentication.user.user_detail.logo_head
    //   };

    //   alert(JSON.stringify(items));
    // };

    // function dateFormatting(timeFrame, df, dt) {
    //   var currentDate = new Date(),
    //     dateFlag = new Date(),
    //     interval = 'day',
    //     date_from = df,
    //     date_to = dt;

    //   var dateFormatter = function(tgl) {
    //     return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
    //   }

    //   if (timeFrame !== 0) {
    //     switch (timeFrame) {
    //       case 1:
    //         dateFlag.setDate(dateFlag.getDate() - 1);
    //         date_from = dateFormatter(dateFlag);
    //         date_to = dateFormatter(currentDate);
    //         break;
    //       case 7:
    //         dateFlag.setDate(dateFlag.getDate() - 7);
    //         date_from = dateFormatter(dateFlag);
    //         date_to = dateFormatter(currentDate);
    //         break;
    //       case 30:
    //         dateFlag.setDate(dateFlag.getDate() - 30);
    //         date_from = dateFormatter(dateFlag);
    //         date_to = dateFormatter(currentDate);
    //         break;
    //       case 365:
    //         dateFlag.setDate(dateFlag.getDate() - 365);
    //         date_from = dateFormatter(dateFlag);
    //         date_to = dateFormatter(currentDate);
    //         interval = 'month';
    //         break;
    //     }
    //   } else {
    //     if (date_to == '' || date_from == '') {
    //       dateFlag.setDate(dateFlag.getDate() - 7);
    //       date_from = dateFormatter(dateFlag);
    //       date_to = dateFormatter(currentDate);
    //     }
    //   }

    //   return {
    //     date_from: date_from,
    //     date_to: date_to,
    //     interval: interval
    //   };
    };

    $scope.downloadExcel = function(){
      var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
        to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

      if (from !== '' && to !== '') {
        from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
        to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
      }

      var url = '/api/dashboards/downloadexcel?' +
        'gc=' + $rootScope.selectedGroupCategory.category_set +
        '&ms=' + $rootScope.selectedGroupMedia.media_set_id +
        '&sc=' + $rootScope.selectedSubCategory.category_id +
        '&mi=' + $rootScope.selectedMediaSelection.user_media_id +
        '&tf=' + $rootScope.selectedPeriodOptions.value +
        '&df=' + from +
        '&dt=' + to;

      $http.get(url).success(function(data, status, headers, config){
        var ws_name = "Antara Insight";
        var wb = new Workbook(), ws = sheet_from_json_array(data);
        wb.SheetNames.push(ws_name);
        wb.Sheets[ws_name] = ws;
        var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
        var filename = "summary - " + new Date() + ".xlsx";
        FileSaver.saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
      }).error(function(data){
        alert(JSON.stringify(data));
      });
      // window.open(url, '_blank');
      // var data = new Blob([text], { type: 'text/plain;charset=utf-8' });

      /*******************/
      /* original data */
    };

    function s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    function datenum(v, date1904) {
      if(date1904) v+=1462;
      var epoch = Date.parse(v);
      return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }
    
    function sheet_from_json_array(data, opts){
      var ws = {};
      // s = start of range, e = end of range
      // c = column, r = row
      var range = {s: {c:0, r:10000000}, e: {c:0, r:0}};
      var cell_ref = XLSX.utils.encode_cell({c: 0, r: 0});
      ws[cell_ref] = {
        v: 'No',
        t: 's',
        s: {
          patternType: 'solid',
          fgColor: {
            theme: 8,
            tint: 0.3999755851924192,
            rgb: '9ED2E0'
          },
          bgColor: {
            indexed: 64
          }
        }
      };

      cell_ref = XLSX.utils.encode_cell({c: 1, r: 0});
      ws[cell_ref] = {v: 'Date', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 2, r: 0});
      ws[cell_ref] = {v: 'Client Set', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 3, r: 0});
      ws[cell_ref] = {v: 'Sub Client Set', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 4, r: 0});
      ws[cell_ref] = {v: 'Title', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 5, r: 0});
      ws[cell_ref] = {v: 'Media Type', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 6, r: 0});
      ws[cell_ref] = {v: 'Media Name', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 7, r: 0});
      ws[cell_ref] = {v: 'Source', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 8, r: 0});
      ws[cell_ref] = {v: 'Tone', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 9, r: 0});
      ws[cell_ref] = {v: 'Journalist', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 10, r: 0});
      ws[cell_ref] = {v: 'PR Value', t: 's'};

      cell_ref = XLSX.utils.encode_cell({c: 11, r: 0});
      ws[cell_ref] = {v: 'Ad Value', t: 's'};

      range.s.r = 0;
      range.e.c = 14;

      for(var R=0;R<data.length;R++){
        if(range.s.r > R) range.s.r = R+1;
        if(range.e.r < R) range.e.r = R+1;

        var cell = {v: R+1, t: 'n'};
        var cell_ref = XLSX.utils.encode_cell({c: 0, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].datee, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 1, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: $rootScope.selectedGroupCategory.descriptionz, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 2, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].sub_client, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 3, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].title, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 4, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].media_type, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 5, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].media_name, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 6, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].page, f: '=HYPERLINK("'+ data[R].page +'","Link")'};
        cell_ref = XLSX.utils.encode_cell({c: 7, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].tone, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 8, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: data[R].journalist, t: 's'};
        cell_ref = XLSX.utils.encode_cell({c: 9, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: 14000000, t: 'n'};
        cell_ref = XLSX.utils.encode_cell({c: 10, r: R+1});
        ws[cell_ref] = cell;

        cell = {v: 14000000, t: 'n'};
        cell_ref = XLSX.utils.encode_cell({c: 11, r: R+1});
        ws[cell_ref] = cell;
      }
      ws['!cols'] = [
        {wch:3}, // "characters"
        {wpx:75}, // "pixels"
        {wch:12},
        {wpx:100},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:24},
        {wpx:125},
        {wch:12},
        {wpx:100},
        {wch:12},
        {wpx:100}
      ];
      if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
      return ws;
    }
     
    function Workbook() {
      if(!(this instanceof Workbook)) return new Workbook();
      this.SheetNames = [];
      this.Sheets = {};
    }

  }
]);

// TV

appModule.controller('DashboardsTvController', ['$scope', '$stateParams', '$location', 'Authentication',
  function($scope, $stateParams, $location, Authentication) {
    $scope.authentication = Authentication;

    $scope.mediaToneConfig = {
      title: {
        text: ''
      },
      xAxis: {
        categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total fruit consumption'
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          }
        }
      },
      options: {
        chart: {
          type: 'column'
        },
        legend: {
          align: 'right',
          x: -70,
          verticalAlign: 'top',
          y: 20,
          floating: true,
          backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
          borderColor: '#CCC',
          borderWidth: 1,
          shadow: false
        },
        tooltip: {
          formatter: function() {
            return '<b>' + this.x + '</b><br/>' +
              this.series.name + ': ' + this.y + '<br/>' +
              'Total: ' + this.point.stackTotal;
          }
        },
        plotOptions: {
          column: {
            stacking: 'normal',
            dataLabels: {
              enabled: true,
              color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
              style: {
                textShadow: '0 0 3px black, 0 0 3px black'
              }
            }
          }
        }
      },
      series: [{
        name: 'John',
        data: [5, 3, 4, 7, 2]
      }, {
        name: 'Jane',
        data: [2, 2, 3, 2, 1]
      }, {
        name: 'Joe',
        data: [3, 4, 4, 2, 5]
      }],
      size: {
        height: 220
      }
    }
  }
]);


// RADIO
appModule.controller('DashboardsRadioController', ['$scope', '$stateParams', '$location', 'Authentication',
  function($scope, $stateParams, $location, Authentication) {
    $scope.authentication = Authentication;

    $scope.mediaToneConfig = {
      title: {
        text: ''
      },
      xAxis: {
        categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total fruit consumption'
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          }
        }
      },
      options: {
        chart: {
          type: 'column'
        },
        legend: {
          align: 'right',
          x: -70,
          verticalAlign: 'top',
          y: 20,
          floating: true,
          backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
          borderColor: '#CCC',
          borderWidth: 1,
          shadow: false
        },
        tooltip: {
          formatter: function() {
            return '<b>' + this.x + '</b><br/>' +
              this.series.name + ': ' + this.y + '<br/>' +
              'Total: ' + this.point.stackTotal;
          }
        },
        plotOptions: {
          column: {
            stacking: 'normal',
            dataLabels: {
              enabled: true,
              color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
              style: {
                textShadow: '0 0 3px black, 0 0 3px black'
              }
            }
          }
        }
      },
      series: [{
        name: 'John',
        data: [5, 3, 4, 7, 2]
      }, {
        name: 'Jane',
        data: [2, 2, 3, 2, 1]
      }, {
        name: 'Joe',
        data: [3, 4, 4, 2, 5]
      }],
      size: {
        height: 220
      }
    }
  }
]);


// LIST ARTICLE
appModule.controller('DashboardsListArticleController', ['$scope', '$rootScope', '$state', '$http', '$sce', 'Authentication', 'ArticleListData',
  function($scope, $rootScope, $state, $http, $sce, Authentication, ArticleListData) {
    $scope.authentication = Authentication;

    $scope.initData = ArticleListData.getObject();
    // Pagination
    $scope.currentPage = 1;
    $scope.maxSize = 5;

    function populateList(val){
      var params = val;
      params.time_frame = $rootScope.selectedPeriodOptions.value;

      if(val.type === 'ews'){
        $http.post('/api/dashboards/getarticlelist', params.old_url).success(function(response) {
          $scope.articleLists = response.data;

          $scope.totalItems = response.pagination.total_articles;
        }).error(function(response) {
          $scope.articleLists = null;
        });
      } else{
        $http.post('/api/dashboards/getarticlelistfromcharts', params).success(function(response) {
          $scope.articleLists = response.data;

          $scope.totalItems = response.pagination.total_articles;
        }).error(function(response) {
          $scope.articleLists = null;
        });
      }
    };

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.requestData = function(val) {
      $scope.requestParameters = val;
      $scope.requestParameters.offset = 0;
      populateList(val);
    };

    $scope.getDetailArticle = function(val) {
      var url = '/api/dashboards/getarticledetail?_id=' + val;
      $scope.mediaHTML = "";
      $http.get(url).success(function(response) {
        // $scope.articleDetail = response.data.data;
        // $.fancybox.open({href: '#dd1'});

        var results = response.data.data;

        var keywords = results.keywords;
        var selectedKeyword;

        // var res = results.content.split(" ");
        // for(var i=0;i<res.length;i++){
        //   for(var j=0;j<keywords.length;j++){
        //     selectedKeyword = keywords[j].split(" ");
        //     for(var k=0;k<selectedKeyword.length;k++){
        //       if(res[i].toLowerCase() == selectedKeyword[k].toLowerCase()){
        //         res[i] = "<span style='color:#f89406; font-weight:bolder;'>" + res[i] + "</span>";
        //       } else{
        //         var a = res[i].toLowerCase().indexOf(selectedKeyword[k].toLowerCase());
        //         if(a != -1){
        //             var separatedString = "<span style='color:#f89406; font-weight:bolder;'>" + res[i].substr(a, selectedKeyword[k].length) + "</span>";
        //             var b = res[i].replace(res[i].substr(a, selectedKeyword[k].length), separatedString);
        //             res[i] = b;
        //         }
        //       }

        //     }
        //   }
        // }
        // results.content = res.join(" ");

        var temp;
        var x, y;
        for(var j=0;j<keywords.length;j++){
          selectedKeyword = keywords[j].match(/\w+|"[^"]+"/g);
          for(var k=0;k<selectedKeyword.length;k++){
            x = selectedKeyword[k];
            x = x.replace(/\"/g, "");
            x = x.replace(/\'/g, "");

            temp = results.content.replace(new RegExp(x, "ig"), "<span style='color:#f89406; font-weight:bolder;'>" + x + "</span>");
            results.content = temp;
          }
        }

        results.content = results.content.replace(/(?:\r\n|\r|\n)/g, '<br />');
        
        if(results.file_pdf.indexOf('.mp4') !== -1){
          var media_type = (results.file_pdf.indexOf('.mp3') !== -1) ? 'media_radio' : 'media_tv';
          var file = response.data.data.file_pdf;
          var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);

          $scope.mediaHTML = '<video width="25%" controls>' +
            '<source src="' + 'http://pdf.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="video/mp4">' +
            'Your browser does not support the video tag.' +
          '</video>';

        } else if(results.file_pdf.indexOf('.mp3') !== -1){
          var file = response.data.data.file_pdf;
          var tanggal = file.substring(8, 10),
          bulan = file.substring(5, 7),
          tahun = file.substring(0, 4);

          $scope.mediaHTML = '<audio width="25%" controls>' +
            '<source src="' + 'http://pdf.antara-insight.id/media_radio/' + tahun + '/' + bulan + '/' + tanggal + '/' + file + '" type="audio/mpeg">' +
            'Your browser does not support the audio tag.' +
          '</audio>';
        } else{
          results.file_pdf = results.file_pdf;
        }

        $scope.articleDetail = results;
        // $scope.articleDetail.media_name = mn;

        $.fancybox.open({href: '#dd1'});

      }).error(function(response) {
        $scope.articleDetail = null;
      });
    };

    // Page changing
    $scope.pageChanged = function() {
      $scope.requestParameters.offset = ($scope.currentPage - 1) * 10;
      populateList($scope.requestParameters);
    };

    $scope.goBackToPreviousState = function(){
      $state.go($rootScope.previousState.name);
    };
  }
]);
