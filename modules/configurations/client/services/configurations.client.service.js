'use strict';

//Configurations service used for communicating with the configurations REST endpoints
angular.module('configurations').factory('Configurations', ['$resource',
  function ($resource) {
    return $resource('api/configurations/:configurationId', {
      configurationId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
