'use strict';

// Configuring the Dashboards module
angular.module('dashboards', ['highcharts-ng', 'ui.bootstrap', 'ngFileSaver', 'ngPrint', 'angular-js-xlsx', 'angular-loading-bar']).run(['Menus',
  function (Menus) {
    // Add the dashboards dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Dashboard',
      state: 'dashboards'
    });
  }
]);
