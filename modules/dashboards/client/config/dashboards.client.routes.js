'use strict';

// Setting up route
angular.module('dashboards').config(['$stateProvider', 'cfpLoadingBarProvider',
  function ($stateProvider, cfpLoadingBarProvider) {
    // Dashboards state routing
    $stateProvider
      .state('dashboards', {
        abstract: true,
        url: '/dashboards',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('dashboards.print1', {
        url: '',
        templateUrl: 'modules/dashboards/views/print/home1-dashboard.client.view.html'
      })
      .state('dashboards.print2', {
        url: '/print2',
        templateUrl: 'modules/dashboards/views/print/home2-dashboard.client.view.html'
      })
      .state('dashboards.print3', {
        url: '/print3',
        templateUrl: 'modules/dashboards/views/print/home3-dashboard.client.view.html'
      })

      .state('dashboards.tv', {
        url: '/tv',
        templateUrl: 'modules/dashboards/views/tv/tv-dashboard.client.view.html'
      })

      .state('dashboards.radio', {
        url: '/radio',
        templateUrl: 'modules/dashboards/views/radio/radio-dashboard.client.view.html'
      })

      .state('dashboards.list', {
        url: '/list',
        templateUrl: 'modules/dashboards/views/print/article-list.client.view.html'
      });

      cfpLoadingBarProvider.includeSpinner = false;
  }
]);
