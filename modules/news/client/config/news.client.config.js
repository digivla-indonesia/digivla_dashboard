'use strict';

// Configuring the Articles module
angular.module('news', ['ngPrint', 'angular-loading-bar', 'ngTable']).run(['Menus',
    function(Menus) {
        // Add the articles dropdown item
        Menus.addMenuItem('topbar', {
            title: 'News Clipping',
            state: 'news'
        });
    }
]);
