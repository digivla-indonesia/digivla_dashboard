'use strict';

// Configuration up route
angular.module('configurations').config(['$stateProvider', 'cfpLoadingBarProvider',
  function ($stateProvider, cfpLoadingBarProvider) {
    // Configurations state routing
    $stateProvider
      .state('configurations', {
        abstract: true,
        url: '/configurations',
        template: '<ui-view/>',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('configurations.groupMedia', {
        url: '',
        templateUrl: 'modules/configurations/views/group-media.client.view.html'
      })
      .state('configurations.groupCategory', {
        url: '/groupcategory',
        templateUrl: 'modules/configurations/views/group-category.client.view.html'
      })
      .state('configurations.groupSubCategory', {
        url: '/subcategory',
        templateUrl: 'modules/configurations/views/group-sub-category.client.view.html'
      });
  }
]);
